#!/bin/sh
#=======================================================================
# Create AGCM model diagnostic files (*gp and *xp)     --- mdiag_t63 ---
# $Id: mdiag_t63_jobdef 657 2012-02-22 23:48:49Z acrnrls $
#=======================================================================
#     keyword :: mdiag_t63
# description :: agcm model diagnostics (T63)
#        hide :: yes

set -a
. betapath2

#  * ........................... Parmsub Parameters ............................

 crawork=${runid}_mdiag; username="acrnxxx"; user="XXX"; days="MAM"
 runid=aaa; uxxx=uxxx; mdiag_flabel_uxxx=$uxxx; mdiag_model_uxxx=$uxxx
 jobname=mdiag; mdiag_flabel_prefix=${mdiag_flabel_uxxx}_${runid}

 oldiag="diag4";

 nqsprfx="x${runid}_"; nqsext='';
 RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;
 run=AGCM;

# use_month_names will flag the use of jan,feb,mar,... rather than m01,m02,m03,...
 use_month_names=0

 year="001"; mon="06"; monn="jan"; flabel="${mdiag_flabel_prefix}_${year}_m${mon}_";
 memory99=1; mdiag_model_prefix=${mdiag_model_uxxx}_${runid}
 model1="${mdiag_model_prefix}_${year}_m${mon}_";
 days="JUN"; obsday="JUN"; plpfn="${runid}_${year}_m${mon}_";

 # atmos_file and year_offset are only required for ts_save.dk
 # atmos_file="${mdiag_flabel_prefix}_${year}_m${mon}_ts"; year_offset="0";

 resol="128_64";
#
# General surface observations:
#
 obsfile="obs_sfc_${resol}_v2";
 obsfile2="pd_nmc_${resol}_1979_1988_m${mon}_";
 if [ "$mon" = "12" ] ; then
   obsfile2="pd_nmc_${resol}_1979_1987_m${mon}_";
 fi
#
# GPCP, ISCCP, NVAP, and Dasilva observational data:
#
# obsgpcp=pooled_88_96_gpcp_${resol};
 obsgpcp=pooled_79_97_xie_arkin_${resol};
 obsisccp=pooled_84_90_isccpc2_${resol};
 obspw=pooled_88_92_nvap_${resol};
 obsbeg=pooled_${resol}_dasilva_obs;
#
  mask=land_mask_${resol};
#
 tlabel="tph";   tendlbl=" ";
 t1="        1"; t2="999999999"; t3="   1"; s3="   1";
 r1="        1"; r2="999999999"; r3="   1";
 g1="        1"; g2="999999999"; g3="   1";
 a1="        1"; a2="999999999"; a3="   1";
 lml="  995";  d="B";
 lay="    2";   coord=" ET15"; topsig="-1.00"; moist=" QHYB"; plid="      50.0";
 sref="   7.15E-3"; spow="        1.";
 m01="  012"; itrvar="QHYB";
 xref_LWC="        0."; xpow_LWC="        1.";
 xref_IWC="        0."; xpow_IWC="        1.";
 xref_BCO="        0."; xpow_BCO="        1.";
 xref_BCY="        0."; xpow_BCY="        1.";
 xref_OCO="        0."; xpow_OCO="        1.";
 xref_OCY="        0."; xpow_OCY="        1.";
 xref_SSA="   1.05E-8"; xpow_SSA="        1.";
 xref_SSC="   7.59E-8"; xpow_SSC="        1.";
 xref_DUA="   1.54E-7"; xpow_DUA="        1.";
 xref_DUC="   2.19E-7"; xpow_DUC="        1.";
 xref_DMS="  2.48E-10"; xpow_DMS="        1.";
 xref_SO2="   1.88E-9"; xpow_SO2="        1.";
 xref_SO4="   1.48E-9"; xpow_SO4="        1.";
 xref_HPO="        0."; xpow_HPO="        1.";
 xref_H2O="        0."; xpow_H2O="        1.";
 plv="   17"; p01="   10"; p02="   20"; p03="   30"; p04="   50"; p05="   70";
            p06="  100"; p07="  150"; p08="  200"; p09="  250"; p10="  300";
            p11="  400"; p12="  500"; p13="  600"; p14="  700"; p15="  850";
            p16="  925"; p17=" 1000"; p18="     "; p19="     "; p20="     ";
            p21="     "; p22="     "; p23="     "; p24="     "; p25="     ";
            p26="     "; p27="     "; p28="     "; p29="     "; p30="     ";
            p31="     "; p32="     "; p33="     "; p34="     "; p35="     ";
            p36="     "; p37="     "; p38="     "; p39="     "; p40="     ";
            p41="     "; p42="     "; p43="     "; p44="     "; p45="     ";
            p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
 kax="    1"; kin="    1"; lxp="    0"; map="    0"; b="  +";

 dtime="100"; gptime="600"; stime="600";

 plunit=VIC;
 lopgm="lopgm_o4xa";
 this_host=`hostname|cut -d'.' -f1`
 case $this_host in
   # This is pollux, use 64 bit binaries
   ib3-*) lopgm=lopgm_o4xal ;;
 esac

 oldiag="diag4";

 CCRNTMP=$CCRNTMP

 memory1="200mb"; memory2="200mb"; memory3="200mb";
 lrt="   63"; lmt="   63"; typ="    2";
 lon="  128"; lat="   64"; npg="    2";
 ncx="    2"; ncy="    2";
 delt="    900.0";

# MAM stuff
# r="       1.5"; etatop="   5.00E-4";
#
# pmaxl=" 1000"; pmin="-0100"; pmax=" 1000";

#  * ............................ Condef Parameters ............................

join=1
auto=on
nextjob=on
noprint=on
tnoprint=on
datatype=specsig
gcmtsav=on
gcm2plus=on
wxstats=on
xtracld=off
obsdat=off
xtradif=on
colourplots=on
shade=off
begplot=off
amip2=off
tprhs=off
qprhs=off
uprhs=off
vprhs=off
rhsplot=off
debug=off
lmstat=off

carbon=off
ctem=off
slab=off

tmpsave=on

splitfiles=on       # switch on the splitting mode.

#  * ............................. Deck Definition .............................

# --------------------------------- Run-time diagnostics.
. rtdiag35.dk
# --------------------------------- Compute beta/del and process MODEL INFO records.
. del.dk
. modinfo.dk
# --------------------------------- Interpolate model data on pressure levels.
. gpint.dk
# --------------------------------- Basic model statistics.
. mslpr_eta.dk
. gpstats.dk
# --------------------------------- Spectral statistics (optional, for reference runs only)
#. spvdqtz.dk
#. spmodinfo.dk
#. sphum.dk
#. spvort.dk
#. spdiv.dk
#. sptemp.dk
#. spintr.dk
#. vdelphi.dk
#. sppurg.dk
# --------------------------------- Tracer statistics (optional).
# #par_def
#   trac="X001 X002 X003 X004 X005 X006 X007 X008";
# . xstats.dk
# --------------------------------- Delete pressure-level files.
. gppurg.dk
# --------------------------------- Model physics statistics.
. mom_stat.dk
. eng_stat3.dk
. wat_stat.dk
. misc_stat.dk
. cloud8.dk
. sfc_stat.dk
. lmlstat.dk
# --------------------------------- Time series deck (optional)
#. ts_save.dk
# --------------------------------- Creation of cp file
. cp_save.dk
# --------------------------------- Creation of co file
#. co_save.dk
# --------------------------------- cleanup
. cleanup.dk

#end_of_job
