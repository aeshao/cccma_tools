#!/bin/sh
#=======================================================================
# Create time series from diagnostic or gz files        --- spltdiag ---
# $Id: spltdiag_jobdef 672 2012-05-08 19:28:23Z acrnrls $
#=======================================================================
# Read a set of diagnostic files and create time series for each
# superlabeled variable defined therein.
# This module simply defines appropriate parameters and calls spltdiag
#
# L.Solheim Nov 31,2010
#=======================================================================
#
#     keyword :: spltdiag
# description :: create time series from diagnostic or gz files
#
  set -a
  . betapath2

#  * ........................... Parmsub Parameters ............................

  runid="job000"; nqsprfx="${runid}_"; nqsext='';
  crawork="${runid}_spltdiag"

  # These variables are set when the job string is created
  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

  # Temporary directory where this script will run
  spltdiag_CCRNTMP=''
  CCRNTMP=${spltdiag_CCRNTMP:=$CCRNTMP}

  # RUNPATH on the execution machine
  spltdiag_RUNPATH=''
  RUNPATH=${spltdiag_RUNPATH:=$RUNPATH}

  # Alternate path to a directory where .queue/.crawork will be found
  JHOME=''

  if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
    # Allow optional reset of DATAPATH/RUNPATH
    JHOME_DATA=''
    DATAPATH=${JHOME_DATA:=$DATAPATH}
    RUNPATH=${JHOME_DATA:=$RUNPATH}
    # Allow optional reset of CCRNTMP
    JHOME_RUN=''
    CCRNTMP=${JHOME_RUN:=$CCRNTMP}
  fi

  jobname=spltdiag
  stime="3600"; memory1="32000mb"; lopgm="lopgm";
  username="acrnxxx"; user="XXX";

  nextjob=on
  noprint=on

  # ---Start_submit_ignore_code----

  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  error_out="${JHOME:-$HOME}/.queue/error_spltdiag_${runid}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" --- spltdiag: $*"
    echo_e `date`" --- spltdiag: $*" >>$error_out
    exit 1
  }

  # If either reset_start_year or reset_stop_year are set then they must be
  # of the form old_year:new_year (ie a colon separated pair of integers)
  # where the first integer is the year that needs to be changed
  # and the second integer is the year that it will be changed to.
  # These may potentially change the value of start_year or stop_year that
  # are defined after the call to make_file_name_list below
  spltdiag_reset_start_year=''
  reset_start_year=${spltdiag_reset_start_year:=''}
  spltdiag_reset_end_year=''
  spltdiag_reset_stop_year=$spltdiag_reset_end_year
  reset_stop_year=${spltdiag_reset_stop_year:=''}

  # This invocation of make_file_name_list will process the *_year and *_months
  # variables defined above and output a file containing definitions for
  # start_year, start_mon, stop_year, stop_mon
  tmp_file_list="spltdiag_date_list_${runid}_${stamp}"
  make_file_name_list --dates_only $tmp_file_list 2>&1 ||\
    bail "Problem in make_file_name_list"

  # Verify that the output list is not empty
  [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

  # A file list was created ...source it
  # This will define start_year, start_mon, stop_year, stop_mon
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  # Define a set of dates that may be modified by the user
  spltdiag_start_year=$start_year
  spltdiag_start_mon=$start_mon
  spltdiag_stop_year=$stop_year
  spltdiag_stop_mon=$stop_mon

  # Define the start and stop dates used by spltdiag
  time_start_year=$spltdiag_start_year
  time_start_month=$spltdiag_start_mon
  time_stop_year=$spltdiag_stop_year
  time_stop_month=$spltdiag_stop_mon

  # User defined date
  spltdiag_start_date=''
  spltdiag_stop_date=''

  # Input diagnostic files are of the form
  #   ${diag_uxxx}_${runid}_${yr}_m${mon}_$sfx
  # where $sfx is a member of the space separated list $diag_suffix_list
  # and yr and mon are determined from the range defined by the variables
  # time_start_year, time_start_month, time_stop_year, time_stop_month
  uxxx='dc'; diag_uxxx=$uxxx; spltdiag_uxxx=$diag_uxxx
  spltdiag_suffix_list=''
  diag_suffix_list=${spltdiag_suffix_list:='gp cp'}

  # By convention, spltdiag_uxxx should take precedence over the more
  # generic diag_uxxx. Unfortunately spltdiag.dk uses diag_uxxx rather
  # than spltdiag_uxxx to define the prefix. The following line will
  # fix this problem by redefining diag_uxxx while hiding this define
  # from cccjob substitution.
  eval diag\_uxxx\=$spltdiag_uxxx

  # mts_uxxx is used by spltdiag to determine the prefix
  # of the output time series files
  spltdiag_out_uxxx=''
  spltdiag_mts_uxxx=$spltdiag_out_uxxx
  mts_uxxx=${spltdiag_mts_uxxx:='sc'}

  # spltdiag_vars may be used to extract a subset of the variables
  # found in the input files. If set, spltdiag_vars must be a whitespace
  # separated list of superlabels found in the input files.
  # If not set then all superlabled sets are extracted.
  spltdiag_vars=''

  # This will not work for a "cloned" job because the variable name
  # will be replaced with another name (e.g. spltdiag_2_vars).
  # The following eval will hide the substitution done during cloning in cccjob and
  # allow the desired definition of spltdiag_vars to appear in this environment.
  eval splt\diag_vars\='${spltdiag_vars}'

  # use_input_ibuf2 = off means use YYYYMM format for ibuf2 in output time series files
  # use_input_ibuf2 =  on means use the ibuf2 value found in the corresponding input file
  spltdiag_use_input_ibuf2=''
  use_input_ibuf2=${spltdiag_use_input_ibuf2:=off}

#  * ............................ Condef Parameters ............................

  # If delete_diag_files is set to "on" then spltdiag
  # will delete all the input (diagnostic) files
  delete_diag_files=off

  # ---Stop_submit_ignore_code----

#  * ............................. Deck Definition .............................

# --------------------------------- split diagnostic files into time series.
  . spltdiag.dk

#end_of_job
