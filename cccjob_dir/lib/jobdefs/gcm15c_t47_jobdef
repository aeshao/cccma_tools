#!/bin/sh
#=======================================================================
# gcm15c model submission job                             --- gcm15c ---
# $Id: gcm15c_t47_jobdef 267 2008-03-31 21:54:13Z acrnrls $
#=======================================================================
#     keyword :: gcm15c
# description :: gcm15c model job string -- T47 35 levels
#
# Variables that must be set:
#   username, year, mon, year_restart, mon_restart, kfinal
# Variables that should be set:
#   runid, uxxx, user, months
#
# To run from initial conditions:
#   samerun = off   # the model will be compiled
#   initsp = on     # run "inphys" and "inatmo" decks and save "an" file
#   set year_restart and year to the same value
#   set mon_restart  and mon  to the same value
#   set kfinal = "      2232" if running for 1 month or more
#
#******************************
#********* WARNING ************
#******************************
#
# auto, nonqs, nextjob, flabel, RCMRUN, runid, run, nqsext, crawork,
# mdest, [sgd]time, nnodex, nprocx, initmem, sv, vic, memory[0-9],
# queue, jobname, gcmjcl
#
# These variables are special in that they are extracted from the
# submission script prior to execution and used to parse file names,
# among other things. Therefore any of these variables whose definition
# depends on another variable that is not in this list will be improperly
# defined in this preprocessing step. This could lead to file names being
# incorrectly specified. The variables are extracted by copying the entire
# line on which they are defined from the submission script. Therefore
# variables that are used to define any of these variables must be defined
# on the same line as that variable and prior to it or on a previous line
# that contains any of these variables.
#
#******************************

set -a
. betapath2

#  * ........................... Parmsub Parameters ............................

runid="job000"; prefix="${uxxx}_${runid}"; crawork=${runid}_job;
RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;
username="acrnxxx"; user="XXX"; uxxx='uxxx'; nqsext="_${runid}";

modver=gcm15c
gcmparm=gcmparm ; stime=900 ; memory1=1000mb
lopgm=lopgm_r8i8; oldiag=diag4;

#  * ............................ Condef Parameters ............................

samerun=on
script=off
openmp=on
nproc_a=4
nnode_a=12
parallel_io=off
noprint=on
nextjob=on

#  * ............................. Deck Definition .............................

. gcmsub.dk

cat > Model_Input <<'end_of_data'

### gcmparm ###

### parmsub ###

 . betapath2

#  * ........................... Parmsub Parameters ...........................

 runid="job000"; prefix="${uxxx}_${runid}"; crawork=${runid}_job;
 RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;
 username="acrnxxx"; user="XXX"; uxxx='uxxx'; nqsext="_${runid}";

 year="yyy"; mon="mm"; year_restart="yyy"; mon_restart="mm"; kfinal="0000000000";
 model=${prefix}_${year}_m${mon}_;
 start=${prefix}_${year_restart}_m${mon_restart}_;

 run="${RUNID}_${year}_M${mon}";

 initime=900; initmem="1000mb";
 gcmtime=10800; gcmmem="15000mb";

 iyear=" 1979"; iday="  001"; gmt=" 0.00"; incd="    1";
 lopgm=lopgm_r8i8;
 isoc="    1"; ires="    1"; resid="RESIDLF";
 ifdiff="    0"; isen="    1";
 issp="   36"; isgg="   36"; israd="   72"; isbeg="   72";
 jlatpr="    0";icsw="    3"; iclw="    3";  iepr="    0";  istr="   72";
 llphys=iph17lp;
 gpinit=ie791gp;
 inatmo=inatmo11;
 inphys=inphys13;
 llchem=ipchemlp;
 oxi=iphoxlp;
 mtnfile=optphis_t47_amip2;
 spfilt=hoskfilt_r_2_n38_t47;
 maskfile=gcm3_landmask_96x48v5;
 subfle=etopo5_gau_020_opt_200_97_48;
# datadest=pollux; datadir=/data/ccrn/r03

# Required parmsub parameters for gcmparm
# (* denotes a critical parameter which must appear in the ##PARC section)
#ex   * ilev   = number of vertical model    levels                (e.g.  35)
#ex   * levs   = number of vertical moisture levels                (e.g.  35)
#ex   * lonsld = number of longitudes         on the dynamics grid (e.g. 144)
#ex   * nlatd  = number of gaussian latitudes on the dynamics grid (e.g.  72)
#ex     lond   = number of grid points per slice for dynamics      (e.g. 144)
#ex   * lonsl  = number of longitudes         on the physics  grid (e.g.  96)
#ex   * nlat   = number of gaussian latitudes on the physics grid  (e.g.  48)
#ex     lon    = number of grid points per slice for physics       (e.g.  96)
#ex   * lmt    = spectral truncation wave number                   (e.g.  47)
#ex     ioztyp = switch for input ozone distribution               (e.g.   2)
#ex   * ntrac  = total number of tracers in the model              (e.g.  17)
#ex   * itraca = number of advected tracers in the model           (e.g.  12)
#ex     nnode_a= number of smp nodes used to run atm (mpi for nnode_a>1)
#
#       nnode_a is defined before gcmsub at the top of this script
#
    lon="   96";
   lond="  144";
 ioztyp="    2";

 ksteps=" 2232"; loop=1;
 iocean="00000";
 icemod="00000";
 months=6;

#  * .................... Begin Critical Parameters ...........................
##PARC

   ilev="   35"     ;   levs="   35"     ;    lmt="   47"     ;
  nlatd="   72"     ; lonsld="  144"     ;
   nlat="   48"     ;  lonsl="   96"     ;
  ntrac="   15"     ; itraca="    7"     ;
  coord=" ET15"     ;   plid="      50.0";
  moist=" QHYB"     ; itrvar="QHYB"      ;
   delt="   1200.0" ;    lay="    2"     ;

 g01="-0102";  g02="-0265";  g03="-0612";  g04="   13";  g05="   23";
 g06="   36";  g07="   52";  g08="   65";  g09="   82";  g10="  104";
 g11="  130";  g12="  164";  g13="  207";  g14="  261";  g15="  329";
 g16="  398";  g17="  466";  g18="  531";  g19="  592";  g20="  648";
 g21="  698";  g22="  742";  g23="  780";  g24="  813";  g25="  841";
 g26="  865";  g27="  885";  g28="  902";  g29="  916";  g30="  930";
 g31="  944";  g32="  958";  g33="  972";  g34="  986";  g35="  995";
 g36="     ";  g37="     ";  g38="     ";  g39="     ";  g40="     ";
 g41="     ";  g42="     ";  g43="     ";  g44="     ";  g45="     ";
 g46="     ";  g47="     ";  g48="     ";  g49="     ";  g50="     ";
 h01="-0102";  h02="-0265";  h03="-0612";  h04="   13";  h05="   23";
 h06="   36";  h07="   52";  h08="   65";  h09="   82";  h10="  104";
 h11="  130";  h12="  164";  h13="  207";  h14="  261";  h15="  329";
 h16="  398";  h17="  466";  h18="  531";  h19="  592";  h20="  648";
 h21="  698";  h22="  742";  h23="  780";  h24="  813";  h25="  841";
 h26="  865";  h27="  885";  h28="  902";  h29="  916";  h30="  930";
 h31="  944";  h32="  958";  h33="  972";  h34="  986";  h35="  995";
 h36="     ";  h37="     ";  h38="     ";  h39="     ";  h40="     ";
 h41="     ";  h42="     ";  h43="     ";  h44="     ";  h45="     ";
 h46="     ";  h47="     ";  h48="     ";  h49="     ";  h50="     ";

# namelists

   sref="   7.50E-3";   spow="        1.";
   ilaun=0;

# tracer names

  it01=LWC;  it02=IWC;  it03=BCO;  it04=BCY;  it05=OCO;
  it06=OCY;  it07=SSA;  it08=SSC;  it09=DUA;  it10=DUC;
  it11=DMS;  it12=SO2;  it13=SO4;  it14=HPO;  it15=H2O;

# tracer reference values (default=0 for all tracers)

  xref01=0.
  xref02=0.
  xref03=0.
  xref04=0.
  xref05=0.
  xref06=0.
  xref07=5.18E-9
  xref08=5.35E-8
  xref09=1.29E-7
  xref10=1.79E-7
  xref11=1.48E-10
  xref12=1.21E-9
  xref13=3.70E-10
  xref14=0.0
  xref15=0.0

# tracer power values (default=1 for all tracers)

# tracer advection: 1/0 flag for tracer advection (on/off)
  adv01=0;   adv02=0;  adv03=0;   adv04=0;    adv05=0;
  adv06=0;   adv07=1;  adv08=1;   adv09=1;    adv10=1;
  adv11=1;   adv12=1;  adv13=1;   adv14=0;    adv15=0;

# tracer physics: 1/0 flag for physics on tracer turned on/off
#                 phxx=1 by default for all tracers
  phs01=0;  phs02=0;  phs15=-1;

# wet deposition (default=0 for all tracers)
  wet01=0;
  wet02=0;
  wet03=-1;
  wet04=1;
  wet05=-1;
  wet06=1;
  wet07=1;
  wet08=1;
  wet09=1;
  wet10=1;
  wet11=0;
  wet12=1;
  wet13=1;
  wet14=0;
  wet15=0;

# dry deposition (default=0 for all tracers)
  dry01=0;
  dry02=0;
  dry03=1;
  dry04=1;
  dry05=1;
  dry06=1;
  dry07=1;
  dry08=1;
  dry09=1;
  dry10=1;
  dry11=0;
  dry12=1;
  dry13=1;
  dry14=0;
  dry15=0;

# convective chemistry (default=0 for all tracers)
  cch01=0;
  cch02=0;
  cch03=-1;
  cch04=1;
  cch05=-1;
  cch06=1;
  cch07=1;
  cch08=1;
  cch09=1;
  cch10=1;
  cch11=0;
  cch12=1;
  cch13=1;
  cch14=1;
  cch15=0;

# boundary condition concentrations (ppm)

# initial data for tracer?

#  * ...................... End Critical Parameters ...........................

### condef ###

initsp=off
openmp=on
noprint=off
nolist=off
acctcom=on
xmu=off
pakjob=off
debug=off
noxlfimp=off

### update script ###
 
%c gcmjcl

### update model ###

%c msizes
%c psizes_15b
%c gcm15c
%c core15pc
%c core15db
%c physicfc
%c rstartec
%c dyncal4d
%c class15c
%c spec_fwd_xfer
%c spec_bwd_xfer
%c wrap_gather
%c trinfo2

### update sub ###

end_of_data

. endjcl.cdk

#end_of_job

