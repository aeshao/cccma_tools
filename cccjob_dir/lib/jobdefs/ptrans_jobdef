#!/bin/sh
#=======================================================================
# Transfer files from the back end to a remote machine    --- ptrans ---
# and dump these same files to tape. These tasks are done in parallel
# $Id: ptrans_jobdef 652 2011-06-17 18:13:52Z acrnrls $
#=======================================================================
#
# Larry Solheim  Jun,2008
#
# Files transferred/dumped will be of the form
#    ${ptrans_prefix}_${year}_m${mon}_$suffix
# for all months from previous_year, previous_month to current_year,
# current_month or from current_year, current_month to next_year,
# next_month depending on which of previous_(year|month) or
# next_(year|month) are set.
#
# The variables current_year, current_month, previous_year and
# previous_month are set when the job string is created.
#
# A variable named ptrans_suffix_list may also be set to modify the list
# of files that will be transferred/dumped. See below for details.
#
# ptrans_prefix_ can be set to override the value of ptrans_prefix
# files transferred/dumped will then be of the form
#    ${ptrans_prefix_}${year}_m${mon}_$suffix
# note the missing underscore between ptrans_prefix_ and year.
#
# The user must set up public key authentication on the remote machine
# so that transport from pollux to the remote machine does not require
# a password. This means, create a public/private key pair on pollux
# using ssh-keygen (if nessecary) then copy the users public key from
# pollux and append it to ~user/.ssh/authorized_keys on the remote machine
#
# A default remote user, remote machine and remote directory into which
# files are to be copied is set in the rtrans script (invoked below).
# The user can define rtrans_remusr, rtrans_remserver and rtrans_remdir
# on the cccjob command line which will override the default values.
#=======================================================================
#
#     keyword :: ptrans
# description :: transfer from back end to a remote machine + dump to tape
#
 set -a
 . betapath2

 username=acrnxxx; user=MYNAME; crawork=ptrans_job

 nextjob=on
 noprint=on
 debug=off

 stime=${stime:=7200}; memory1=${memory1:=900mb}
 jobname=ptrans; time=$stime ; memory=$memory1;

 # It is more efficient to set CCRNTMP = $RUNPATH if all files
 # to be transferred are on $RUNPATH
 CCRNTMP=$CCRNTMP

 # Alternate path to a directory where .queue/.crawork will be found
 JHOME=''

 if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
   # Allow optional reset of DATAPATH/RUNPATH
   JHOME_DATA=''
   DATAPATH=${JHOME_DATA:=$DATAPATH}
   RUNPATH=${JHOME_DATA:=$RUNPATH}
   # Allow optional reset of CCRNTMP
   JHOME_RUN=''
   CCRNTMP=${JHOME_RUN:=$CCRNTMP}
 fi

 # ptrans_ssh_args may be used to supply additional
 # command line arguments to the ssh commands used below.
 # This is only useful for debugging.
 ptrans_ssh_args=''

 # This is a debugging switch. Turning ptrans_dry_run on will prevent any
 # copy or resubmission.
 ptrans_dry_run=0

 # ptrans_with_rtrans is used to control inclusion of the remote copy
 # If ptrans_with_rtrans is set to "off" then only the tdumper job will run.
 ptrans_with_rtrans=on
 XXX=`echo $ptrans_with_rtrans|sed 's/ //g'`
 eval ptrans_with_rtrans\=$XXX
 [ "$ptrans_with_rtrans" = 'on' ]  && eval ptrans_with_rtrans\=1
 [ "$ptrans_with_rtrans" = 'off' ] && eval ptrans_with_rtrans\=0
 [ "$ptrans_with_rtrans" = 'yes' ] && eval ptrans_with_rtrans\=1
 [ "$ptrans_with_rtrans" = 'no' ]  && eval ptrans_with_rtrans\=0

 # ptrans_with_mdump is used to control inclusion of the tape dump job
 # If ptrans_with_mdump is set to "off" then only the rtrans job will run.
 ptrans_with_mdump=on
 XXX=`echo $ptrans_with_mdump|sed 's/ //g'`
 eval ptrans_with_mdump\=$XXX
 [ "$ptrans_with_mdump" = 'on' ]  && eval ptrans_with_mdump\=1
 [ "$ptrans_with_mdump" = 'off' ] && eval ptrans_with_mdump\=0
 [ "$ptrans_with_mdump" = 'yes' ] && eval ptrans_with_mdump\=1
 [ "$ptrans_with_mdump" = 'no' ]  && eval ptrans_with_mdump\=0

 # ptrans_with_diag is used to control inclusion of the diagnostic job
 # If ptrans_with_diag is set to "off" then no diagnostic job is launched.
 ptrans_with_diag=off
 XXX=`echo $ptrans_with_diag|sed 's/ //g'`
 eval ptrans_with_diag\=$XXX
 [ "$ptrans_with_diag" = 'on' ]  && eval ptrans_with_diag\=1
 [ "$ptrans_with_diag" = 'off' ] && eval ptrans_with_diag\=0
 [ "$ptrans_with_diag" = 'yes' ] && eval ptrans_with_diag\=1
 [ "$ptrans_with_diag" = 'no' ]  && eval ptrans_with_diag\=0

 # If ptrans_use_scp is set to "on" then scp will be used for transport
 # to the gateway machine. The default if ptrans_use_scp = off is to use
 # nrcp for transport.
 ptrans_use_scp=on
 XXX=`echo $ptrans_use_scp|sed 's/ //g'`
 eval ptrans_use_scp\=$XXX
 [ "$ptrans_use_scp" = 'on' ]  && eval ptrans_use_scp\=1
 [ "$ptrans_use_scp" = 'off' ] && eval ptrans_use_scp\=0
 [ "$ptrans_use_scp" = 'yes' ] && eval ptrans_use_scp\=1
 [ "$ptrans_use_scp" = 'no' ]  && eval ptrans_use_scp\=0

 . comjcl.cdk
 cat > Execute_Script <<'end_of_script'

  # bail is a simple error exit routine
  bail(){
    echo "ptrans: "$1
    echo " " >> haltit
    exit 1
  }

  username=acrnxxx; user=MYNAME; crawork=ptrans_job
  runid=job000; uxxx=uxxx; ptrans_uxxx=$uxxx;
  ptrans_prefix=${ptrans_uxxx}_${runid}; ptrans_prefix_=${ptrans_prefix}_

  noprint=on
  nextjob=on

  # Indicate how cccjob should be invoked
  # Setting CCCJOB_ROOT will allow a job specific version of cccjob to used
  CCCJOB_ROOT=''
  if [ -z "$CCCJOB_ROOT" ]; then
    CCCJOB_ENV=''
  else
    eval CCCJOB_ENV=\'env CCCJOB_ROOT\=$CCCJOB_ROOT\'
  fi

  # ptrans_suffix_list is a white space separated list of suffixes
  # of file names to be transferred/dumped. Any suffix in this list may be
  # modified by appending a + followed by a comma separated list of
  # numbers (no white space is allowed within this modifier). Each
  # number within the modifier list will correspond to a month (1-12)
  # for which a file with this suffix is to be included. If the
  # modifier exists for a particular suffix then only those months
  # indicated in the modifier will be added to the file list.
  # e.g. the inclusion of rs+12 in this list will copy only DEC restarts
  mdump_suffix_list=''
  ptrans_suffix_list=${mdump_suffix_list:='gs ss td gz cm ab+12 ob+12 an+12 rs+12 cs+12 os+12'}

  # ptrans_suffix_ flags the addition of an underscore between the
  # suffix and the month (e.g. m01_gs) in file names generated here
  ptrans_suffix_=1
  XXX=`echo $ptrans_suffix_|sed 's/ //g'`
  eval ptrans_suffix_\=$XXX
  [ "$ptrans_suffix_" = 'on' ]  && eval ptrans_suffix_\=1
  [ "$ptrans_suffix_" = 'off' ] && eval ptrans_suffix_\=0
  [ "$ptrans_suffix_" = 'yes' ] && eval ptrans_suffix_\=1
  [ "$ptrans_suffix_" = 'no' ]  && eval ptrans_suffix_\=0

  # These variables are set when the job string is created
  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet
  current_year=NotSet
  current_month=NotSet
  previous_year=NotSet
  previous_month=NotSet
  next_year=NotSet
  next_month=NotSet
  [ $current_year = "NotSet" -o $current_month = "NotSet" ] && \
    bail "current_year or current_month is not set"

  # files transferred will be for months in the range from
  # ptrans_start_year,ptrans_start_mon to ptrans_end_year,ptrans_end_mon
  if [ $previous_year = "NotSet" -o $previous_month = "NotSet" ]; then
    if [ $next_year = "NotSet" -o $next_month = "NotSet" ]; then
      bail "Neither previous_(year|month) nor next_(year|month) are set"
    else
      ptrans_start_year=$current_year
      ptrans_start_mon=$current_month
      ptrans_end_year=$next_year
      ptrans_end_mon=$next_month
    fi
  else
    ptrans_start_year=$previous_year
    ptrans_start_mon=$previous_month
    ptrans_end_year=$current_year
    ptrans_end_mon=$current_month
  fi

  ptrans_start_year=`echo $ptrans_start_year|awk '{printf "%3.3d",$1}' -`
  ptrans_start_mon=`echo $ptrans_start_mon|awk '{printf "%2.2d",$1}' -`
  ptrans_end_year=`echo $ptrans_end_year|awk '{printf "%3.3d",$1}' -`
  ptrans_end_mon=`echo $ptrans_end_mon|awk '{printf "%2.2d",$1}' -`

  [ -z "$ptrans_start_year" ] && bail "ptrans_start_year is null"
  [ -z "$ptrans_end_year" ]   && bail "ptrans_end_year is null"
  [ -z "$ptrans_start_mon" ]  && bail "ptrans_start_mon is null"
  [ -z "$ptrans_end_mon" ]    && bail "ptrans_end_mon is null"
  [ $ptrans_start_mon -gt 12 -o $ptrans_start_mon -lt 1 ] &&\
    bail "ptrans_start_mon=$ptrans_start_mon is out of range"
  [ $ptrans_end_mon -gt 12 -o $ptrans_end_mon -lt 1 ] &&\
    bail "ptrans_end_mon=$ptrans_end_mon is out of range"
  [ $ptrans_start_year -gt $ptrans_end_year ] &&\
    bail "ptrans_start_year=$ptrans_start_year is out of range"

  # Generate a sequence of file names to transfer
  ptrans_curr_year=`echo $ptrans_start_year|awk '{y=$1-1;printf "%3.3d", y}' -`
  nfile=0
  while [ $ptrans_curr_year -lt $ptrans_end_year ]; do
    ptrans_curr_year=`echo $ptrans_curr_year|awk '{y=1+$1;printf "%3.3d", y}' -`
    if [ $ptrans_curr_year -eq $ptrans_start_year ]; then
      mm=`echo $ptrans_start_mon|awk '{m=$1-1;printf "%2.2d", m}' -`
    else
      mm=0
    fi
    if [ $ptrans_curr_year -eq $ptrans_end_year ]; then
      mm_end=$ptrans_end_mon
    else
      mm_end=12
    fi
    while [ $mm -lt $mm_end ]; do
      mm=`echo $mm|awk '{m=1+$1;printf "%2.2d", m}' -`
      if [ "$ptrans_suffix_" = '1' ]; then
        bname=${ptrans_prefix_}${ptrans_curr_year}_m${mm}_
      else
        bname=${ptrans_prefix_}${ptrans_curr_year}_m${mm}
      fi
      for suffix in $ptrans_suffix_list; do
        mlist=`echo $suffix|awk -F'+' '{print $2}' -`
        mlist=`echo $mlist|sed 's/,/ /g'`
        if [ -n "$mlist" ]; then
          suffix=`echo $suffix|sed 's/+.*$//'`
          # assume that mlist is a white space separated list of numbers
          # indicating which months to dump
          for xx in $mlist; do
            if [ $xx -eq $mm ]; then
              nfile=`echo $nfile|awk '{printf "%d",$1+1}' -`
              nfile=`echo $nfile|sed -e 's/^ *//' -e 's/^0*//'`
              eval file${nfile}=$bname$suffix
            fi
          done
        else
          nfile=`echo $nfile|awk '{printf "%d",$1+1}' -`
          nfile=`echo $nfile|sed -e 's/^ *//' -e 's/^0*//'`
          eval file${nfile}=$bname$suffix
        fi
      done
    done
  done

  # These assignments of join and nfile are required here in case the user
  # requests a list of files that override the file names generated above.
  # If there is a user supplied list it will redefine join appropriately.
  join=$nfile
  # <<INSERT_PTRANS>>
  nfile=$join

  # gateway is the name of the machine to which files
  # are sent prior to being moved off site
  ptrans_gateway=''
  gateway=${ptrans_gateway:='pollux'}

  # Get the name of the machine running this script
  this_mach=`uname -n|awk -F\. '{print \$1}' -`

  # running_on_gateway, if set, indicates that this script
  # is running on the gateway machine
  running_on_gateway=0
  if [ "$this_mach" = "$gateway" ]; then
    running_on_gateway=1
    echo "ptrans is running on $gateway"
  fi

  # Define a unique stamp for use in file names etc
  stamp=`date "+%j"$$`

  # Extra ssh args (use -v for debugging connections)
  ssh_args=${ptrans_ssh_args:-''}

  # save_on_gateway flags "saving" files on the gateway machine
  # Normally files are copied into a tmp dir on gateway then saved
  # with the save command. Setting save_on_gateway=0 (or off or no)
  # allows the copy to the tmp dir but does not allow the save.
  save_on_gateway=1
  XXX=`echo $save_on_gateway|sed 's/ //g'`
  eval save_on_gateway\=$XXX
  [ "$save_on_gateway" = 'on' ]  && eval save_on_gateway\=1
  [ "$save_on_gateway" = 'off' ] && eval save_on_gateway\=0
  [ "$save_on_gateway" = 'yes' ] && eval save_on_gateway\=1
  [ "$save_on_gateway" = 'no' ]  && eval save_on_gateway\=0

  # Create a temporary dir on the gateway machine
  if [ $running_on_gateway -eq 1 ]; then
    # If this script is running on the gateway machine
    # then set rem_dir to a local dir name and create it
    remdir=$CCRNTMP/tmp$stamp
    mkdir -p $remdir
  else
    # If this script is not running on the gateway machine
    # then create a tmp dir on the gateway machine
    fail=0
    remdir=`ssh $ssh_args $gateway 'echo $CCRNTMP'` 2>&1 || fail=$?
    if [ $fail -ne 0 ]; then
      bail "***ERROR*** Unable to determine remote dir name.\n"
    fi
    remdir=$remdir/tmp$stamp
    echo "remdir=$remdir"

    fail=0
    ssh $ssh_args $gateway "mkdir -p $remdir" 2>&1 || fail=$?
    if [ $fail -ne 0 ]; then
      bail "***ERROR*** Unable to create remote dir $remdir\n"
    fi
  fi

  # Copy files from the local machine to the gateway machine
  nsent=0
  rm -f file_list
  touch file_list
  while [ $nsent -lt $nfile ]; do
    nsent=`expr $nsent + 1`
    eval local_file=\$file$nsent
    [ -z "$local_file" ] && bail "Invalid file name in file$nsent"

    if [ $ptrans_dry_run -ne 1 ]; then
      # Copy files unless this is a dry run
      access $local_file $local_file na

      # This may or may not be useful, wait and see
      # # Abort if this file is missing or empty and it does not have a 'ts' suffix
      # ts_sfx=`echo $local_file|sed -n 's/.*\(ts\)$/\1/p'`
      # [ ! -s "$local_file" -a -z "$ts_sfx" ] && bail "Missing or empty file $local_file"

      # Abort if this file is missing or empty
      [ ! -s "$local_file" ] && bail "Missing or empty file $local_file"

      if [ $running_on_gateway -eq 1 ]; then
        # If this script is running on the gateway machine then there is
        # no need for this remote copy. Simply release the accessed file.
        release $local_file
      else
        # If this script is not running on the gateway machine then copy
        # local files to the gateway machine.
        rem_file=${gateway}:$remdir/$local_file

        if [ $ptrans_use_scp -eq 1 ]; then
          # scp will work from most machines whereas nrcp will not
          fail=0
          scp_xcompress='-o NoneSwitch=yes'
          scp $scp_xcompress $local_file $rem_file 2>&1 || fail=$?
          if [ $fail -ne 0 ]; then
            bail "***ERROR*** Unable to (scp) copy $local_file to $remdir\n"
          fi
        else
          # nrcp is ~3 times faster than scp going from the backend to pollux
          fail=0
          nrcp -b 4M $local_file $rem_file 2>&1 || fail=$?
          if [ $fail -ne 0 ]; then
            bail "***ERROR*** Unable to (nrcp) copy $local_file to $remdir\n"
          fi
        fi
        release $local_file

        if [ $save_on_gateway -ne 0 ]; then
          # save this file on the gateway machine
          fail=0
          ssh $ssh_args $gateway "cd $remdir; save $local_file $local_file" 2>&1 || fail=$?
          [ $fail -ne 0 ] && \
            bail "***ERROR*** Unable to save $local_file on $remdir\n"

          # delete (release) this file from the tmp dir on the gateway machine
          fail=0
          ssh $ssh_args $gateway "cd $remdir; release $local_file" 2>&1 || fail=$?
          [ $fail -ne 0 ] && \
            bail "***ERROR*** Unable to release $local_file on $remdir\n"
        fi
      fi
    fi

    # Append this file name to a file containing a list of files copied
    # Create this list in a format that may be inserted directly into a
    # a delete job
    echo "  file${nsent}=$local_file" >> file_list
  done

  # Append the number of files to file_list
  echo "  join=$nfile" >> file_list

  if [ $ptrans_dry_run -ne 1 ]; then
    # Clean up on the gateway machine unless this is a dry run
    if [ $save_on_gateway -ne 0 -a $running_on_gateway -eq 0 ]; then
      # Remove the temporary dir on the gateway machine
      fail=0
      ssh $ssh_args $gateway "rm -fr $remdir" 2>&1 || fail=$?
      [ $fail -ne 0 ] && \
        bail "***ERROR*** Unable to delete $remdir on $gateway\n"
    fi
  fi

  # Once all the files have been transferred, create a job to dump them
  # to tape, a job to transfer them to a remote machine and a job to run
  # diagnostics (assuming history files are being dumped).

  # ym_range is used as part of some file names defined below
  ym_range="${ptrans_start_year}m${ptrans_start_mon}_${ptrans_end_year}m${ptrans_end_mon}"

  # Define partial file names for the dump, rtrans and diag jobs that will be
  # used in multiple places below
  fdump=mdump_${runid}_$ym_range
  ftran=rtrans_${runid}_$ym_range
  fdiag=diag_${runid}_$ym_range

  # Define names of lock files for each of tape/transfer/diag jobs

  if [ $running_on_gateway -eq 1 ]; then
    # If this script is running on the gateway machine
    # then use $HOME for rem_dir
    rem_home=${JHOME:-$HOME}
  else
    # If this script is not running on the gateway machine
    # then set rem_dir to the users home dir on the gateway machine
    fail=0
    rem_home=`ssh $gateway 'echo $HOME'` 2>&1 || fail=$?
    if [ $fail -ne 0 ]; then
      bail "***ERROR*** Unable to determine remote home directory.\n"
    fi
  fi
  mdump_lock="${rem_home}/.queue/.crawork/lock_${fdump}_$stamp"
  rtrans_lock="${rem_home}/.queue/.crawork/lock_${ftran}_$stamp"
  diag_lock="${rem_home}/.queue/.crawork/lock_${fdiag}_$stamp"
  mdump_string="${rem_home}/.queue/.crawork/${fdump}_${stamp}_string"
  rtrans_string="${rem_home}/.queue/.crawork/${ftran}_${stamp}_string"
  diag_string="${rem_home}/.queue/.crawork/${fdiag}_${stamp}_string"

  if [ $ptrans_dry_run -ne 1 ]; then
    # Create the lock files unless this is a dry run
    if [ $running_on_gateway -eq 1 ]; then
      # If this script is running on the gateway machine
      # then create the lock files on this machine
      [ $ptrans_with_mdump -eq 1 ]  && cp -f file_list $mdump_lock
      [ $ptrans_with_rtrans -eq 1 ] && cp -f file_list $rtrans_lock
      [ $ptrans_with_diag -eq 1 ] && cp -f file_list $diag_lock
    else
      # If this script is not running on the gateway machine
      # then create the lock files on the gateway machine
      if [ $ptrans_with_mdump -eq 1 ]; then
        fail=0
        scp file_list ${gateway}:$mdump_lock 2>&1 || fail=$?
        if [ $fail -ne 0 ]; then
          bail "***ERROR*** Unable to create lock file $mdump_lock\n"
        fi
      fi
      if [ $ptrans_with_rtrans -eq 1 ]; then
        fail=0
        scp file_list ${gateway}:$rtrans_lock 2>&1 || fail=$?
        if [ $fail -ne 0 ]; then
          bail "***ERROR*** Unable to create lock file $rtrans_lock\n"
        fi
      fi
      if [ $ptrans_with_diag -eq 1 ]; then
        fail=0
        scp file_list ${gateway}:$diag_lock 2>&1 || fail=$?
        if [ $fail -ne 0 ]; then
          bail "***ERROR*** Unable to create lock file $diag_lock\n"
        fi
      fi
    fi
  fi
  
  # Each of the tape job, the rtrans job and the diag job string will contain
  # a final job in the string that will delete the files, but not until all
  # jobs have completed.

  # This eval is necessary to prevent the shebang line from being
  # blindly replaced with a different shell by submit3
  eval shebang=\#\!/\bin/\sh
  cat > del_part1 <<EOF
$shebang
  set -a
  . betapath2
  jobname=del_list; crawork=delete_job; username=acrnxxx; user=XXX;

  #  * ........................... Parmsub Parameters ....................
  lopgm="lopgm"; stime="1800"; memory1="24mb";

EOF

  cat > del_part2_mdump <<EOF
  # Remove the mdump lock file
  rm -f $mdump_lock

  # If the rtrans or diag lock file exists then do not delete any files but remove
  # this mdump job string from the crawork directory
  if [ -f "$rtrans_lock" -o -f "$diag_lock" ]; then
    rm -f "$mdump_string"
    exit 0
  fi

EOF

  cat > del_part2_rtrans <<EOF
  # Remove the rtrans lock file
  rm -f $rtrans_lock

  # If the mdump or diag lock file exists then do not delete any files but remove
  # this rtrans job string from the crawork directory
  if [ -f "$mdump_lock" -o -f "$diag_lock" ]; then
    rm -f "$rtrans_string"
    exit 0
  fi

EOF

  cat > del_part2_diag <<EOF
  # Remove the diag lock file
  rm -f $diag_lock

  # If the mdump or diag lock file exists then do not delete any files but remove
  # this diag job string from the crawork directory
  if [ -f "$mdump_lock" -o -f "$rtrans_lock" ]; then
    rm -f "$diag_string"
    exit 0
  fi

EOF

  cat > del_part4 <<'EOF'

  #  * ............................ Condef Parameters ............................
  noprint=on
  nextjob=on

  #  * ............................. Deck Definition .............................
  . comjcl.cdk
  cat > Execute_Script <<'end_of_exec_script'
    nn=0
    while [ $nn -lt $join ]; do
      nn=`expr $nn + 1`
      eval FILE=\$file$nn
      # Note: this access will fail on an error, unlike delet.dk
      access $FILE $FILE nocp
      delete $FILE
    done
end_of_exec_script
EOF
  echo "  . endjcl.cdk" >> del_part4
  echo "#end_of_job"    >> del_part4

  cat del_part1 del_part2_mdump  file_list del_part4 > mdump_delete_job
  cat del_part1 del_part2_rtrans file_list del_part4 > rtrans_delete_job
  cat del_part1 del_part2_diag   file_list del_part4 > diag_delete_job

  # Require cccjob
  which cccjob || bail "cccjob is not in your path"

  # Define start/stop year/month cccjob options to be used below
  curr_year=`echo $ptrans_start_year|awk '{printf "%d",$1}' -`
  curr_mon=`echo $ptrans_start_mon|awk '{printf "%d",$1}' -`
  next_year=`echo $ptrans_end_year|awk '{printf "%d",$1}' -`
  next_mon=`echo $ptrans_end_mon|awk '{printf "%d",$1}' -`
  start_opt="--start_time=${curr_year}:$curr_mon"
  stop_opt="--stop_time=${next_year}:$next_mon"

  ######### mdump job ###########

  # These variable may be set when the ptrans job string is created
  # If a variable is set then it will be used in the mdump job

  # Define an arclabel for this tape dump
  hasmodel=0
  hasdiag=0
  for sfx in $ptrans_suffix_list; do
    [ "$sfx" = "gs" ] && hasmodel=1
    [ "$sfx" = "ss" ] && hasmodel=1
    [ "$sfx" = "gz" ] && hasmodel=1
    [ "$sfx" = "gp" ] && hasdiag=1
    [ "$sfx" = "xp" ] && hasdiag=1
    [ "$sfx" = "cp" ] && hasdiag=1
  done
  mdump_arclabel="${runid}_$ym_range"
  if [ $hasdiag -eq 1 -a $hasmodel -eq 0 ]; then
    # assume that this is diagnostic data
    mdump_arclabel="${runid}_diag_$ym_range"
  fi
  if [ $hasmodel -eq 1 -a $hasdiag -eq 0 ]; then
    # assume that this is model data
    mdump_arclabel="${runid}_model_$ym_range"
  fi
  mdump_uxxx=''
  mdump_prefix=''
  mdump_prefix_=''
  mdump_suffix_list="$ptrans_suffix_list"
  mdump_cfsuser=''
  cfsuser=''
  masterdir=off
  shortermdir=on
  # sv should always be on when gateway is pollux
  # sv on means dump from pollux disks
  eval sv\=on
  varlist='uxxx
           CCCJOB_ROOT
           JHOME
           JHOME_DATA
           JHOME_RUN
           runid
           mdump_uxxx
           mdump_prefix
           mdump_prefix_
           mdump_suffix_list
           mdump_cfsuser
           cfsuser
           masterdir
           shortermdir
           sv
           mdump_arclabel'

  # Create a file containing variable definitions used by the tape job

  # This eval will protect this crawork def from cccjob substitution
  eval craworkdef\=crawork\=${fdump}_$stamp
  cat > mdump_defs <<EOF
$craworkdef
EOF
  for var in $varlist; do
    eval val=\$$var
    # If this variable is defined add it to the list
    [ -n "$val" ] && echo ${var}=\'$val\' >> mdump_defs
  done

  # Create the tape job
  $CCCJOB_ENV cccjob --out=${fdump}_job --job='mdump:s mdump_delete_job:s' \
         $start_opt $stop_opt mdump_defs

  ######### rtrans job ###########

  # These variable may be set when the ptrans job string is created
  # If a variable is set then it will be used in the rtrans job
  rtrans_remusr=''
  rtrans_remserver=''
  rtrans_remdir=''
  rtrans_force=''
  rtrans_scp_args=''
  rtrans_ssh_args=''
  rtrans_getput=''
  rtrans_suffix_list='gs ss td gz'
  rtrans_uxxx=''
  rtrans_prefix=''
  rtrans_prefix_=''
  rtrans_suffix_=''
  varlist='uxxx
           CCCJOB_ROOT
           JHOME
           JHOME_DATA
           JHOME_RUN
           runid
           rtrans_remusr
           rtrans_remserver
           rtrans_remdir
           rtrans_force
           rtrans_scp_args
           rtrans_ssh_args
           rtrans_getput
           rtrans_suffix_list
           rtrans_uxxx
           rtrans_prefix
           rtrans_prefix_
           rtrans_suffix_'

  # Create a file containing variable definitions used by the rtrans job

  # This eval will protect this crawork def from cccjob substitution
  eval craworkdef\=crawork\=${ftran}_$stamp
  cat > rtrans_defs <<EOF
$craworkdef
EOF
  for var in $varlist; do
    eval val=\$$var
    # If this variable is defined add it to the list
    [ -n "$val" ] && echo ${var}=\'$val\' >> rtrans_defs
  done

  # Create the rtrans job
  $CCCJOB_ENV cccjob --out=${ftran}_job --job='rtrans:s rtrans_delete_job:s' \
         $start_opt $stop_opt rtrans_defs

  ######### diag job ###########

  # These variable may be set when the ptrans job string is created
  # If a variable is set then it will be used in the diag job
  diag_uxxx=''
  model_uxxx=''
  hres=''
  modver=''
  xref01=''
  xref02=''
  xref03=''
  xref04=''
  xref05=''
  xref06=''
  xref07=''
  xref08=''
  xref09=''
  xref10=''
  xref11=''
  xref12=''
  xref13=''
  xref14=''
  xref15=''
  varlist='runid
           CCCJOB_ROOT
           JHOME
           JHOME_DATA
           JHOME_RUN
           diag_uxxx
           model_uxxx
           hres
           modver
           xref01
           xref02
           xref03
           xref04
           xref05
           xref06
           xref07
           xref08
           xref09
           xref10
           xref11
           xref12
           xref13
           xref14
           xref15'

  # Create a file containing variable definitions used by the diag job

  # This eval will protect this crawork def from cccjob substitution
  eval craworkdef\=crawork\=${fdiag}_$stamp
  cat > diag_defs <<EOF
$craworkdef
EOF
  for var in $varlist; do
    eval val=\$$var
    # If this variable is defined add it to the list
    [ -n "$val" ] && echo ${var}=\'$val\' >> diag_defs
  done

  # Allow the user to supply their own diagnostic job
  # ptrans_diag_job, if set, should be a full pathname or exist
  # as ~/cccjobs/${ptrans_diag_job}_jobdef on the execution machine
  ptrans_diag_job=${ptrans_diag_job:='stdiag'}

  # Create the diag job
  $CCCJOB_ENV cccjob --out=${fdiag}_job --job="${ptrans_diag_job}:m diag_delete_job:s" \
         $start_opt $stop_opt diag_defs

  ######### submit jobs ###########

  # Submit these jobs to run on the gateway machine
  if [ $ptrans_dry_run -ne 1 ]; then
    # Submit jobs unless this is a dry run

    # Create a temporary directory in ~/tmp to hold submission files
    psubdir=${JHOME:-$HOME}/tmp/tmp_ptrans_$stamp
    mkdir -p $psubdir || bail "Cannot create $psubdir"

    if [ $ptrans_with_mdump -eq 1 ]; then
      # rsub mdest=$gateway ${fdump}_job
      this_remjob=${fdump}_job
      cp $this_remjob $psubdir
      fail=0
      ssh $ssh_args $gateway "cd ${psubdir}; rsub mdest=$gateway $this_remjob" 2>&1 || fail=$?
      if [ $fail -ne 0 ]; then
        bail "***ERROR*** Remote submission failed for $this_remjob\n"
      fi
    fi

    if [ $ptrans_with_rtrans -eq 1 ]; then
      #rsub mdest=$gateway ${ftran}_job
      this_remjob=${ftran}_job
      cp $this_remjob $psubdir
      fail=0
      ssh $ssh_args $gateway "cd ${psubdir}; rsub mdest=$gateway $this_remjob" 2>&1 || fail=$?
      if [ $fail -ne 0 ]; then
        bail "***ERROR*** Remote submission failed for $this_remjob\n"
      fi
    fi
    if [ $ptrans_with_diag -eq 1 ]; then
      # rsub mdest=$gateway ${fdiag}_job
      this_remjob=${fdiag}_job
      cp $this_remjob $psubdir
      fail=0
      ssh $ssh_args $gateway "cd ${psubdir}; rsub mdest=$gateway $this_remjob" 2>&1 || fail=$?
      if [ $fail -ne 0 ]; then
        bail "***ERROR*** Remote submission failed for $this_remjob\n"
      fi
    fi

    # Clean up the temporary directory
    rm -fr $psubdir

  fi

  # sucessful completion
  exit 0

end_of_script

 . endjcl.cdk

#end_of_job
