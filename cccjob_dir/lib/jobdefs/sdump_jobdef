#!/bin/sh
#=======================================================================
# Copy seasonal files to cfs                               --- sdump ---
# $Id: sdump_jobdef 655 2011-08-04 23:04:36Z acrnrls $
#=======================================================================
#
# files dumped will be of the form
#    ${sdump_prefix}_${year}_${season}_$suffix
# for all seasons from previous_year, previous_month to current_year,
# current_month or from current_year, current_month to next_year,
# next_month depending on which of previous_(year|month) or
# next_(year|month) are set.
# The default suffix list is "gp xp" which means that gp and xp files
# are dumped each season
#
# The variables current_year, current_month, previous_year and
# previous_month are set when the job string is created.
#
# explicitly setting sdump_start_year overrides previous_year
# explicitly setting sdump_start_mon  overrides previous_month
# explicitly setting sdump_stop_year   overrides current_year
# explicitly setting sdump_stop_mon    overrides current_month
#
# A variable named sdump_suffix_list may be set to modify the list
# of files that will be dumped. See below for details.
#
# sdump_prefix_ can be set to override the value of sdump_prefix
# files dumped will then be of the form
#    ${sdump_prefix_}${year}_${season}_$suffix
# note the missing underscore between sdump_prefix_ and year.
#
# sdump_djf, sdump_mam, sdump_jja, sdump_son and sdump_ann may be set
# to flag the inclusion of individual seasonal or annual average files.
#=======================================================================
#
#     keyword :: sdump
# description :: dump seasonal and/or annual averaged files to cfs
#

  set -a
  . betapath2

#  * ........................... Parmsub Parameters ............................

  runid="job000"; uxxx='uxxx'; pool_uxxx=$uxxx; sdump_uxxx=$pool_uxxx;
  jobname=sdump; sdump_prefix="${sdump_uxxx}_${runid}"; sdump_prefix_=${sdump_prefix}_
  crawork="${runid}_job"; username="acrnxxx"; user="XXX";
  nqsprfx="${runid}_"; nqsext='';

  dump_cfsuser=$username
  sdump_cfsuser=$dump_cfsuser
  cfsuser=$sdump_cfsuser

  RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;

  stime="1800"; memory1="250mb"; lopgm="lopgm";

  # It is more efficient to set CCRNTMP = $RUNPATH if all files
  # to be dumped are on pollux:$RUNPATH
  CCRNTMP=$CCRNTMP

  # Alternate path to a directory where .queue/.crawork will be found
  JHOME=''

  if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
    # Allow optional reset of DATAPATH/RUNPATH
    JHOME_DATA=''
    DATAPATH=${JHOME_DATA:=$DATAPATH}
    RUNPATH=${JHOME_DATA:=$RUNPATH}
    # Allow optional reset of CCRNTMP
    JHOME_RUN=''
    CCRNTMP=${JHOME_RUN:=$CCRNTMP}
  fi

  # BERUNPATH must be set if files are to be copied from a machine that is
  # not the default back end machine
  BERUNPATH=$BERUNPATH

  # RMTRUNPATH is used when vic = on is specified
  RMTRUNPATH=$RMTRUNPATH

  # ---Start_submit_ignore_code----

  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  error_out="${JHOME:-$HOME}/.queue/error_sdump_${runid}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" --- sdump: $*"
    echo_e `date`" --- sdump: $*" >>$error_out
    exit 1
  }

  # These variables are set when the job string is created

  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

  # Pooling start and stop dates
  # These may differ from run start and stop dates, but are the same by default
  pool_start_year=$run_start_year
  pool_start_month=$run_start_month
  pool_stop_year=$run_stop_year
  pool_stop_month=$run_stop_month

  # If either reset_start_year or reset_stop_year are set then they must be
  # of the form old_year:new_year (ie a colon separated pair of integers)
  # where the first integer is the year that needs to be changed
  # and the second integer is the year that it will be changed to.
  # These may potentially change the value of start_year or stop_year that
  # are defined after the call to make_file_name_list below
  sdump_reset_start_year=''
  reset_start_year=${sdump_reset_start_year:=''}
  sdump_reset_end_year=''
  sdump_reset_stop_year=$sdump_reset_end_year
  reset_stop_year=${sdump_reset_stop_year:=''}

  # This invocation of make_file_name_list will process the *_year and *_months
  # variables defined above and output a file containing definitions for
  # start_year, start_mon, stop_year, stop_mon
  tmp_file_list="${JHOME:-$HOME}/tmp/sdump_date_list_${runid}_${stamp}"
  make_file_name_list --dates_only $tmp_file_list >>$error_out 2>&1 ||\
    bail "Problem in make_file_name_list"
  rm -f $error_out

  # Verify that the output list is not empty
  [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

  # A file list was created ...source it
  # This will define start_year, start_mon, stop_year, stop_mon
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  # Define start and stop dates
  sdump_start_year=$start_year
  sdump_start_mon=$start_mon
  sdump_stop_year=$stop_year
  sdump_stop_mon=$stop_mon

  # These variables are depreciated but are here set for backward compatibility
  sdump_end_year=$stop_year
  sdump_end_mon=$stop_mon

  # sdump_pooled flags dumping pooled seasons rather than individual
  # seasons. The difference is that file names for pooled files are
  # of the form e.g.
  # ${sdump_prefix}_${sdump_start_year}_${sdump_stop_year}_djf_gp
  # ${sdump_prefix}_${sdump_start_year}_${sdump_stop_year}_djf_xp
  sdump_pooled=0
  XXX=`echo $sdump_pooled|sed 's/ //g'`
  eval sdump_pooled\=$XXX
  [ "$sdump_pooled" = 'on' ]  && eval sdump_pooled\=1
  [ "$sdump_pooled" = 'off' ] && eval sdump_pooled\=0
  [ "$sdump_pooled" = 'yes' ] && eval sdump_pooled\=1
  [ "$sdump_pooled" = 'no' ]  && eval sdump_pooled\=0

  # If arclabel is set then files to be dumped will be combined in a
  # cmcarc file with a name of the form {arclabel}_{Date-Time-Stamp}_arc
  # If arclabel is not set then files to be dumped will be combined in a
  # cmcarc file with a name of the form {common-prefix}_{Date-Time-Stamp}_arc
  ym_range="${sdump_start_year}m${sdump_start_mon}_${sdump_stop_year}m${sdump_stop_mon}"
  if [ $sdump_pooled -eq 1 ]; then
    sdump_arclabel="${runid}_pooled_$ym_range"
  else
    sdump_arclabel="${runid}_seasons_$ym_range"
  fi
  arclabel=${sdump_arclabel:-''}

  # sdump_suffix_list is a white space separated list of suffixes
  # of file names to be generated here. Any suffix in this list may be
  # modified by appending a + followed by a comma separated list of
  # numbers (no white space is allowed within this modifier). Each
  # number within the modifier list will correspond to a month (1-12)
  # for which a file with this suffix is to be included. If the
  # modifier exists for a particular suffix then only those months
  # indicated in the modifier will be added to the file list.
  sdump_suffix_list='gp xp'

 # sdump_suffix_ flags the addition of an underscore between the
 # suffix and the month (e.g. djf_gs) in file names generated here
 sdump_suffix_=1
 XXX=`echo $sdump_suffix_|sed 's/ //g'`
 eval sdump_suffix_\=$XXX
 [ "$sdump_suffix_" = 'on' ]  && eval sdump_suffix_\=1
 [ "$sdump_suffix_" = 'off' ] && eval sdump_suffix_\=0
 [ "$sdump_suffix_" = 'yes' ] && eval sdump_suffix_\=1
 [ "$sdump_suffix_" = 'no' ]  && eval sdump_suffix_\=0

 # sdump_djf, sdump_mam, sdump_jja and sdump_son flag the inclusion of
 # seasonal average files determined as file names of the form 
 # ${sdump_prefix_}${year}_${season}_$suffix
 # for season = djf, mam, jja or son
 sdump_djf=1
 XXX=`echo $sdump_djf|sed 's/ //g'`
 eval sdump_djf\=$XXX
 [ "$sdump_djf" = 'on' ]  && eval sdump_djf\=1
 [ "$sdump_djf" = 'off' ] && eval sdump_djf\=0
 [ "$sdump_djf" = 'yes' ] && eval sdump_djf\=1
 [ "$sdump_djf" = 'no' ]  && eval sdump_djf\=0
 sdump_mam=1
 XXX=`echo $sdump_mam|sed 's/ //g'`
 eval sdump_mam\=$XXX
 [ "$sdump_mam" = 'on' ]  && eval sdump_mam\=1
 [ "$sdump_mam" = 'off' ] && eval sdump_mam\=0
 [ "$sdump_mam" = 'yes' ] && eval sdump_mam\=1
 [ "$sdump_mam" = 'no' ]  && eval sdump_mam\=0
 sdump_jja=1
 XXX=`echo $sdump_jja|sed 's/ //g'`
 eval sdump_jja\=$XXX
 [ "$sdump_jja" = 'on' ]  && eval sdump_jja\=1
 [ "$sdump_jja" = 'off' ] && eval sdump_jja\=0
 [ "$sdump_jja" = 'yes' ] && eval sdump_jja\=1
 [ "$sdump_jja" = 'no' ]  && eval sdump_jja\=0
 sdump_son=1
 XXX=`echo $sdump_son|sed 's/ //g'`
 eval sdump_son\=$XXX
 [ "$sdump_son" = 'on' ]  && eval sdump_son\=1
 [ "$sdump_son" = 'off' ] && eval sdump_son\=0
 [ "$sdump_son" = 'yes' ] && eval sdump_son\=1
 [ "$sdump_son" = 'no' ]  && eval sdump_son\=0

 # sdump_ann flags the inclusion of annual average files determined
 # as file names with ann replacing djf, mam, jja or son.
 sdump_ann=0
 XXX=`echo $sdump_ann|sed 's/ //g'`
 eval sdump_ann\=$XXX
 [ "$sdump_ann" = 'on' ]  && eval sdump_ann\=1
 [ "$sdump_ann" = 'off' ] && eval sdump_ann\=0
 [ "$sdump_ann" = 'yes' ] && eval sdump_ann\=1
 [ "$sdump_ann" = 'no' ]  && eval sdump_ann\=0

 # sdump_mon flags the inclusion of monthly average files
 # file names with m01,m02,... replacing djf, mam, jja or son.
 sdump_mon=0
 XXX=`echo $sdump_mon|sed 's/ //g'`
 eval sdump_mon\=$XXX
 [ "$sdump_mon" = 'on' ]  && eval sdump_mon\=1
 [ "$sdump_mon" = 'off' ] && eval sdump_mon\=0
 [ "$sdump_mon" = 'yes' ] && eval sdump_mon\=1
 [ "$sdump_mon" = 'no' ]  && eval sdump_mon\=0

 # force_first_djf flags the inclusion of a DJF dump during run_start_year
 # Note: To dump force_first_djf must be either 1, 'on' or 'yes'.
 force_first_djf=0
 XXX=`echo $force_first_djf|sed 's/ //g'`
 eval force_first_djf\=$XXX
 [ "$force_first_djf" = 'on' ]  && eval force_first_djf\=1
 [ "$force_first_djf" = 'off' ] && eval force_first_djf\=0
 [ "$force_first_djf" = 'yes' ] && eval force_first_djf\=1
 [ "$force_first_djf" = 'no' ]  && eval force_first_djf\=0

 # force_first_sea flags pooling the first season in the string
 # even if pool_start_(year|month) falls after the first month of that season
 force_first_sea=off
 XXX=`echo $force_first_sea|sed 's/ //g'`
 eval force_first_sea\=$XXX
 [ "$force_first_sea" = 'on' ]  && eval force_first_sea\=1
 [ "$force_first_sea" = 'off' ] && eval force_first_sea\=0
 [ "$force_first_sea" = 'yes' ] && eval force_first_sea\=1
 [ "$force_first_sea" = 'no' ]  && eval force_first_sea\=0

 # Ensure that force_first_djf is true when force_first_sea is true
 [ $force_first_sea -eq 1 ] && eval force_first_djf\=1

 # pool_range is used as part of pooled file names
 pool_range_first_sea=${sdump_start_year}_${sdump_stop_year}

 # Generate a list of history files
 sdump_curr_year=`echo $sdump_start_year|awk '{y=$1-1;printf "%3.3d", y}' -`
 join=0
 while [ $sdump_curr_year -lt $sdump_stop_year ]; do
   sdump_curr_year=`echo $sdump_curr_year|awk '{y=1+$1;printf "%3.3d", y}' -`

   if [ $sdump_curr_year -eq $sdump_start_year ]; then
     mm=`echo $sdump_start_mon|awk '{m=$1-1;printf "%2.2d", m}' -`
   else
     mm=0
   fi
   if [ $sdump_curr_year -eq $sdump_stop_year ]; then
     mm_end=$sdump_stop_mon
   else
     mm_end=12
   fi

   if [ $sdump_pooled -eq 1 ]; then
     # When dumping pooled files iterate only once through the year loop
     if [ $sdump_curr_year -eq $sdump_start_year ]; then
       # Ensure that the month loop interates over all months
       mm=0
       mm_end=12
       force_first_djf=1
     else
       continue
     fi
   fi

   while [ $mm -lt $mm_end ]; do
     mm=`echo $mm|awk '{m=1+$1;printf "%2.2d", m}' -`
     # Reinitialize pool_range on each loop iteration
     pool_range=$pool_range_first_sea
     case $mm in
       02) # Feb ...dump DJF
           if [ "$sdump_djf" = '1' ]; then
             if [ $run_start_year -eq $sdump_curr_year ]; then
               if [ $force_first_djf -eq 1 ]; then
                 # Force a dump of the first DJF
                 # The user must set force_first_djf to indicate that
                 # the DEC files from the previous year are present
                 season='djf'
               else
                 continue
               fi
             else
               season='djf'
             fi
           else
             continue
           fi
           ;;
       05) # May ...dump MAM
           if [ "$sdump_mam" = '1' ]; then
             if [ $run_start_year -eq $sdump_curr_year -a \
                  $run_start_month -ge 3 ]; then
               if [ $force_first_sea -eq 1 ]; then
                 # Force a dump of the first MAM
                 season='mam'
               else
                 continue
               fi
             else
               season='mam'
             fi
           else
             continue
           fi
           ;;
       08) # Aug ...dump JJA
           if [ "$sdump_jja" = '1' ]; then
             if [ $run_start_year -eq $sdump_curr_year -a \
                  $run_start_month -ge 6 ]; then
               if [ $force_first_sea -eq 1 ]; then
                 # Force a dump of the first JJA
                 season='jja'
               else
                 continue
               fi
             else
               season='jja'
             fi
           else
             continue
           fi
           ;;
       11) # Nov ...dump SON
           if [ "$sdump_son" = '1' ]; then
             if [ $run_start_year -eq $sdump_curr_year -a \
                  $run_start_month -ge 9 ]; then
               if [ $force_first_sea -eq 1 ]; then
                 # Force a dump of the first SON
                 season='son'
               else
                 continue
               fi
             else
               season='son'
             fi
           else
             continue
           fi
           ;;
       12) # Dec ...dump annual average
           if [ "$sdump_ann" = '1' ]; then
             season='ann'
           else
             continue
           fi
           ;;
       *) continue ;;
     esac
     if [ "$sdump_suffix_" = '1' ]; then
       if [ "$sdump_pooled" = '1' ]; then
         # use a different form for file names when dumping pooled files
         bname=${sdump_prefix_}${pool_range}_${season}_
       else
         bname=${sdump_prefix_}${sdump_curr_year}_${season}_
       fi
     else
       if [ "$sdump_pooled" = '1' ]; then
         # use a different form for file names when dumping pooled files
         bname=${sdump_prefix_}${pool_range}_$season
       else
         bname=${sdump_prefix_}${sdump_curr_year}_$season
       fi
     fi
     for suffix in $sdump_suffix_list; do
       mlist=`echo $suffix|awk -F'+' '{print $2}' -`
       mlist=`echo $mlist|sed 's/,/ /g'`
       if [ -n "$mlist" ]; then
         suffix=`echo $suffix|sed 's/+.*$//'`
         # assume that mlist is a white space separated list of numbers
         # indicating which months to dump
         for xx in $mlist; do
           if [ $xx -eq $mm ]; then
             join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
             join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
             eval file${join}=$bname$suffix
           fi
         done
       else
         join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
         join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
         eval file${join}=$bname$suffix
       fi
     done

   done
 done

 # Add monthly files to the list if requested
 if [ "$sdump_mon" = '1' ]; then
   sdump_curr_year=`echo $sdump_start_year|awk '{y=$1-1;printf "%3.3d", y}' -`
   while [ $sdump_curr_year -lt $sdump_stop_year ]; do
     sdump_curr_year=`echo $sdump_curr_year|awk '{y=1+$1;printf "%3.3d", y}' -`

     if [ $sdump_curr_year -eq $sdump_start_year ]; then
       mm=`echo $sdump_start_mon|awk '{m=$1-1;printf "%2.2d", m}' -`
     else
       mm=0
     fi
     if [ $sdump_curr_year -eq $sdump_stop_year ]; then
       mm_end=$sdump_stop_mon
     else
       mm_end=12
     fi

     if [ $sdump_pooled -eq 1 ]; then
       # When dumping pooled files iterate only once through the year loop
       if [ $sdump_curr_year -eq $sdump_start_year ]; then
         # Ensure that the month loop interates over all months
         mm=0
         mm_end=12
       else
         continue
       fi
     fi

     while [ $mm -lt $mm_end ]; do
       mm=`echo $mm|awk '{m=1+$1;printf "%2.2d", m}' -`
       if [ "$sdump_suffix_" = '1' ]; then
         if [ "$sdump_pooled" = '1' ]; then
           # use a different form for file names when dumping pooled files
           bname=${sdump_prefix_}${sdump_start_year}_${sdump_stop_year}_m${mm}_
         else
           bname=${sdump_prefix_}${sdump_curr_year}_m${mm}_
         fi
       else
         if [ "$sdump_pooled" = '1' ]; then
           # use a different form for file names when dumping pooled files
           bname=${sdump_prefix_}${sdump_start_year}_${sdump_stop_year}
         else
           bname=${sdump_prefix_}${sdump_curr_year}_m$mm
         fi
       fi
       for suffix in $sdump_suffix_list; do
         mlist=`echo $suffix|awk -F'+' '{print $2}' -`
         mlist=`echo $mlist|sed 's/,/ /g'`
         if [ -n "$mlist" ]; then
           suffix=`echo $suffix|sed 's/+.*$//'`
           # assume that mlist is a white space separated list of numbers
           # indicating which months to dump
           for xx in $mlist; do
             if [ $xx -eq $mm ]; then
               join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
               join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
               eval file${join}=$bname$suffix
             fi
           done
         else
           join=`echo $join|awk '{j=1+$1;printf "%d",j}' -`
           join=`echo $join|sed -e 's/^ *//' -e 's/^0*//'`
           eval file${join}=$bname$suffix
         fi
       done
     done
   done
 fi

 [ $join -eq 0 ] &&
   bail "No files selected for the year/mon range $sdump_start_year/$sdump_start_mon to $sdump_stop_year/$sdump_stop_mon"

 # ---Stop_submit_ignore_code----

#  * ............................ Condef Parameters ............................

 noprint=on
 nextjob=on

 dump_masterdir=off
 sdump_masterdir=${dump_masterdir:=off}
 masterdir=${sdump_masterdir:=off}

 dump_shortermdir=on
 sdump_shortermdir=${dump_shortermdir:=on}
 shortermdir=${sdump_shortermdir:=on}

 # The default if none of sv, both, vic or svvic are set to "on" is to
 # dump files to the back end machine
 # Note: Once again submit3 is causing a problem. It will not respect end of line
 # comments inside its internally created file named job.info. As a result a
 # constructs such as "vic =" are flagged as a syntax error even though they
 # are commented out.

 # sv : on means dump files from pollux
 dump_sv=off
 sdump_sv=${dump_sv:=off}
 sv=${sdump_sv:=off}

 both=off    # both  : on means dump files from pollux and the back end machine
 vic=off     # vic   : on means dump files from Victoria
 svvic=off   # svvic : on means dump files from pollux and Victoria

  # nolist : off means save CFSDATA* files in ~/info
  # nolist : on means do not save CFSDATA* files in ~/info
  dump_nolist=off
  sdump_nolist=${dump_nolist:=off}
  nolist=${sdump_nolist:=off}

  # rmdskcpy = on means remove the copy of the file on the cfs disk buffer
  # as soon as it has been written to tape
  sdump_rmdskcpy=''
  rmdskcpy=${sdump_rmdskcpy:=off}

  # cnfsallow = on will allow the user to use a cnfs dir for
  # RUNPATH and/or CCRNTMP when svsave = on
  # Normally this is not allowed because of the possibility that files
  # may get corrupted during transfer from/to the nfs file system
  sdump_cnfsallow=''
  cnfsallow=${sdump_cnfsallow:=off}

  # dpalist = on means store DATAPATH information in a local file for use by access/save/delete
  # Since this local file is never deleted, dpalist = on should only be used when access,
  # save or delete are part of a job that runs in a temporary working directory which gets
  # removed after the job completes. Most decks do this, but not all. You have been warned!
  sdump_dpalist=''
  dpalist=${sdump_dpalist:=''}

  # besc = on causes a tdumper job to run on the back end (spica/hadar)
  # These definitions need to be on the same line to work around an error in the submit scripts
  sdump_besc=''; besc=${sdump_besc:=off}

#  * ............................. Deck Definition .............................

 # Note: If tdumper is placed inside a conditional then submit3 will fail.
 # submit3 will ignore everthing after the tdumper line when it creates an
 # internal file named job.info. It then attempts to source job.info and dies
 # due to a syntax error (the final fi is missing).
 . tdumper.dk


#end_of_job

