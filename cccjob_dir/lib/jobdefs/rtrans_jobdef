#!/bin/sh
#=======================================================================
# Transfer files to/from a remote machine                 --- rtrans ---
# $Id: rtrans_jobdef 671 2012-05-04 23:35:22Z acrnrls $
#=======================================================================
#
# Larry Solheim  Feb,2007
#
# files transferred will be of the form
#    ${rtrans_prefix}_${year}_m${mon}_$suffix
# for all months from previous_year, previous_month to current_year,
# current_month or from current_year, current_month to next_year,
# next_month depending on which of previous_(year|month) or
# next_(year|month) are set.
#
# The variables current_year, current_month, previous_year and
# previous_month are set when the job string is created.
#
# A variable named rtrans_suffix_list may also be set to modify the list
# of files that will be dumped. See below for details.
#
# rtrans_prefix_ can be set to override the value of rtrans_prefix
# files dumped will then be of the form
#    ${rtrans_prefix_}${year}_m${mon}_$suffix
# note the missing underscore between rtrans_prefix_ and year.
#
# The user must set up public key authentication on the remote machine
# so that transport from pollux to the remote machine does not require
# a password. This means, create a public/private key pair on pollux
# using ssh-keygen (if nessecary) then copy the users public key from
# pollux and append it to ~user/.ssh/authorized_keys on the remote machine
#
# A default remote user, remote machine and remote directory into which
# files are to be copied is set in the script below (see the defs for
# the variables remusr, remserver and remdir in the Execute_Script here
# document). The user can modify these to suit or define rtrans_remusr,
# rtrans_remserver and rtrans_remdir, which will override the default
# values.
#=======================================================================
#
#     keyword :: rtrans
# description :: transfer files to/from a remote machine
#
 set -a
 . betapath2

#  * ........................... Parmsub Parameters ............................

 runid=job000; uxxx=uxxx; rtrans_uxxx=$uxxx;
 username=acrnxxx; user=MYNAME; crawork=rtrans_job

 nextjob=on
 noprint=on
 debug=off

 stime=${stime:=7200}; memory1=${memory1:=1500mb}
 jobname=rtrans; time=$stime ; memory=1000mb;

 # The following parameters may be set by the user. This is not required
 # since each parameter is assigned a default value below.

 # rtrans_remusr is the login name of the user on the remote machine
 rtrans_remusr=''

 # rtrans_remserver is the fully qualified domain name of the remote machine
 rtrans_remserver=''

 # rtrans_remdir is a directory on the remote machine into which files will
 # be copied. If this is a relative dir name (ie it does not start with /)
 # then it will be relative to the remote user's home directory.
 rtrans_remdir=''

 # rtrans_force flags the forced removal, prior to the copy, of any files on the
 # remote machine that have the same name as files that are to be copied.
 # This may be required if the remote user does not have write permission on
 # the remote file, which would otherwise cause scp to fail.
 rtrans_force=''

 # rtrans_scp_args and rtrans_ssh_args may be used to supply additional
 # command line arguments to the scp or ssh commands used below.
 # This is only useful for debugging.
 rtrans_scp_args=''
 rtrans_ssh_args=''

 # rtrans_getput may be set to either get or put indicating the direction
 # of the transfer (think ftp)
 rtrans_getput=''

 # rtrans_rem_save flags saving the file (using the CCCma "save" command)
 # on the remote machine. This is only valid when rtrans_getput=put.
 rtrans_rem_save=''

 # rtrans_fspec? variables are not yet implemented
 # *** DO NOT USE ***
 rtrans_fspec1=''
 rtrans_fspec2=''

 # rtrans_get_ext is an optional extention that will be appended to file
 # names on the remote machine when getput=get
 rtrans_get_ext=''

 # Temporary directory where this script will run
 rtrans_CCRNTMP=''
 CCRNTMP=${rtrans_CCRNTMP:=$CCRNTMP}

 # RUNPATH on the execution machine
 rtrans_RUNPATH=''
 RUNPATH=${rtrans_RUNPATH:=$RUNPATH}

 # Alternate path to a directory where .queue/.crawork will be found
 JHOME=''

 if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
   # Allow optional reset of DATAPATH/RUNPATH
   JHOME_DATA=''
   DATAPATH=${JHOME_DATA:=$DATAPATH}
   RUNPATH=${JHOME_DATA:=$RUNPATH}
   # Allow optional reset of CCRNTMP
   JHOME_RUN=''
   CCRNTMP=${JHOME_RUN:=$CCRNTMP}
 fi

 . comjcl.cdk
 cat > Execute_Script <<'end_of_script'

  # Certain variables need to be exported so that
  # they are visible to make_file_name_list
  set -a

  # ---Start_submit_ignore_code----

  # These variables are set when the job string is created
  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

  runid=job000; uxxx=uxxx; rtrans_uxxx=$uxxx;

  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  error_out="${JHOME:-$HOME}/.queue/error_rtrans_${runid}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" --- rtrans: $*"
    echo_e `date`" --- rtrans: $*" >>$error_out
    exit 1
  }

  # ssh will not work when running on a batch node
  # To get around this simply prefix each invocation of ssh below
  # with an ssh to the head node, where ssh will work
  this_dir=`pwd`
  this_host=`hostname|cut -d'.' -f1`
  case $this_host in
    ib3-*) presh='ssh pollux' ;;
      al*) presh='ssh alef' ;;
      sp*) presh='ssh spica' ;;
      c1*) presh='ssh spica' ;;
      ha*) presh='ssh hadar' ;;
      c2*) presh='ssh hadar' ;;
        *) presh='' ;;
  esac

  # rtd parameters
  # PhysA = on/off     physical atmosphere variables
  # PhysO = on/off     physical ocean variables
  # CarbA = on/off     atmosphere carbon variables
  # CarbO = on/off     ocean carbon variables
  # CarbL = on/off     land carbon variables
  PhysA=on
  PhysO=on
  CarbA=on
  CarbO=on
  CarbL=on

  # RHS for certain ocean variables
  rhs_avg=off

  # If either reset_start_year or reset_stop_year are set then they must be
  # of the form old_year:new_year (ie a colon separated pair of integers)
  # where the first integer is the year that needs to be changed
  # and the second integer is the year that it will be changed to.
  # These may potentially change the value of start_year or stop_year that
  # are defined after the call to make_file_name_list below
  rtrans_reset_start_year=''
  reset_start_year=${rtrans_reset_start_year:=''}
  rtrans_reset_end_year=''
  rtrans_reset_stop_year=$rtrans_reset_end_year
  reset_stop_year=${rtrans_reset_stop_year:=''}

  # prefix_list and suffix_list are used by make_file_name_list, together with
  # other variables from the current environment, to build a list of files names
  rtrans_suffix_list=''
  suffix_list="${rtrans_suffix_list:-gs ss}"

  rtrans_prefix_list=''
  prefix_list="${rtrans_prefix_list:-$rtrans_uxxx}"

  # Create a file containing a list of file names that may then be
  # "sourced" in the current environment to define the variables
  # file1, file2,..., file$join, join. These variables are used by
  # tdumper to compile the list of files to be archived.
  join=0

  # make_file_name_list uses the variables current_year, current_month,
  # previous_year, previous_month, next_year and next_month to
  # determine start and stop dates for file name creation.
  # It also uses runid, prefix_list and suffix_list from the current
  # environment to build these file names.

  which make_file_name_list || bail "make_file_name_list is not in your path"

  # Allow user supplied command line options for make_file_name_list
  # The following invocation of make_file_name_list will not allow multi-list
  # output so if any command line option is supplied that will turn on
  # multi-list output (e.g. --months_max=... --size_max=... --number_max=..)
  # then this script will abort.
  rtrans_file_list_opts=''
  fopts="${rtrans_file_list_opts:-}"

  rtrans_mon_offset=''
  if [ -n "$rtrans_mon_offset" ]; then
    # Set a user supplied month offset
    eval fopts=\"$fopts --mon_offset\=$rtrans_mon_offset\"
  fi

  # Create a temporary file containing the file list
  tmp_file_list="rtrans_file_list_${runid}_${stamp}"
  make_file_name_list $fopts --nomulti_list $tmp_file_list >>$error_out 2>&1 ||\
    bail "Problem in make_file_name_list"
  rm -f $error_out

  [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

  # A file list was created ...source it
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  # At this point join, file1, file2,... are defined in the current environment
  # as well as certain other variables such as start_year, start_mon,
  # stop_year and stop_mon which correspond to the start and stop dates
  # for the file names that were created.
  ym_range="${start_year}m${start_mon}_${stop_year}m${stop_mon}"

  # These are defined for backward compatability
  rtrans_start_year=$start_year
  rtrans_start_mon=$start_mon
  rtrans_end_year=$stop_year
  rtrans_end_mon=$stop_mon

  # join is defined when the file created by make_file_name_list is sourced
  nfile=$join

  # NTFILE and TFILEN are used in the rtrans_file_list conditional block below
  # to test for existence of a user supplied list of files as opposed to an
  # internally generated list.
  NTFILE=0
  while [ $NTFILE -lt $nfile ]; do
    NTFILE=`echo $NTFILE|awk '{printf "%d",$1+1}' -`
    eval TFILE${NTFILE}=\$file$NTFILE
  done

  # These assignments of join and nfile are required here in case the user
  # requests a list of files that override the file names generated above.
  # If there is a user supplied list it must redefine join appropriately.
  join=$nfile
  # <<INSERT_RTRANS>>
  nfile=$join

  # If rtrans_file_list is defined then add the file names defined therein
  # to the list of files to be dumped. This will be in addition to any
  # provided by the user when this string was created.
  # The file provided in rtrans_file_list must be available via "access"
  rtrans_file_list=''

  if [ -n "$rtrans_file_list" ]; then
    # access a file containing the list of file names
    access LOCAL_FILE_LIST $rtrans_file_list nocp=off dpalist=off

    # Determine how many, if any, fileN variables are defined
    # Any file names supplied by the user at the time this string
    # is created will already be defined by this point.
    # Note that the variables fileN must be exported so that the
    # following script will have access to them
    user_supplied_list=0
    if [ $NTFILE -ne $nfile ]; then
      # Number of files do not match...the user has supplied a list of files
      user_supplied_list=1
    else
      # We need to test each file to see if any are changed
      n=0
      while [ $n -lt $nfile ]; do
        n=`echo $n|awk '{printf "%d",$1+1}' -`
        eval xfile=\$TFILE${n}
        eval yfile=\$file${n}
        if [ x"$xfile" != x"$yfile" ]; then
          # files do not match...the user has supplied a file list
          user_supplied_list=1
          break
        fi
      done
    fi
    if [ $user_supplied_list -eq 0 ]; then
      # It is assumed that the user has NOT supplied a list of files above
      # and any files defined internally will be ignored.
      N=0
    else
      # The user has supplied a list of files above
      # In this case nfile must be set to the number of files in that list
      N=$nfile
    fi

    # Create a shell script that may be "sourced" to add the
    # file names provided in rtrans_file_list to the current env
    # If rtrans_file_list contains comment lines of the form "#break"
    # then an extra fileN=:break will be defined. These extra defs
    # are used to force a new sub list whenever they are encountered
    # in the sub loop processing done below.
    # Similarly, if rtrans_file_list contains comment lines of the form "#stop"
    # then an extra fileN=:stop will be defined and used to terminate
    # sub list processing in the script below.
    awk -v N=$N '
        BEGIN {print "# Files added from list" > "fileN_from_list"}
        $0 ~ /^[ \t]*#/ {if ($0 ~ /^[ \t]*#[ \t]*[Ss][Tt][Oo][Pp][ \t]*$/) {exit};
                         next}
        $0 ~ /^[ \t]*$/ {next}
        {N+=1; printf "file%d=%s\n",N,$1 >> "fileN_from_list"}
        END {printf "join=%d\nnfile=%d\n",N,N >> "fileN_from_list"}' LOCAL_FILE_LIST
    [ ! -s fileN_from_list ] && bail "Error processing $rtrans_file_list"
    chmod u+x fileN_from_list

    release LOCAL_FILE_LIST

    : ; . fileN_from_list

  fi

  # rtrans_abort flags continuation on error when rtrans_abort=0 or abort on
  # error when rtrans_abort is not 0, for remote commands used below
  # rtrans_abort may also be assigned the strings "on", "off", "yes" and "no".
  rtrans_abort=1
  XXX=`echo $rtrans_abort|sed 's/ //g'`
  eval rtrans_abort\=$XXX
  [ "$rtrans_abort" = 'on' ]  && eval rtrans_abort\=1
  [ "$rtrans_abort" = 'off' ] && eval rtrans_abort\=0
  [ "$rtrans_abort" = 'yes' ] && eval rtrans_abort\=1
  [ "$rtrans_abort" = 'no' ]  && eval rtrans_abort\=0

  # rem_save flags saving files on the remote machine
  rem_save=${rtrans_rem_save:=0}
  XXX=`echo $rem_save|sed 's/ //g'`
  eval rem_save\=$XXX
  [ "$rem_save" = 'on' ]  && eval rem_save\=1
  [ "$rem_save" = 'off' ] && eval rem_save\=0
  [ "$rem_save" = 'yes' ] && eval rem_save\=1
  [ "$rem_save" = 'no' ]  && eval rem_save\=0

  # getput may only be set to get or put to indicate direction of transfer
  getput=${rtrans_getput:-'put'}
  XXX=`echo $getput|sed 's/ //g'`
  eval getput\=$XXX
  if [ x"$getput" != x"get" -a x"$getput" != x"put" ]; then
    bail "rtrans_getput=$rtrans_getput must be set to either get or put"
  fi

  # remloc is the remote user@host (or just host if remuser is not set)
  remusr=${rtrans_remusr:-`whoami`}
  remusr=`echo $remusr|sed 's/ //g'`
  remserver=${rtrans_remserver:-'lxsrv.cccma.ec.gc.ca'}
  remserver=`echo $remserver|sed 's/ //g'`
  if [ -n "$remusr" ]; then
    remloc="${remusr}@$remserver"
  else
    remloc="$remserver"
  fi

  # remdir is the remote directory to put files into
  # Note: def_remdir will be null if the remote echo fails
  def_remdir=`$presh ssh $remloc 'echo $RUNPATH'` 2>/dev/null
  remdir=${rtrans_remdir:-$def_remdir}
  [ -z "$remdir" ] && bail "Unable to determine RUNPATH on $remserver"

  rtrans_mkdir_remdir=0
  XXX=`echo $rtrans_mkdir_remdir|sed 's/ //g'`
  eval rtrans_mkdir_remdir\=$XXX
  [ "$rtrans_mkdir_remdir" = 'on' ]  && eval rtrans_mkdir_remdir\=1
  [ "$rtrans_mkdir_remdir" = 'off' ] && eval rtrans_mkdir_remdir\=0
  [ "$rtrans_mkdir_remdir" = 'yes' ] && eval rtrans_mkdir_remdir\=1
  [ "$rtrans_mkdir_remdir" = 'no' ]  && eval rtrans_mkdir_remdir\=0

  # Default "always on" scp and ssh command line args
  # scp_args='-p -o NoneSwitch=yes'
  scp_args='-p'
  ssh_args=''

  # additional user defined scp or ssh command line options
  usr_scp_args=${rtrans_scp_args:=''}
  usr_ssh_args=${rtrans_ssh_args:=''}
  scp_args="$scp_args $usr_scp_args"
  ssh_args="$ssh_args $usr_ssh_args"

  if [ $rtrans_mkdir_remdir -eq 1 -a x"$getput" = x"put" ]; then
    # Ensure that the remote dir exists. Create it if is does not exist.
    # This is only done when the copy is from a local to a remote machine
    # It is assumed that remdir will always be a directory. This may not be true
    # if the user has explicitly set rtrans_remdir to the name of an existing
    # regular file on the remote machine. However, the remote mkdir done below
    # should return an error in this case, which will be followed by an abort
    # (but only when rtrans_abort is set).
    fail=0
    $presh ssh $ssh_args $remloc "mkdir -p $remdir" 2>&1 || fail=1
    if [ $fail -eq 1 ]; then
      echo "***ERROR*** Unable to create remote dir $remdir\n"
      [ $rtrans_abort -ne 0 ] && bail "abort\n"
    fi
  fi

  # define the remote file specification for scp
  remspec=`echo $remloc|sed 's/:$//'`':'
  if [ -n "$remdir" ]; then
    if [ `expr $remdir : '\(.\)'` = '/' ]; then
      # this is an absoulte pathname
      remspec=$remspec$remdir
    else
      # this is a relative pathname
      # assume it is realtive to ~remusr
      remdir='./'$remdir
      remspec=$remspec$remdir
    fi
  else
    remdir='./'
    remspec=$remspec$remdir
  fi
  echo "Remote Directory $remspec\n"

  # forcecp will force a copy by explicitly removing the file before the copy
  forcecp=${rtrans_force:-0}
  XXX=`echo $forcecp|sed 's/ //g'`
  eval forcecp\=$XXX
  [ "$forcecp" = 'on' ]  && eval forcecp\=1
  [ "$forcecp" = 'off' ] && eval forcecp\=0
  [ "$forcecp" = 'yes' ] && eval forcecp\=1
  [ "$forcecp" = 'no' ]  && eval forcecp\=0

  # copy files
  n=0
  nerr=0
  # remsave is the name of a file that will be copied to the remote machine
  # and executed to save a file that was recently copied there
  remsave="remsave$$"
  while [ $n -lt $nfile ]; do
    n=`echo $n|awk '{printf "%d",$1+1}' -`
    eval fname=\$file$n
    if [ x"$getput" = x"put" ]; then
      # copy local files to a remote machine
      fspec1=$fname
      fspec2=$remspec
      access $fname $fname
      if [ $forcecp -ne 0 ]; then
        # remove the remote file prior to the transfer
        # The following remote command requires a bourne shell,
        # therefore explicitly use /bin/sh -c to ensure that this is so.
        # Note: submit3 blindly replaces /bin/sh with the name of a shell
        # that will not exist outside of the cccma environment. The following
        # eval will hide the name of the remote shell from this substitution.
        eval remshell=\'\/bin\/sh -c\'
        remcmd="[ -f $remdir/$fname ] && set -x;rm -f $remdir/$fname || x=1"
        cptries=0
        while [ $cptries -lt 3 ]; do
          # retry up to 3 times if the command fails
          cptries=`echo $cptries|awk '{printf "%d",$1+1}' -`
          fail=0
          $presh ssh $ssh_args $remloc "$remshell '$remcmd'" 2>&1 || fail=$?
          if [ $fail -eq 0 ]; then
            break
          else
            echo "*** RETRY rm *** $cptries  error=$fail"
          fi
        done
        if [ $fail -ne 0 ]; then
          echo "***ERROR*** Unable to remove remote file $remdir/$fname\n"
          [ $rtrans_abort -ne 0 ] && bail "abort\n"
        fi
      fi
    elif [ x"$getput" = x"get" ]; then
      # copy remote file to this machine
      fspec1=$remspec/$fname$rtrans_get_ext
      fspec2=$fname
      [ -n "$rtrans_get_ext" ] && fspec2='.'
      if [ $forcecp -ne 0 ]; then
        # remove the local file prior to the transfer
        rm -f $fname
      fi
    else
      bail "getput=$getput must be set to either get or put"
    fi
    cptries=0
    tmp_err=scp_stdout_$stamp
    rm -f $tmp_err
    while [ $cptries -lt 3 ]; do
      # retry up to 3 times if the copy command fails
      cptries=`echo $cptries|awk '{printf "%d",$1+1}' -`
      # invoke the copy command
      xcmd=${JHOME:-$HOME}/tmp/xcmd${runid}$$
      echo '#!/bin/sh' > $xcmd
      echo "cd $this_dir" >> $xcmd
      eval echo "scp $scp_args \'$fspec1\' \'$fspec2\'" >> $xcmd
      chmod u+x $xcmd
      fail=0
      if [ -z "$presh" ]; then
        $xcmd >>$tmp_err 2>&1 || fail=$?
      else
        $presh $xcmd >>$tmp_err 2>&1 || fail=$?
      fi
      if [ $fail -eq 0 ]; then
        rm -f $xcmd
        rm -f $tmp_err
        break
      else
        echo "SCP script:"
        cat $xcmd
        echo "Error executing this script"
        cat $tmp_err
        rm -f $xcmd
        rm -f $tmp_err
        if [ $cptries -eq 1 -a x"$getput" = x"put" ]; then
          # On the first retry, attempt a forced delete of the remote file before
          # retrying the copy, ignoring whether or not the rm fails or succeeds
          $presh ssh $ssh_args $remloc "rm -f $remdir/$fname" 2>&1 || :
        fi
        echo "*** RETRY scp *** $cptries  error=$fail"
      fi
    done
    nerr=`echo $nerr $fail|awk '{printf "%d",$1+$2}' -`
    if [ $fail -eq 0 ]; then
      echo "Transferred $fspec1 to $fspec2"
    else
      echo "*** ERROR *** failed to copy $fspec1 to $fspec2\n"
      echo "using: "`which scp`
      ls -al
      [ $rtrans_abort -ne 0 ] && bail "abort\n"
    fi
    if [ x"$getput" = x"put" ]; then
      release $fname
      if [ $rem_save -eq 1 ]; then
        # save the file just copied on the remote machine
        # The following remote command requires a bourne shell,
        # therefore explicitly use /bin/sh -c to ensure that this is so.
        # Note: submit3 blindly replaces /bin/sh with the name of a shell
        # that will not exist outside of the cccma environment. The following
        # eval will hide the name of the remote shell from this substitution.
        eval shebang=\'\#\!\/bin\/sh\'
        eval remshell=\'\/bin\/sh -c\'
        eval def_rmrun=RUNPATH\=$remdir
#        remcmd="set -x;cd $remdir;eval RUNPATH\=$remdir;export RUNPATH;save $fname $fname && release $fname"
        remcmd="set -x;cd $remdir;$def_rmrun;export RUNPATH;save $fname $fname && release $fname"
        cat > $remsave <<EOF
$shebang
$remcmd
EOF
        chmod u+rx $remsave
        fail=0
        if [ -z "$presh" ]; then
          ssh $ssh_args $remloc "$remshell '$remcmd'" 2>&1 || fail=$?
        else
          $presh "cd $this_dir;scp $scp_args $remsave $remspec" 2>&1 || fail=$?
          if [ $fail -eq 0 ]; then
            $presh ssh $ssh_args $remloc "$remdir/$remsave" 2>&1 || fail=$?
            if [ $fail -ne 0 ]; then
              # Try this save a second time after waiting 120 seconds
              # This usually fails because the database server on lxsrv is down
              # Waiting will give it a chance to restart
              sleep 120
              $presh ssh $ssh_args $remloc "$remdir/$remsave" 2>&1 || fail=$?
            fi
          fi
        fi
        if [ $fail -ne 0 ]; then
          echo "***ERROR*** Unable to save remote file $remdir/$fname\n"
          [ $rtrans_abort -ne 0 ] && bail "abort\n"
        fi
      fi
    elif [ x"$getput" = x"get" ]; then
      # Ensure we have write permission
      chmod u+rw ${fname}*
      if [ -n "$rtrans_get_ext" ]; then
        # reset fname to the the name with the extension
        # NOTE: Use the highest edition number if this is a numeric extension
        fname=`ls -1 ${fname}$rtrans_get_ext|tail -1`
      fi
      # Strip any numeric extention from the file name prior to saving
      fnox=`echo $fname|sed 's/\.[0-9][0-9][0-9] *$//'`
      if [ -n "$fnox" -a "$fnox" != "$fname" ]; then
        mv $fname $fnox
        fname=$fnox
      fi
      # save the file just copied to DATAPATH/RUNPATH
      [ -s "$fname" ] && save $fname $fname
      release $fname
    else
      bail "getput=$getput must be set to either get or put"
    fi
  done

  # Remove the file named in remsave from the remote machine
  if [ -n "$presh" ]; then
    # Ignore any error returned for this command
    $presh ssh $ssh_args $remloc "rm -f $remdir/$remsave" 2>&1 || :
  fi

  if [ $nerr -gt 0 ]; then
    echo "***ERROR*** Failed to copy $nerr files\n"
    [ $rtrans_abort -ne 0 ] && bail "abort\n"
  fi

  # sucessful completion
  exit 0

  # ---Stop_submit_ignore_code----

end_of_script

 . endjcl.cdk

#end_of_job
