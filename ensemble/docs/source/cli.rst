.. _cli:


Command Line Interfaces
***********************

While you can use the python classes directly, a few command line interfaces are provided that are
generally easier to use.
These are available from the command line in the environment that ``canesm-ensemble`` was installed.

There are three main tasks associated with ensembles, which are exposed through the command line interfaces:

  - :ref:`setup-ensemble<setup_ensemble>`
  - :ref:`submit-ensemble<submit_the_jobs>`
  - :ref:`monitor-ensemble<monitor_ensemble>`

.. _setup_ensemble:

setup-ensemble
^^^^^^^^^^^^^^

This is the main setup function, and once you've written your configuration file
(let's call it ``conifg.yaml``) you can setup the ensemble members on the remote maching
using

.. code-block:: bash

   $ setup-ensemble config.yaml

In addition to the initial setup, you can also submit the ensemble, add members, and extend the simulation
time using ``setup-ensemble``.

Submitting an Ensemble
''''''''''''''''''''''

By default the ensemble is not submitted after being created. However they can be by using
the flag ``--submit`` when calling the ``setup-ensemble`` script.

.. code-block:: bash

   $ setup-ensemble config.yaml --submit

This will submit each member to ``machine`` as soon as the setup of the member is complete.

.. _add_members:

Adding Members to an Ensemble
'''''''''''''''''''''''''''''

If you ran an ensemble and want to add more members you can do that by simply extending your ``config.yaml``
and/or ``config_table.txt`` files. For example, lets say we setup an ensemble with 10 members with everything
the same except for the random number seed. If we want to add another 10 members we can just change:

.. code-block:: yaml

   ensemble_size: 20

Now, when we run ``setup-ensemble config.yaml``, the first 10 jobs will be skipped since they are already
setup, and the next 10 will be processed and added to the ``run_directory``. Similarily, you can add additional
rows to your ``config_table.txt`` file, and previously setup jobs will be skipped.

.. _extend_ensemble:

Extending an Ensemble
'''''''''''''''''''''

If you ran an ensemble, but want a longer simulation time you can extend the
simulations using the command line tool

.. code-block:: bash

   $ setup-ensemble config.yaml --extend 10 --submit

This will continue running the ensemble members from their previous ``stop_time`` for another ``10`` years.
The job status will be reset in the submission database, and a new configuration file and table
(if used) will be generated with the postfix ``_extended``. These new files are also placed on the
remote machine. ``--submit`` is optional, and works as it does for the original ensemble,
so you can do something like

.. code-block:: bash

   $ setup-ensemble config.yaml --extend 5
   $ submit-ensemble config_extended.yaml -n 10

If you want to extend the simulation by 5 years and then submit the first 10 members.

.. warning::

   Right now this can only be ran after the entire ensemble has finished running. Otherwise the database
   will end up in a broken state where ``setup`` and ``submitted`` do not accurately reflect the state
   of jobs that have not finished the initial simulation. Restart files may also not be available, depending
   when the jobs are submitted.

.. _restart_setup:

Restarting Cancelled Ensemble Setups
''''''''''''''''''''''''''''''''''''

If the ensemble setup is cancelled in the middle of running you can simply run it again
and setup will continue where it left off. However, you will likely end up with a renamed folder in your
``$CCRNSRC`` directory for the ensemble member that failed, so clean this up periodically if you use this feature.


.. _submit_the_jobs:

submit-ensemble
^^^^^^^^^^^^^^^

If you want more fine-grained control
on submitting jobs you can submit in batches using the ``submit-ensemble`` tool after using
``setup-ensemble``. For example, if you have a big ensemble, and want to submit the first ``10``
jobs you can use

.. code-block:: bash

   $ setup-ensemble big_ensemble.yaml
   $ submit-ensemble big_ensemble.yaml -n 10

This will setup the ensemble and submit the first ``10`` members. Then, when these are finshed you can run

.. code-block:: bash

   $ submit-ensemble big_ensemble.yaml -n 10

To submit the next ``10``. Determination of what jobs have been submitted is handled by an sqlite database
stored in the ``run_directory`` on the remote machine. You can see the database status using

.. code-block:: bash

   $ submit-ensemble big_ensemble.yaml --show_status

If you would like to submit a lot of jobs, but want a delay in between each one, you can use the flag

.. code-block:: bash

   $ submit-ensemble big_ensemble.yaml --delay 60

This will submit all the ensemble members, but wait ``60`` minutes between each one. You can run this before the setup
finishes, and it will submit jobs once they are ready. Just give ``setup-ensemble`` a minute or so to initialize
the database.

.. _monitor_ensemble:

monitor-ensemble
^^^^^^^^^^^^^^^^

After ensembles have been launched the overall status can be monitored using

.. code-block:: bash

   $ monitor-ensemble config.yaml

This will display some basic information in a web page indicating what jobs are running,
queued, and if they have failed. You may have to open the page ``http://localhost:5006/ensemble_monitor``
if nothing appears and give it a minute to load. If you are using the ``canesm-ensemble`` conda
environment available on science, you will need to run this from ``gpsc-vis`` and use
`tlclient <http://wiki.cccma.ec.gc.ca/twiki/bin/view/Main/UsingGPSCVIS>`_ to view the status.

