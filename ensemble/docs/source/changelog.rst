.. _changelog:

What's New?
===========
v0.5.4
------
- Fix bug where executables are not save with correct date string when start month is not January

v0.5.3
------
- Fix bug where ``pp_rdm_num_pert`` was being set to ``None`` in ``phys_parm`` when left unset.


v0.5.2
------
- Deprecation warning for old CLI methods

v0.5.1
------
- Job submission can be delayed with the option ``submit_ensemble --delay delay_in_minutes``
- Failed/incomplete setups can now be resubmitted as-is and previously setup jobs will automatically be skipped
- Members can now be added to an esemble, see :ref:`here<add_members>`
- More options can now be set on a per-run basis (``run_directory``, ``tapeload``, ``phys_parm_from_local``, ``inline_diag_from_local``)
- Extended runs now use the initial rtdiag ``start_time``
- Numerous bug fixes to ``extend-ensemble``

v0.5
----
- Can now share executables using the option ``share_executables``
- Can now :ref:`extend ensembles<extend_ensemble>` with the command line tool ``extend-ensemble``
- Configuration files placed on remote now have global read permission
- Option to use :ref:`variables<var_assign>` in the ``canesm_cfg``, ``phys_parm``, ``inline_diag_nl`` files.
- Bug fixes to ``monitor-ensemble``

v0.4.5
------
- Fix bug where ``phys_parm`` and ``inline_diag_nl`` settings were not being set in the `$RUNPATH`.
  This bug was introduced in v0.4.4, so will not affect older versions.

v0.4.4
------
- Ensemble status database has been renamed to the first runid of the ensemble to avoid collisions
  when multiple ensembles are ran from the same folder
- Bug fix to allow arbitrary runids
- Bug fix for job submission: proper folder was not being ``cd``'d to when trying to submit

v0.4.3
------
- Bug fixes for job submission
- ``date_in_c`` and ``runid_in_c`` now also set for ``AMIP`` runs when ``save_restart`` is used
- ``date_in_a`` and ``runid_in_a`` now also set for ``ESM`` runs when ``save_restart`` is used
- ``setup-canesm`` now falls back to ``/home/scrd101/setup-canesm`` if command cannot be found

v0.4.2
------
- options in ``tapeload`` and ``save_restart`` files can now be set through the
  option ``restart_options``


v0.4.1
------
- ``*.tmp`` files now created in user directory ``user_data_dir\canesm-ensemble`` to avoid write
  permissions from shared environments
- Job submission database now stored on remote machine to avoid conflicts when setting jobs from
  different computers
- CI pipeline and the beginnings of a test suite
- Project now resides in ``CanESM`` so changes to install script are required

v0.4
----
- ``share_member_code`` is now ``True`` by default
- ``canesm.cfg``, ``phys_parm``, etc can now be set on a per run basis.
- A copy of the configuration file is now stored in the ``run_directory`` on the remote machine for posterity
- Setup verification: check that at least one parameter is different between ensemble members and raise an error if not true
- Option ``job_str_zeropad`` to change runid format
- Bug fixes:

  - Multiple variables on one line of config files now supported
  - Basefile options were not being set properly
  - Issue where older versions of sqlite (<3.23) would crash when accessing the database
  - ``submit-ensemble`` was not being properly configured
  - ``monitor-ensemble`` now included with ``pip install``


v0.3.4
------
- Support for submitting ensembles in batches using ``submit-ensemble``

v0.3.3
------
- Support for changes to cppdefs
- Bug fix where options with ``$`` in the value were not being set
- Use ``setup-canesm`` script from ``canesm_bin_latest`` when ``share_member_code`` is set
- Add auto-response for ``-p`` and ``-d`` options in tapeload /save_restart scripts and rename already
  existing folder

v0.3.2
------
- Support for changes in ``INLINE_DIAG_NL`` file
- Support for ``-p`` and ``-d`` options in ``save_restart`` and ``tapeload``

v0.3.1
------
- Support for monthly resolution
- ``phys_parm`` are now configured in both ``RUNPATH`` and ``WRK_DIR`` when set.
  Previously they may have only been set in ``WRK_DIR``.

v0.3
----
- Support for passing additional flags to ``canesm-setup`` using the ``setup_flags`` option
- Fix error where ``runmode`` variable could not be set by the config file
- Basic ensemble monitoring interface (beta version)

v0.2
----
- Support for sharing code between jobs
- Ability to setup a job from a table
- The README.md file in the ``run_directory`` is now updated after setup
- ``start_time`` and ``stop_time`` can now be provided as a list