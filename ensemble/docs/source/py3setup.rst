.. _setting_up_p3:

Setting up Python 3
===================

The steps below can be used to setup a python 3 installation

1. Check if you have `conda <https://docs.conda.io/projects/conda/en/latest/index.html>`_
   installed by running

   .. code-block:: bash

      conda --version

   from the command line. If you see something like

   .. code-block:: bash

       conda 4.6.8

   Then skip to step 2. If you see

   .. code-block:: bash

       conda: command not found

   Then you will need to install it. You can download a version of Anaconda from https://www.anaconda.com/distribution/.
   Or, install a minimal version from the command line using

   .. code-block:: bash

      wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh
      bash ~/miniconda.sh -b -p $HOME/miniconda
      export PATH="$HOME/miniconda/bin:$PATH"

2. Create a `conda environment <https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html>`_.
   To create a python 3.6 environment run the following line

   .. code-block:: bash

      $ conda create --name canesm-ensemble-env python=3.6

3. Activate the conda environment

   .. code-block:: bash

      $ source activate canesm-ensemble-env

You are now ready to install canesm-ensemble!

.. note::
   The ``canesm-ensemble-env`` environment will be active only for the current session,
   so will need to re-activated after closing.
