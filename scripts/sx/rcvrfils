#! /bin/sh

#    Dec 03/08 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#    Oct 16/06 - F.Majaess (Revised for ma/maia, ns/naos)
#    Mar 01/06 - F.Majaess (Revised for rg/rigel)
#    Jun 20/03 - F.Majaess (Revised for az/azur)
#    Feb 25/02 - F.Majaess (Added ya/yata)
#    Jan 05/00 - F.Majaess (Added kz/kaze)
#    Jan 21/99 - F.Majaess (Added yo/yonaka)
#    Jul 14/97 - F.Majaess (Added sx/hiru)
#    Mar 18/97 - F.Majaess (Revise for group restructuring)
#    Mar 18/96 - F.Majaess (revise for SX-4)
#    Nov 17/94 - F. Majaess
 
#id  rcvrfils - Recovers files from back-end $CCRNTMP/recover.
 
#    AUTHOR - F. Majaess 
 
#hd  PURPOSE - "rcvrfils" submits a job to one of the back-end machines
#hd            to recover all user's files residing in '$path' 
#hd            subdirectory and officially save them in users's 
#hd            $RUNPATH subdirectory most likely mounted on the other 
#hd            back-end machine.
#hd 
#hd            No printout is returned unless the "outfil" switch is used.
 
#pr  PARAMETERS:
#pr  
#pr    PRIMARY
#pr   
#pr      Note: One of 'ma','za','sa','al','af' or 'ib' is to be specified.
#pr
#pr          ma = to target job to IBM (maia).
#pr
#pr          za = to target job to IBM (zeta).
#pr
#pr          sa = to target job to IBM (saiph).
#pr
#pr          al = to target job to IBM (algol).
#pr
#pr          ha = to target job to IBM (hadar).
#pr
#pr          sp = to target job to IBM (spica).
#pr
#pr          af = to target job to Linux (alef).
#pr
#pr          ib = to target job to Linux (dorval-ib).
#pr
#pr  fil_grpname= User has the option of specifying one of
#pr               'ccrn_shr/ccrn_stf/ccrn_net/ccrn_rcm/ccrn_mam/ccrn_gst'
#pr               group ownership for officially saved recovered 
#pr               files.
#pr               (=$DEFFGRP).
#pr
#pr        time = time limit for job in seconds (=60)
#pr  
#pr         mem = memory limit for job in megabytes (=25)
#pr  
#pr        path = path of directory to recover from
#pr               (='$CCRNTMP/recover')
#pr
 
#ex  EXAMPLE: 
#ex  
#ex  rcvrfils fil_grpname='ccrn_shr' ma
#ex 
#ex  The above example transfers all user's files from $CCRNTMP/recover
#ex  on MAIA into $RUNPATH on the back-end and save with "ccrn_shr"
#ex  group ownership.

#  * Obtain any specified option. 

for arg in $@
do
  case $arg in
      -x) set $arg                     ;;
     *=*) eval $arg                    ;;
      ma) mdest=${mdest:='maia'}       ;;
      za) mdest=${mdest:='zeta'}       ;;
      sa) mdest=${mdest:='saiph'}      ;;
      al) mdest=${mdest:='algol'}      ;;
      ha) mdest=${mdest:='hadar'}       ;;
      sp) mdest=${mdest:='spica'}       ;;
      ib) mdest=${mdest:='dorval-ib'}  ;;
      af) mdest=${mdest:='alef'}       ;;
       *) echo " Argument $arg ignored"
  esac
done

#  * Set the defaults.

time=${time:=60}
mem=${mem:=25}
path=${path:='$CCRNTMP/recover'}
 
#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
  echo "please enter a destination; ma/za/sa/al/ha/sp/ib/af: > \\c"
  read tmdest
  case $tmdest in
      ma) mdest='maia'                  ;;
      za) mdest='zeta'                  ;;
      sa) mdest='saiph'                 ;;
      al) mdest='algol'                 ;;
      ha) mdest='hadar'                 ;;
      sp) mdest='spica'                 ;;
      ib) mdest='dorval-ib'             ;;
      af) mdest='alef'                  ;;
       *) echo "illegal destination ! " ;;
  esac
done
 
#  * Construct and submit the job.

if [ "$mdest" = 'maia' ] ; then
  outdest=$OUTPUTQ/rcvrfils.ma.$$
elif [ "$mdest" = 'zeta' ] ; then
  outdest=$OUTPUTQ/rcvrfils.za.$$
elif [ "$mdest" = 'saiph' ] ; then
  outdest=$OUTPUTQ/rcvrfils.sa.$$
elif [ "$mdest" = 'algol' ] ; then
  outdest=$OUTPUTQ/rcvrfils.al.$$
elif [ "$mdest" = 'hadar' ] ; then
  outdest=$OUTPUTQ/rcvrfils.ha.$$
elif [ "$mdest" = 'spica' ] ; then
  outdest=$OUTPUTQ/rcvrfils.sp.$$
elif [ "$mdest" = 'dorval-ib' ] ; then
  outdest=$OUTPUTQ/rcvrfils.ib.$$
elif [ "$mdest" = 'alef' ] ; then
  outdest=$OUTPUTQ/rcvrfils.af.$$
else
  outdest=$OUTPUTQ/rcvrfils.sss.$$
fi
# fil_grpname=${fil_grpname:='ccrn_shr'}
DEFFGRP=${DEFFGRP:='ccrn_shr'}
fil_grpname=${fil_grpname:=$DEFFGRP}
if [ -n "$fil_grpname" ] ; then
 Tfil_grpname="fil_grpname=$fil_grpname && export fil_grpname "
else
 Tfil_grpname=" : "
fi

cd $HOME/tmp
qsub -q $mdest -lT $time -lM ${mem}mb -r rcvrfils \
               -eo -o $outdest <<endjob
#!/bin/sh
#
set -x
$Tfil_grpname 
#
Stamp="\${HOSTID}_"\`date +%Y%j%H%M%S\`
mkdir -m 755 \$CCRNTMP/tmp_rcvrfils_\$\$_\$Stamp
#
#  * Obtain line containing the absolute path to the desired file of the 
#  * specified edition. The latest edition is used if the edition number
#  * is not specified.
#
#  * Obtain a list of all user's files in $path.
#
files=`ls -l $path/*[0-9]* 2>/dev/null | grep $USER | sed -n -e 's/^.* //p'`
#
#  * Attempt official save on the files found (if any)
#  * Abort if there is a problem saving the file(s).
#
if [ -n "$files" ] ; then
  for file in $files
  do
    echo "Processing file: $file"
    pdn=`expr //$file : '.*/\(.*\).[0-9][0-9][0-9]'`
    mv $file lfn
    save lfn pdn
    if [ -f $file ] ; then
     echo "Please retry later"
     break
    fi
    release lfn
  done
else
  echo 'No files found'
fi
#
cd \$CCRNTMP
rm -rf tmp_rcvrfils_\$\$_\$Stamp
#
( rmllfls || : )
exit
endjob

exit
