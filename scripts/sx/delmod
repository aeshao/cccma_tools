#! /bin/sh

#    Dec 03/08 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#    Oct 16/06 - F.Majaess (Revised for ma/maia, ns/naos)
#    Mar 01/06 - F.Majaess (Revised for rg/rigel)
#    Feb 15/05 - F.Majaess (Revised for AIX/Linux setup in Victoria)
#    Nov 24/03 - F.Majaess (Added "cs" to the list ...)
#    Jun 20/03 - F.Majaess (Revised for az/azur)
#    Feb 25/02 - F.Majaess (Revised for ya/yata)
#    Mar 28/01 - F.Majaess (Added "cb,fb,ib" to the list ...)
#    Sep 14/00 - F.Majaess (Revised for tu/tsunami)
#    Jan 05/00 - F.Majaess (Added kz/kaze)
#    Jan 21/99 - F.Majaess (Added yo/yonaka)
#    Dec 08/98 - F.Majaess (Added "td" to the list ...)
#    Apr 23/98 - F.Majaess (Added "iv" to the list ...)
#    Dec 02/97 - F.Majaess (Added "gz" to the list ...)
#    Jul 14/97 - F.Majaess (Added sx/hiru)
#    Mar 14/96 - F.Majaess (Added sx4)
#    Jan 08/96 - MB,FM (Revise to use access/delete)
#    Jun 02/95 - F.Majaess (Added orion, gandalf & aragorn)
#    Jan 04/95 - F.Majaess (Delete ob only if the rs option is used)
#    Nov 08/94 - F.Majaess (Modify for new destination identifiers)
#    Jun 14/94 - E. Chan (Delete script and nmf files if the rs option is used)
#    Dec 10/92 - E. Chan (Delete an and ab files only if the rs option is used)
#    Nov 25/92 - E. Chan (Modify to delete files and their associated links
#                         on the official data directory)
#    Nov 19/92 - E. Chan   
 
#id  delmod  - Removes a family of model files associated with a model
#id            run from any official CCRD/N file system and their 
#id            corresponding links on the official data directory 
 
#    AUTHOR - E. Chan  
 
#hd  PURPOSE - "delmod" submits a job to one of the local machine(s) at the 
#hd            local site to delete files related to a particular model run 
#hd            from any official CCRD/N file system. Their corresponding 
#hd            symbolic links on the official CCRD/N data directory are also 
#hd            removed.
#hd 
#hd            By default, the latest edition of each file is removed.
#hd            Although an optional parameter "ed" may be used to delete  
#hd            other editions, this usually is not very useful since the 
#hd            files in the same family normally do not have the same 
#hd            edition numbers.
#hd  
#hd            The affected files must start with "model" and end (if the
#hd            edition number is disregarded) with en, gs, gz, iv, ss, td, 
#hd            or cm. Other model files (ab, ob, rs_nmf, cb, fb, ib, and 
#hd            model batch script) may also be deleted, including the model 
#hd            restart files (rs, ro, os, ia) by using the "rs" option.
#hd  
#hd            No printout is returned unless the "outfil" switch is used.
 
#pr  PARAMETERS:
#pr  
#pr    POSITIONAL     
#pr  
#pr       model = family name of permanent files to be deleted
#pr  
#pr    PRIMARY
#pr
#pr      Note: One of 'ha/sp/po/ca/mz/hr/br/p1/p2'  in Dorval.
#pr            In Victoria, default destination: "$HOSTID"  for Linux.
#pr
#pr          hr = to target job to hare.
#pr
#pr          br = to target job to brooks.
#pr
#pr          p1 = to target job to ppp1.
#pr
#pr          p2 = to target job to ppp2.
#pr
#pr          ha = to target job to IBM (hadar).
#pr
#pr          sp = to target job to IBM (spica).
#pr
#pr          po = to target job to Linux (pollux).
#pr
#pr          ca = to target job to Linux (castor).
#pr
#pr          mz = to target job to Linux (mez).
#pr
#pr   
#pr        time = time limit for job in seconds (=200)
#pr  
#pr         mem = memory limit for job in megabytes (=25)
#pr  
#pr          ed = edition number of the model files (defaults to the latest
#pr               editions)
#pr  
#pr        path = path of data directory (='$RUNPATH') 
#pr 
#pr    SECONDARY
#pr 
#pr          rs = switch to delete model restart files 
#pr  
#pr         adt = switch to produce an audit of back-end disk space usage
#pr  
#pr      outfil = switch to force return of printout

#ex  EXAMPLE: 
#ex  
#ex  delmod mmm17 rs po
#ex 
#ex  The above example deletes on "pollux", the latest editions of all model 
#ex  files associated with run "mmm17". The corresponding symbolic links 
#ex  are removed from the official data directory. 
#ex  
 
#  * Obtain any specified option. 

for arg in $@
do
  case $arg in
        -*) set $arg                            ;;
       *=*) eval $arg                           ;;
      ed=*) ed=`expr $arg : 'ed=\([0-9]*\)'`    ;;
        rs) rs=yes                              ;;
       adt) adt=yes                             ;;
        hr) mdest=${mdest:='hare'}             ;;
        br) mdest=${mdest:='brooks'}             ;;
        p1) mdest=${mdest:='ppp1'}             ;;
        p2) mdest=${mdest:='ppp2'}             ;;
        ha) mdest=${mdest:='hadar'}             ;;
        sp) mdest=${mdest:='spica'}             ;;
        po) mdest=${mdest:='pollux'}            ;;
        ca) mdest=${mdest:='castor'}         ;;
        mz) mdest=${mdest:='mez'}              ;;
    mamccc) mdest=${mdest:='mamccc'}            ;;
    odin) mdest=${mdest:='odin'}            ;;
    climate) mdest=${mdest:='climate'}            ;;
    outfil) outfil=yes                          ;;
         *) model=${model:=$arg}
  esac
done

#  * Set the defaults.

time=${time:=200}
mem=${mem:=25}
path=${path:='$RUNPATH'}
nonqs=${nonqs:=$NONQS}
nonqs=${nonqs:='no'}
rs=${rs:='no'}

#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
 if [ "$SITE_ID" = 'Victoria' ] ; then
    mdest="$HOSTID"
 elif [ "$SITE_ID" = 'York' ] ; then
   mdest="$HOSTID"
 elif [ "$SITE_ID" = 'Utor1' -o "$SITE_ID" = 'UOFT' ] ; then
   mdest="$HOSTID"
 elif [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
  echo "please enter a destination; ha/sp/po/ca/mz/hr/br/p1/p2: > \\c"
  read tmdest
  case $tmdest in
      hr) mdest='hare'                  ;;
      br) mdest='brooks'                ;;
      p1) mdest='ppp1'                  ;;
      p2) mdest='ppp2'                  ;;
      ha) mdest='hadar'                 ;;
      sp) mdest='spica'                 ;;
      po) mdest='pollux'                ;;
      ca) mdest='castor'                ;;
      mz) mdest='mez'                   ;;
       *) echo "illegal destination ! " ;;
  esac
 elif [ "$SITE_ID" = 'Downsview' ] ; then
    mdest="$HOSTID"
 else
  echo "please enter a destination: > \\c"
  read mdest
 fi
done

if [ "$mdest" = 'hare' ] ; then
  ext=.hr
elif [ "$mdest" = 'brooks' ] ; then
  ext=.br
elif [ "$mdest" = 'ppp1' ] ; then
  ext=.p1
elif [ "$mdest" = 'ppp2' ] ; then
  ext=.p2
elif [ "$mdest" = 'hadar' ] ; then
  ext=.ha
elif [ "$mdest" = 'spica' ] ; then
  ext=.sp
elif [ "$mdest" = 'pollux' ] ; then
  ext=.po
elif [ "$mdest" = 'castor' ] ; then
  ext=.ca
elif [ "$mdest" = 'mez' ] ; then
  ext=.mz
fi

#  * Prompt for a "model" if none was specified.
 
while [ -z "$model" ] 
do
  echo "please enter a value for model: > \\c"
  read model
done

#  * Translate "model" to lower case.

model=`echo $model | tr '[A-Z]' '[a-z]'`

#  * Remove leading zeros in the edition number and ensure that  
#  * three digits are used if it is specified.
 
if [ -n "$ed" ] ; then
  ed=`expr $ed + 0`
  if [ "$ed" -lt 100 ] ; then
    ed=0$ed
  fi
  if [ "$ed" -lt 10 ] ; then
    ed=0$ed
  fi
  Ted="ed=$ed"
fi

#   * Check parameter values.

chkrl=" delmod model=$model ed=$ed  "
chkrl=" $chkrl  adt=$adt rs=$rs mdest=$mdest"
if [ "$SITE_ID" = 'York' ] ; then
 . checker | more 
elif [ "$SITE_ID" = 'Victoria' -o "$SITE_ID" = 'Downsview' ] ; then
 . checker 
elif [ \( "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' \) -a "$OS" = 'Linux' ] ; then
 . checker 
else
 . checker | more 
fi

#  * Construct and submit the job.

  if [ "$nonqs" = 'yes' ] ; then
# if [ "$nonqs" = 'yes' -a "$OS" != 'Linux' ] ; then
  if [ -z "$outfil" -a -z "$adt" ] ; then
    outdest='1>>/dev/null 2>>/dev/null'
  elif [ -z "$adt" ] ; then
    outdest="1>>$OUTPUTQ/delmod_${model}${ext}.$$ 2>>$OUTPUTQ/delmod_${model}${ext}.$$"
  else
    outdest="1>>$OUTPUTQ/delmod_${model}${ext}.$$ 2>>/dev/null"
  fi
  if [ "$OS" = 'Linux' ] ; then
   Batch_job="/tmp/tmp_delmod_job$$"
   RM_Batch_job="rm -f $Batch_job"
   line="cat > $Batch_job ; chmod u+x $Batch_job ; nohup $Batch_job $outdest & "
  else
   RM_Batch_job=':'
   line="nohup sh -s - $outdest & "
  fi
else
  if [ -z "$outfil" -a -z "$adt" ] ; then
    if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
      outdest='-eo -o $mdest:/dev/null'
    else
      outdest='-eo -o /dev/null'
    fi
  elif [ -z "$adt" ] ; then
    outdest="-eo -o $OUTPUTQ/delmod_${model}${ext}.$$"
  else
    if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
      outdest="-o $OUTPUTQ/delmod_${model}${ext}.$$ -e $mdest:/dev/null"
    else
      outdest="-o $OUTPUTQ/delmod_${model}${ext}.$$ -e /dev/null"
    fi
  fi
  line="$CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r delmod $outdest"
fi

cd $HOME/tmp
# $CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r delmod.$model \
# $CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r delmod \
#               $outdest <<endjob
(cat << endjob) | eval "$line"
#!/bin/sh
#
if [ -n "$outfil" ] ; then
  set -x
fi
#
Stamp="\${HOSTID}_"\`date +%Y%j%H%M%S\`
mkdir -m 755 \$CCRNTMP/tmp_delmod_\$\$_\$Stamp
cd \$CCRNTMP/tmp_delmod_\$\$_\$Stamp
#
#  * Access and remove the desired files.
#  * The latest edition is used if the edition number is not specified.
#
for f in en gs gz iv ss td cm
  do
     access delmod\$f ${model}\$f $Ted na nocp 2>/dev/null
     delete delmod\$f na
     release delmod\$f
  done
#
#  * Remove restart files associated with "model" if desired.
#
# if [ -n "$rs" ] ; then
if [ "$rs" = 'yes' ] ; then
  for f in an ab ob os cb cs fb ib rs ts ro ia _script rs_nmf ts_nmf
    do
      access delmod\$f ${model}\$f $Ted na nocp 2>/dev/null
      delete delmod\$f na
      release delmod\$f
    done
fi
#
#
#  * Generate an audit of user's files in directory if desired.
#
cd \$CCRNTMP
\rm -r -f \$CCRNTMP/tmp_delmod_\$\$_\$Stamp
cd $path 
if [ -n "$adt" ] ; then
  auditor own full
fi 
#
( rmllfls || : )
$RM_Batch_job
exit
endjob

exit
