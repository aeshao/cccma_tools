#! /bin/sh
 
#    Jun 11/96 - F.Majaess
 
#id  chkcode - Checks the contents of files with similar names.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "chkcode" script is mainly used to help in ensuring
#hd            files with the same names contain the same code by
#hd            having it reporting differences in contents.
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      filname = filename to process.
#pr                (ie. valid entry for -name switch in find utility)
#pr
#pr    PRIMARY 
#pr
#pr      dirname = path and directory name to scan.
#pr                (defaults to current directory) 
#pr
#pr
#ex  EXAMPLE: 
#ex
#ex    chkcode filname='*.f' dirname=$CCRNSRC/source_irix/lssub/local
#ex
#ex   The above will result in simply checking all '.f' files with 
#ex   the same name for consistency in their contents.

AWK=${AWK:-awk}

#  * Obtain the file names and any specified option.

arg_list=$@
for arg in $arg_list
do
  case $arg in
      -*) set $arg                 ;;
     *=*) eval $arg                ;;
       *) tmp1=${filname:+$arg} ; 
          filname=${filname:=$arg}
  esac
done
 
#   * Prompt for a positional parameter value(s) if none was/were
#   * specified...

while [ -z "$filname" ] ; do
  echo "please enter filename to be processed  > \\c"
  read filname
done

#   * Ensure "dirname" is a valid directory...

dirname=${dirname:=`pwd`}
if [ ! -d "$dirname/." ] ; then
  echo "Abort in 'chkcode': Sorry, $dirname must be a valid directory!"
  exit 1
fi

#   ****   Task of the script...   ****
 
excode=0
cd $dirname
Cwd=`pwd`
Stamp="${HOSTID}_"`date +%Y%j%H%M%S`
Tmpdir="$CCRNTMP/tmp_chkcode_$$_$Stamp"
mkdir -m 755 $Tmpdir &&
cd $Tmpdir &&
(\rm -f Files_Differ || : ) &&
find -L ${Cwd}/. -name $filname -exec obtnfil nocp {} \; || exit 10
if [ -f Files_Differ ] ; then
 echo "Abort in chkcode: conflict in the contents of files sharing the same name!"
 excode=3
fi
cd $Cwd
\rm -rf $Tmpdir
exit $excode
