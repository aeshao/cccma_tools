#! /bin/sh

#    Aug 12/99 - F.Majaess 
#
#id  conv_rpn2ccc - Used to convert RPN datafile into CCRN standard binary format. 
#
#    AUTHOR  - F.Majaess.
#
#  PURPOSE - "conv_rpn2ccc" script, given the RPN filename in the first
#            argument, it produces from it based on the second argument
#            a CCRN standard binary format file.
#            Note: Only available on pollux.
#
#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     arg1    = Input; path/filename of RPN datafile.
#pr
#pr     arg2    = Output; path/filename of CCRN standard binary format datafile.
#pr
#pr     arg3    = Switch to control adding cyclical longitude.
#pr               "nolaslon" is the only valid setting for it.
#pr               (default: is to add cyclical longitude)
#pr

#ex EXAMPLE:
#ex
#ex   conv_rpn2ccc rpnfile ccrnfile [nolaslon]
#ex
#

# echo " no. or arfguments = $#"
if [ $# -lt 2 ] ; then
 echo "conv_rpn2ccc: Invalid call: $@ !"
 exit 1
else
 if [ $# -gt 2 ] ; then
   if [ "$3" == "nolaslon" ] ; then
    nolaslon='yes'
   else
    echo "conv_rpn2ccc: Invalid third argument $3 !"
    exit 2
   fi
 fi
fi
if [ ! -s $1 ] ; then
 echo "conv_rpn2ccc: Sorry, input file $1 is non existing or empty!"
 exit 3
fi
if [ -f $2 ] ; then
 echo "conv_rpn2ccc: $2 already exists, please remove and re-invoke"
 exit 4
fi
ARMNLIB=/usr/local/env/armnlib
export ARMNLIB
PATH="${PATH}:${ARMNLIB}:${ARMNLIB}/bin:${ARMNLIB}/scripts"
# Note: TMPDIR must be defined.
TMPDIR=`pwd`
export TMPDIR 
set -x
if [ "$nolaslon" == 'yes' ] ; then
 r.diag convert -rpn $1 -ccrn $2 -keep rpn -info ERRORS -opktyp PK92 
else
 r.diag convert -rpn $1 -ccrn $2 -keep rpn -info ERRORS -laslon -opktyp PK92
fi &&
exit || exit 5
