#!/bin/sh

#    Dec 05/06 - F.Majaess (Added support for "source_asis" code)
#    Jun 11/99 - F.Majaess (Added "sublib").
#    Aug 21/96 - F.Majaess

#id viewcode - Used to invoke "getcode" script with viewing option
#              turned on. 

#    AUTHORS - D.Liu,F.Majaess

#hd  PURPOSE - "viewcode" invokes "getcode" script with viewing option
#hd            enabled to view the requested file. Please refer to "getcode"
#hd            documentation for the full list of switches allowed.

#pr  PARAMETERS:
#pr 
#pr    POSITIONAL
#pr 
#pr        fil  = name of subroutine, function or program (i.e. name of file to
#pr               be found, without the ".f" or ".vd" extensions).
#pr 
#pr        lib  = subdirectory in $CCRNSRC/source$OSbin to start search from (i.e.
#pr               lssub for subroutines and functions, lspgm for programs, 
#pr               lsmod for GCM fortran source code in update form, and 
#pr               diag4 for diagnostic COS jcl).
#pr               ($CCRNSRC/source_asis is targeted if "_asis" suffix is specified)
#pr 
#pr      sublib = (optional); subdirectory residing ( not necessarily immediately)
#pr               under $CCRNSRC/source$OSbin/$lib to start search for "fil" from 
#pr               instead of the default $CCRNSRC/source$OSbin/$lib.
#pr               (i.e. if lib=lssub and sublib=gcm6u; target search subdirectory 
#pr                     will be $CCRNSRC/source$OSbin/lssub/.../gcm6u )

#pr 
#pr    SECONDARY
#pr 
#pr         vd  = switch to access variable dimensioned program code
#pr 
#pr       lnum  = switch to enable generation of identification line numbers 
#pr               (useful if making modifications to the source code using update). 
#pr  
#pr      print  = switch to generate hardcopy printout by automatically invoking 
#pr               script "lsh". 
#pr
#pr      view   = switch enabling just viewing the code.
#pr
#pr      vwr    = user's preferred viewer utility (=$PAGER).
#pr               (used only if "view" switch is on)

#ex  EXAMPLES:
#ex 
#ex   1) viewcode gaussg lssub
#ex 
#ex      The above example leads to viewing a copy of the source code for 
#ex      the subroutine "gaussg" by doing a find for the "gaussg.f" file, 
#ex      starting from $CCRNSRC/source$OSbin/lssub and invoking the viewer 
#ex      utility indicated by "$PAGER" variable on it.

#ex   2) viewcode gpqtz diag4 lnum vwr=vi
#ex 
#ex      The above example gets the source code for the deck "gpqtz" by executing
#ex      a find for the "gpqtz.dk" file starting at $CCRNSRC/source$OSbin/diag4. 
#ex      The source code is stamped with identification line numbers beginning 
#ex      at column 83.  Then the copy is viewed by "vi" before being deleted 
#ex      upon exiting.

#ex   3) viewcode rstrt6u lsmod gcm6u lnum vwr=vi
#ex 
#ex      The above example gets the source code for the deck "rstrt6u" by executing
#ex      a find for the "rstrt6u.dk" file starting at 
#ex      $CCRNSRC/source$OSbin/lsmod/.../gcm6u.
#ex      The source code is stamped with identification line numbers beginning at 
#ex      column 83.  Then the copy is viewed by "vi" before being deleted upon 
#ex      exiting.

#ex   4) viewcode progx lspgm_asis vwr=vi
#ex 
#ex      The above example gets the source code for "progx" by executing a
#ex      find for it within "$CCRNSRC/source_asis/lspgm_asis", then the copy 
#ex      is viewed by "vi" before being deleted upon exiting.

set -x
exec getcode view=yes $*
