#!/bin/sh

#   May 14/2008 - F.Majaess (Revised for sa/saiph, ib/dorval-ib)
#   Oct 16/2006 - F.Majaess (Revised for ma/maia, ns/naos)
#   Mar 01/2006 - F.Majaess (Revised for rg/rigel)
#   Jun 25/2003 - F.Majaess (Revised for az/azur and "openmp" support)
#   Feb 25/2002 - F.Majaess (Revised for SX6)
#   Feb 20/2001 - F.Majaess (Revised to support "f90" on SX4s via "f90x4" switch)
#   Jan 29/2001 - F.Majaess (Revised to support SXCROSS_KIT/SXF90_VER)
#   Feb 23/2000 - F.Majaess (Revised for "sxf90")
#   Jun 14/1999 - F.Majaess (Added "modver" support)
#   Jan 21/1999 - F.Majaess (Added yo/yonaka)
#   May 04/1998 - F.Majaess (Removed o2000-2)
#   Mar 27/1998 - F.Majaess (Added o2000-2)
#   Jul 14/1997 - F.Majaess (Revise for hiru; disable ieee64 switch)
#   May 06/1997 - F.Majaess (Add "multi" switch for SX4)
#   Oct 19/1996 - F.Majaess (revise to accept input=... )
#   Apr 04/1996 - F.Majaess (revise for SX-4)
#   Dec 08/1995 - F.Majaess (Replace 'hostname' calls by "$HOSTID")
#   Oct 26/1995 - F.Majaess (Implement "nopack" & modify for IBM RS6000)
#   Nov 08/1994 - F.Majaess (Modify for sx3r)
#   Jul 08/1993 - F. Majaess (add "$LINKEXT" to library list)
#   Oct 15/1992 - E. Chan (add capability to pass additional options to the
#                        compiler and add option to generate listings)
#   Mar 24/1992 - E. Chan

#id nogo    - creates a binary executable from source code in batch mode 

#   AUTHOR  - E. Chan

#hd PURPOSE - "nogo" generates an executable from Fortran and/or C source 
#hd           code. The source code is taken from "Input_Cards" which is 
#hd           a file used in batch mode to contain all input card images
#hd           used by the batch job (see the subroutine JCLPNT).
#hd 
#hd           Compiler options (preceded by a dash) may be specified on the 
#hd           "nogo" command line. These are passed on to the compiler.
#hd           If an option requires the specification of an associated value
#hd           separated from the option by blanks, then the blanks must be 
#hd           replaced with underscores (see example below).
#hd

#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr          a = name of executable to be generated
#pr   
#pr   PRIMARY
#pr
#pr     modver = Model libary version to target for linking.
#pr             (=$DEFMODL).
#pr   
#pr   SECONDARY
#pr
#pr       input= name of the file to be compiled (must have a valid filename).
#pr       list = switch to turn on compiled listings
#pr     nopack = switch used to disable packing by enforcing packing density 
#pr              of 1 via linking to revised version of "recpk2" subroutine;
#pr              (=no/yes)
#pr
#pr   Note: If neither "float1","float2" [nor "openmp"] 
#pr         (see below) is specified, the default
#pr         for hadar/spica/pollux/castor/mez: 64-bits real/ 32_bits integer;
#pr         (ie. whatever mode "$F77" is set to).
#pr
#pr     openmp = switch to compile the code in "openmp" 32-bits (real/integer)
#pr              mode if "float1=yes", otherwise 64-bits mode. This switch is 
#pr              currently valid only on hadar,spica,pollux,castor,mez
#pr              at CMC as well as under AIX O/S.
#pr              (='no'/'yes')
#pr     float1 = switch to compile the code in 32-bits (real/integer) mode.
#pr              It can be used in conjunction with "openmp=yes".
#pr              (='no'/'yes')
#pr     float2 = switch to compile the code in 64-bits (real/integer)
#pr              It's intended to be used on "hadar/ha","spica/sp","pollux/po",
#pr              "castor/ca","mez/mz"  or other suitable targets.
#pr              (='no'/'yes')
#pr   noxlfimp = switch valid only under AIX on one of the IBM clusters at CMC.
#pr              It's used to aid in getting better debugging trace in case of 
#pr              executable abort.
#pr              (='no'/'yes')
#pr
#pr      p5lib = switch valid only under AIX on one of the IBM clusters at CMC.
#pr              It's used to allow linking to P5 captured "essl/mass/blas" 
#pr              libraries instead of the default set under "/usr/lib".
#pr              (='no'/'yes')
#pr

#ex EXAMPLE:
#ex   
#ex     nogo a=pgm list
#ex  
#ex     The above example extracts the source code from the file "Input_Cards"
#ex     and and passes it to the Fortran compiler. The name of the executable
#ex     is "pgm". A compiled listing of the program is generated.
#ex  
#ex     nogo pgm -NO -s -L_page list modver=gcm6u 
#ex 
#ex     As in the first example, the executable "pgm" is generated, but 
#ex     three additional compiler options have been specified. Because
#ex     the "-L" option requires a following suboption separated by whitespace, 
#ex     the whitespace must be replaced with a blank. The linking targets 
#ex     the appropriate "gcm6u" archive library.
#ex    
#ex     nogo a=pgm float1 openmp input=pgm_src
#ex
#ex     The above example compiles the source code provided in "pgm_src"
#ex     input file in openmp 32-bits (real/integer) mode and generates
#ex     the binary executable in "pgm".

#    Temporary sample for IBM@CMC:
#    nogo a=add xlfver=xlf12104 lvcode_dir=new_lvcode_dir98 input=add_src

#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script). 

IFS=' '
export IFS

#  * Obtain the filename and any specified option.

for arg in $@
do
  case $arg in
       *=*) eval $arg                        ;;
        -*) arg=`echo $arg | tr '_' ' '`     ;
            options="$options $arg"          ;;
      LIST|list) list='yes'                  ;;
    NOPACK|nopack) nopack='yes'              ;;
    FLOAT1|float1) float1='yes'              ;;
    FLOAT2|float2) float2='yes'              ;;
    OPENMP|openmp) openmp='yes'              ;;
    NOXLFIMP|noxlfimp) noxlfimp='yes'        ;;
    P5LIB|p5lib) p5lib='yes'                 ;;
         *) a=${a:=$arg}     
  esac
done
 
#  * Set the defaults.

nopack=${nopack:=no}
float1=${float1:=no}
float2=${float2:='no'}
openmp=${openmp:='no'}
noxlfimp=${noxlfimp:='no'}
if [ "$noxlfimp" = 'on' ] ; then
  noxlfimp='yes'
elif [ "$noxlfimp" = 'off' ] ; then
  noxlfimp='no'
fi
p5lib=${p5lib:='no'}
if [ "$p5lib" = 'on' ] ; then
 p5lib='yes'
elif [ "$p5lib" = 'off' ] ; then
 p5lib='no'
fi
modver=${modver:=$DEFMODL}
HOSTHW=${HOSTHW:=`uname -m`}
OS=${OS:=`uname -s`}
if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'AIX' ] ; then
 xlfver=${xlfver:="$XLFVER"}
#xlfver=${xlfver:='xlf12104'}
 # HOSTIDf=`echo $HOSTID | cut -c 1-3`
 HOSTIDf=`echo $HOSTID | sed -e 's/eccc.*-ppp/cs/g' | cut -c 1-3`
#xlfver=${xlfver:="$XLFVER"}
#if [ "$HOSTIDf" = 'c8f' -o "$HOSTIDf" = 'c1f' -o "$HOSTIDf" = 'c1h' -o "$HOSTIDf" = 'c1r' -o "$HOSTIDf" = 'c1s' -o "$HOSTIDf" = 'c2f' -o "$HOSTIDf" = 'c2h' -o "$HOSTIDf" = 'c2r' -o "$HOSTIDf" = 'c2s' ] ; then
# xlfver=${xlfver:='xlf13108'}
#else
# xlfver=${xlfver:='xlf12104'}
#fi
 if [ "$xlfver" = 'xlf10103' ] ; then
  echo " NOGO: Altered XLF target to version 10.1.0.3"
# xlfver='xlf10103'
 elif [ "$xlfver" = 'xlf12104' ] ; then
  echo " NOGO: Altered XLF target to version 12.1.0.4"
# xlfver='xlf12104'
 elif [ "$xlfver" = 'xlf13108' ] ; then
  echo " NOGO: Altered XLF target to version 13.1.0.8"
 else
  xlfver=""
 fi
else
 xlfver=""
fi
if [ -n "$lvcode_dir" ] ; then
 if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
  if [ ! -d "$CCRNSRC/$lvcode_dir/." ] ; then
   echo "" ; echo "  nogo: $CCRNSRC/$lvcode_dir is not a valid subdirectory!"
   exit 5
  fi
 else
  echo "nogo: lvcode_dir=$lvcode_dir is not valid on local site and will be ignored"
  lvcode_dir=''
 fi
fi

# Check and adjust key environment variables possibly 
# set as condef parameters to on/off instead of yes/no...

Nswitches=0
case $float1 in
 ON|on) float1='yes' 
        Nswitches=`expr $Nswitches + 1` ;;
 OFF|off) float1='no' ;;
  *) : ;;
esac
case $float2 in
 ON|on) float2='yes' 
        Nswitches=`expr $Nswitches + 1` ;;
 OFF|off) float2='no' ;;
  *) : ;;
esac
case $openmp in
 ON|on) openmp='yes' 
        if [ "$float1" != 'yes' ] ; then
         Nswitches=`expr $Nswitches + 1` 
        fi ;;
 OFF|off) openmp='no' ;;
  *) : ;;
esac
case $noxlfimp in
 ON|on) noxlfimp='yes' ;;
 OFF|off) noxlfimp='no' ;;
  *) : ;;
esac
if [ "$noxlfimp" = 'yes' -a "$OS" != 'AIX' ] ; then
  echo "NOGO: Sorry, noxlfimp=$noxlfimp can only be used under AIX!"
  exit 1
fi
case $p5lib in
 ON|on) p5lib='yes' ;;
 OFF|off) p5lib='no' ;;
  *) : ;;
esac
if [ "$p5lib" = 'yes' -a "$OS" != 'AIX' ] ; then
  echo "NOGO: Sorry, p5lib=$p5lib can only be used under AIX!"
  exit 1
fi

#

if [ "$Nswitches" -gt 1 ] ; then
  echo "" ; echo "  nogo: Problem, only one of float1/float2/openmp can be used!"
  (echo "" ; echo "  nogo: Problem, only one of float1/float2/openmp can be used!") >> haltit
  echo "" ; echo "  nogo: illegal use of float1=$float1, float2=$float2, openmp=$openmp !"
  (echo "" ; echo "  nogo: illegal use of float1=$float1, float2=$float2, openmp=$openmp !") >> haltit
  exit 1
fi
if test "$float2" != 'no' -a "$OS" != 'AIX' -a "$OS" != 'Linux' -a "$HOSTID" != 'halo' ; then
  echo "" ; echo "  nogo: Sorry, float2 switch is not a valid option with targeted OS !"
  (echo "" ; echo "  nogo: Sorry, float2 switch is not a valid option with targeted OS !") >> haltit
  exit 2
fi
if test "$openmp" != 'no' -a "$HOSTHW" != 'SX-6' -a "$OS" != 'AIX' -a "$OS" != 'Linux' ; then
  echo "" ; echo "  nogo: Sorry, openmp switch is not a valid option with targeted OS !"
  (echo "" ; echo "  nogo: Sorry, openmp switch is not a valid option with targeted OS !") >> haltit
  exit 4
fi

if [ -z "$input" ] ; then
  #  * Strip the code from the file "Input_Cards" and place it in
  #  * a temporary file.  Also strip off the next "*EOR" card image from 
  #  * "Input_Cards".
  
  sed -n "1,/^\*EOR/{
            /^\*EOR/!p
            /^\*EOR/s//%#%#%#%#%/
          }
          /%#%#%#%#%/,\${
            /%#%#%#%#%/!w Tmp1_File$$
          }"                           Input_Cards > Tmp2_file$$
  
  \mv Tmp1_File$$ Input_Cards
else
 if [ -s "$input" ] ; then
  cp $input Tmp2_file$$
 else
  echo " nogo: Problem file $input is not found!"
  echo " nogo: Problem file $input is not found!" >> haltit
  exit 4
 fi
fi
chmod u+w Tmp2_file$$

#  * If requested, override the default leveling routines...

if [ -n "$lvcode_dir" ] ; then
 cat $CCRNSRC/$lvcode_dir/lv*.f >> Tmp2_file$$
fi

#  * Possibly massage the source code ...

\mv Tmp2_file$$ code$$.f

if [ "$float1" = 'no' -a "$float2" = 'no' -a "$openmp" = 'no' ] ; then
 unset float
elif [ "$float1" = 'no' -a "$openmp" = 'no' ] ; then
 float='_float2'
elif [ "$float1" = 'no' ] ; then
 float='_openmp'
elif [ "$openmp" = 'no' ] ; then
  float='_float1'
else
  float='_float1_openmp'
  if [ "$modver" = 'gcm6u' ] ; then
   # modver='gcm15f' # Enable if necessary
   :
  fi
fi
if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'AIX' ] ; then
  F77Tmp="F77""$float"
  eval "F77Tmp=${F77Tmp}"
  if [ "$xlfver" = 'xlf10103' ] ; then
  # eval "F77${float}=\`echo ${F77}${float} | sed -e 's/-qfloat=nans:/-qfloat=/g' -e 's/-qflttrap=nanq:/-qflttrap=/g'\`"
    eval "F77${float}=\`echo \${$F77Tmp} | sed -e 's/-qfloat=nans:/-qfloat=/g' -e 's/-qflttrap=nanq:/-qflttrap=/g'\`"
  elif [ "$xlfver" = 'xlf12104' ] ; then
   if [ "$HOSTIDf" = 'c8f' -o "$HOSTIDf" = 'c1f' -o "$HOSTIDf" = 'c1h' -o "$HOSTIDf" = 'c1r' -o "$HOSTIDf" = 'c1s' -o "$HOSTIDf" = 'c2f' -o "$HOSTIDf" = 'c2h' -o "$HOSTIDf" = 'c2r' -o "$HOSTIDf" = 'c2s' ] ; then
    # eval "F77${float}=\`echo \${$F77Tmp} | sed -e 's/-qfloat=rrm:/-qfloat=nans:rrm:/g' -e 's/ -qxflag=ngenstub / /' -e 's/ -qfullpath / /' -e 's/ -qreport / /' -e 's/ -qnoescape / -qnoescape -qxflag=ngenstub /' -e 's/ -qdbg / -qdbg -qfullpath /' -e 's/:nofold /:nofold -qreport /'\`"
    eval "F77${float}=\`echo \${$F77Tmp} | sed -e 's/-qspillsize=[0-9]* /-qspillsize=32648 /' -e 's/-qfloat=rrm:/-qfloat=nans:rrm:/g' -e 's/ -qxflag=ngenstub / /' -e 's/ -qfullpath / /' -e 's/ -qreport / /' -e 's/ -qnoescape / -qnoescape -qxflag=ngenstub /' -e 's/ -qdbg / -qdbg -qfullpath /' -e 's/:nofold /:nofold -qreport /' -e 's/-qarch=auto /-qarch=pwr5x /' -e 's/-qarch=pwr7 /-qarch=pwr5x /' -e 's/-qtune=auto /-qtune=pwr5 /' -e 's/-qtune=pwr7 /-qtune=pwr5 /' -e 's/-qsimd=auto / /' \`"
   else
    eval "F77${float}=\`echo \${$F77Tmp} | sed -e 's/-qfloat=rrm:/-qfloat=nans:rrm:/g' -e 's/ -qxflag=ngenstub / /' -e 's/ -qfullpath / /' -e 's/ -qreport / /' -e 's/ -qnoescape / -qnoescape -qxflag=ngenstub /' -e 's/ -qdbg / -qdbg -qfullpath /' -e 's/:nofold /:nofold -qreport /'\`"
   fi
  elif [ "$xlfver" = 'xlf13108' ] ; then
   eval "F77${float}=\`echo \${$F77Tmp} | sed -e 's/-qfloat=rrm:/-qfloat=nans:rrm:/g' -e 's/ -qxflag=ngenstub / /' -e 's/ -qfullpath / /' -e 's/ -qreport / /' -e 's/ -qnoescape / -qnoescape -qxflag=ngenstub /' -e 's/ -qdbg / -qdbg -qfullpath /' -e 's/:nofold /:nofold -qreport /'\`"
# else
#  echo "" ; echo "  nogo: Sorry, a valid XLF version must be specified via 'xlfver' parameter!"
#  touch haltit
# (echo "" ; echo "  nogo: Sorry, a valid XLF version must be specified via 'xlfver' parameter!") >> haltit
#  exit 6 
  fi
  if [ "$noxlfimp" = 'yes' ] ; then
   eval "F77${float}=\`echo \${F77${float}} | sed -e 's/:imprecise//g' | sed -n -e 's/:imp//g' -e 's/-bnoquiet//g' -e '1,\$p'\`"
  fi
fi
export float
# set -x
F77cut="F77""${float}"
eval "F77cut=\${${F77cut}}"
F77cut=`echo "${F77cut}" | $AWK '{ print $1 ; }'`
F77cut=`basename ${F77cut} | sed -e 's/^.*\(pgf\).*$/\1/g'`
# set +x

fix77 code$$.f
## if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'Linux' -a "$HOSTIDf" != 'ib3' -a "$HOSTIDf" != 'ib4' -a "$HOSTIDf" != 'ib8' ] ; then
##  eval "`/opt/ssm/all/bin/ssmuse sh -d /home/ordenv/ssm-domains0/ssm-pgi-722`" >> /dev/null
##  if [ -z "$PGI_BASE" ] ; then
##  #PGI_BASE='/home/dormrb02/ssm-pgi-710/PGI-CMC_7.1-6_linux24-i386'
##   PGI_BASE='/home/ordenv/ssm-domains0/ssm-pgi-722/PGI-CMC_7.2-2_linux24-i386'
##   PGI="$PGI_BASE/etch_static"
##   FORCE_PGI="$PGI"
##   FORCE_LM_LICENSE_FILE='27000@mrblmserv.cmc.ec.gc.ca'
##   export PGI_BASE PGI FORCE_PGI FORCE_LM_LICENSE_FILE
##  fi
## fi

#  * Enable production of compiler listing output ...

if [ "$OS" = 'AIX' ] ; then
 lstswtch=' -qsource '
 AIX_LOCAL=$LOCAL
# elif [ "$OS" = 'IRIX64' ] ; then
#  lstswtch=' -listing '
elif [ "$OS" = 'Linux' -a \( "$SITE_ID" = 'ORNS' -o "$F77cut" = 'pgf' \) ] ; then
 lstswtch=' -Mlist '
fi
# set -x

#  * Force nopacking in the executable if so requested ....

if [ "$nopack" = 'yes' ] ; then
 pkdoto="$RTEXTBLS/m_recpk2${float}.o"
fi

 if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'AIX' ] ; then
  if [ "$xlfver" = 'xlf10103' ] ; then
   . suppress_compilers_setup
   . xlf10103_setup
  elif [ "$xlfver" = 'xlf12104' ] ; then
   . suppress_compilers_setup
   . xlf12104_setup
  elif [ "$xlfver" = 'xlf13108' ] ; then
   . suppress_compilers_setup
   . xlf13108_setup
# else
#  . suppress_compilers_setup
#  . xlf12104_setup
  fi
  xlf90 -qversion
 fi
#  * Run the compiler.
# set -v
# set -x
if [ "$float1" = 'no' -a "$float2" = 'no' -a "$openmp" = 'no' ] ; then
  if [ -n "$modver" ] ; then
#  LOMODEL_ver=`eval echo '${LOMODEL'"${Tf90x4}"'}' | eval sed 's/_model/_model_${modver}/g'`
   LOMODEL_ver=`echo ${LOMODEL} | eval sed 's/_model/_model_${modver}/g'`
  else
#  LOMODEL_ver=`eval echo '${LOMODEL'"${Tf90x4}"'}'`
   LOMODEL_ver="${LOMODEL}"
  fi
  if [ "$xlfver" = 'xlf10103' ] ; then
   LOPLOT=`echo $LOPLOT | sed -e 's/\.a/_xlf10103.a/'`
   LODIAG=`echo $LODIAG | sed -e 's/\.a/_xlf10103.a/'`
   LOCOMM=`echo $LOCOMM | sed -e 's/\.a/_xlf10103.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf10103.a/'`
  elif [ "$xlfver" = 'xlf12104' ] ; then
   LOPLOT=`echo $LOPLOT | sed -e 's/\.a/_xlf12104.a/'`
   LODIAG=`echo $LODIAG | sed -e 's/\.a/_xlf12104.a/'`
   LOCOMM=`echo $LOCOMM | sed -e 's/\.a/_xlf12104.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf12104.a/'`
  elif [ "$xlfver" = 'xlf13108' ] ; then
   LOPLOT=`echo $LOPLOT | sed -e 's/\.a/_xlf13108.a/'`
   LODIAG=`echo $LODIAG | sed -e 's/\.a/_xlf13108.a/'`
   LOCOMM=`echo $LOCOMM | sed -e 's/\.a/_xlf13108.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf13108.a/'`
  fi
  if [ "$OS" = 'AIX' -a "$p5lib" = 'yes' ] ; then
   LOCOMM=`echo $LOCOMM | sed -e 's/ \/usr\/lib\/lib/ \/home\/crb_ccrn\/pollux\/acrn\/src\/plib\/p5lib\/lib/g' | sed -e 's/massvp. /massvp4 /g'`
  fi
   set -x
    $F77 -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LOPLOT $LODIAG $LOCOMM $AIX_LOCAL $LINKNCAR $LINKEXT
   set +x
###   fi
elif [ "$float1" = 'no' -a "$openmp" = 'no' ] ; then
  if [ -n "$modver" ] ; then
   LOMODEL_ver=`echo $LOMODEL_float2 | eval sed 's/_model/_model_${modver}/g'`
  else
   LOMODEL_ver="$LOMODEL_float2"
  fi
  if [ "$xlfver" = 'xlf10103' ] ; then
   LOPLOT_float2=`echo $LOPLOT_float2 | sed -e 's/\.a/_xlf10103.a/'`
   LODIAG_float2=`echo $LODIAG_float2 | sed -e 's/\.a/_xlf10103.a/'`
   LOCOMM_float2=`echo $LOCOMM_float2 | sed -e 's/\.a/_xlf10103.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf10103.a/'`
  elif [ "$xlfver" = 'xlf12104' ] ; then
   LOPLOT_float2=`echo $LOPLOT_float2 | sed -e 's/\.a/_xlf12104.a/'`
   LODIAG_float2=`echo $LODIAG_float2 | sed -e 's/\.a/_xlf12104.a/'`
   LOCOMM_float2=`echo $LOCOMM_float2 | sed -e 's/\.a/_xlf12104.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf12104.a/'`
  elif [ "$xlfver" = 'xlf13108' ] ; then
   LOPLOT_float2=`echo $LOPLOT_float2 | sed -e 's/\.a/_xlf13108.a/'`
   LODIAG_float2=`echo $LODIAG_float2 | sed -e 's/\.a/_xlf13108.a/'`
   LOCOMM_float2=`echo $LOCOMM_float2 | sed -e 's/\.a/_xlf13108.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf13108.a/'`
  fi
  if [ "$OS" = 'AIX' -a "$p5lib" = 'yes' ] ; then
   LOCOMM_float2=`echo $LOCOMM_float2 | sed -e 's/ \/usr\/lib\/lib/ \/home\/crb_ccrn\/pollux\/acrn\/src\/plib\/p5lib\/lib/g' | sed -e 's/massvp. /massvp4 /g'`
  fi
# $F77_float2 -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LINKPLT_float2 $LINKNCAR_float2 $LINKEXT_float2 
   set -x
  $F77_float2 -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LOPLOT_float2 $LODIAG_float2 $LOCOMM_float2 $LINKNCAR_float2 $LINKEXT_float2 
   set +x
elif [ "$float1" = 'no' ] ; then
  if [ -n "$modver" ] ; then
#  LOMODEL_ver=`eval echo '${LOMODEL'"${Tf90x4}"'_openmp}' | eval sed 's/_model/_model_${modver}/g'`
   LOMODEL_ver=`echo ${LOMODEL_openmp} | eval sed 's/_model/_model_${modver}/g'`
  else
#  LOMODEL_ver=`eval echo '${LOMODEL'"${Tf90x4}"'_openmp}'`
   LOMODEL_ver="${LOMODEL_openmp}"
  fi
  if [ "$xlfver" = 'xlf10103' ] ; then
   LOPLOT_openmp=`echo $LOPLOT_openmp | sed -e 's/\.a/_xlf10103.a/'`
   LODIAG_openmp=`echo $LODIAG_openmp | sed -e 's/\.a/_xlf10103.a/'`
   LOCOMM_openmp=`echo $LOCOMM_openmp | sed -e 's/\.a/_xlf10103.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf10103.a/'`
  elif [ "$xlfver" = 'xlf12104' ] ; then
   LOPLOT_openmp=`echo $LOPLOT_openmp | sed -e 's/\.a/_xlf12104.a/'`
   LODIAG_openmp=`echo $LODIAG_openmp | sed -e 's/\.a/_xlf12104.a/'`
   LOCOMM_openmp=`echo $LOCOMM_openmp | sed -e 's/\.a/_xlf12104.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf12104.a/'`
  elif [ "$xlfver" = 'xlf13108' ] ; then
   LOPLOT_openmp=`echo $LOPLOT_openmp | sed -e 's/\.a/_xlf13108.a/'`
   LODIAG_openmp=`echo $LODIAG_openmp | sed -e 's/\.a/_xlf13108.a/'`
   LOCOMM_openmp=`echo $LOCOMM_openmp | sed -e 's/\.a/_xlf13108.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf13108.a/'`
  fi
  if [ "$OS" = 'AIX' -a "$p5lib" = 'yes' ] ; then
   LOCOMM_openmp=`echo $LOCOMM_openmp | sed -e 's/ \/usr\/lib\/lib/ \/home\/crb_ccrn\/pollux\/acrn\/src\/plib\/p5lib\/lib/g' | sed -e 's/massvp. /massvp4 /g'`
  fi
   set -x
#  $F77_openmp -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LOCOMM_openmp
   $F77_openmp -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LOPLOT_openmp $LODIAG_openmp $LOCOMM_openmp $LINKNCAR_openmp $LINKEXT_openmp 
   set +x
###   fi
elif [ "$openmp" = 'no' ] ; then
  if [ -n "$modver" ] ; then
   LOMODEL_ver=`echo $LOMODEL_float1 | eval sed 's/_model/_model_${modver}/g'`
  else
   LOMODEL_ver="$LOMODEL_float1"
  fi
  if [ "$OS" = 'AIX' ] ; then
   OBJECT_MODE='32' ; export OBJECT_MODE
  fi
  if [ "$xlfver" = 'xlf10103' ] ; then
   LOPLOT_float1=`echo $LOPLOT_float1 | sed -e 's/\.a/_xlf10103.a/'`
   LODIAG_float1=`echo $LODIAG_float1 | sed -e 's/\.a/_xlf10103.a/'`
   LOCOMM_float1=`echo $LOCOMM_float1 | sed -e 's/\.a/_xlf10103.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf10103.a/'`
  elif [ "$xlfver" = 'xlf12104' ] ; then
   LOPLOT_float1=`echo $LOPLOT_float1 | sed -e 's/\.a/_xlf12104.a/'`
   LODIAG_float1=`echo $LODIAG_float1 | sed -e 's/\.a/_xlf12104.a/'`
   LOCOMM_float1=`echo $LOCOMM_float1 | sed -e 's/\.a/_xlf12104.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf12104.a/'`
  elif [ "$xlfver" = 'xlf13108' ] ; then
   LOPLOT_float1=`echo $LOPLOT_float1 | sed -e 's/\.a/_xlf13108.a/'`
   LODIAG_float1=`echo $LODIAG_float1 | sed -e 's/\.a/_xlf13108.a/'`
   LOCOMM_float1=`echo $LOCOMM_float1 | sed -e 's/\.a/_xlf13108.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf13108.a/'`
  fi
  if [ "$OS" = 'AIX' -a "$p5lib" = 'yes' ] ; then
   LOCOMM_float1=`echo $LOCOMM_float1 | sed -e 's/ \/usr\/lib\/lib/ \/home\/crb_ccrn\/pollux\/acrn\/src\/plib\/p5lib\/lib/g' | sed -e 's/massvp. /massvp4 /g'`
  fi
# $F77_float1 -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LINKPLT_float1 $LINKNCAR_float1 $LINKEXT_float1 
   set -x
  $F77_float1 -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LOPLOT_float1 $LODIAG_float1 $LOCOMM_float1 $LINKNCAR_float1 $LINKEXT_float1 
   set +x
else
  if [ -n "$modver" ] ; then
   LOMODEL_ver=`echo $LOMODEL_float1_openmp | eval sed 's/_model/_model_${modver}/g'`
  else
   LOMODEL_ver="$LOMODEL_float1_openmp"
  fi
  if [ "$xlfver" = 'xlf10103' ] ; then
   LOPLOT_float1_openmp=`echo $LOPLOT_float1_openmp | sed -e 's/\.a/_xlf10103.a/'`
   LODIAG_float1_openmp=`echo $LODIAG_float1_openmp | sed -e 's/\.a/_xlf10103.a/'`
   LOCOMM_float1_openmp=`echo $LOCOMM_float1_openmp | sed -e 's/\.a/_xlf10103.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf10103.a/'`
  elif [ "$xlfver" = 'xlf12104' ] ; then
   LOPLOT_float1_openmp=`echo $LOPLOT_float1_openmp | sed -e 's/\.a/_xlf12104.a/'`
   LODIAG_float1_openmp=`echo $LODIAG_float1_openmp | sed -e 's/\.a/_xlf12104.a/'`
   LOCOMM_float1_openmp=`echo $LOCOMM_float1_openmp | sed -e 's/\.a/_xlf12104.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf12104.a/'`
  elif [ "$xlfver" = 'xlf13108' ] ; then
   LOPLOT_float1_openmp=`echo $LOPLOT_float1_openmp | sed -e 's/\.a/_xlf13108.a/'`
   LODIAG_float1_openmp=`echo $LODIAG_float1_openmp | sed -e 's/\.a/_xlf13108.a/'`
   LOCOMM_float1_openmp=`echo $LOCOMM_float1_openmp | sed -e 's/\.a/_xlf13108.a/'`
   LOMODEL_ver=`echo $LOMODEL_ver | sed -e 's/\.a/_xlf13108.a/'`
  fi
  if [ "$OS" = 'AIX' -a "$p5lib" = 'yes' ] ; then
   LOCOMM_float1_openmp=`echo $LOCOMM_float1_openmp | sed -e 's/ \/usr\/lib\/lib/ \/home\/crb_ccrn\/pollux\/acrn\/src\/plib\/p5lib\/lib/g' | sed -e 's/massvp. /massvp4 /g'`
  fi
  # if [ "$OS" = 'AIX' ] ; then
  #  OBJECT_MODE='32' ; export OBJECT_MODE
  # fi
# $F77_float1_openmp -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LINKPLT_float1_openmp $LINKNCAR_float1_openmp $LINKEXT_float1_openmp 
   set -x
  $F77_float1_openmp -o $a $options $lstswtch code$$.f $pkdoto $LOMODEL_ver $LOPLOT_float1_openmp $LODIAG_float1_openmp $LOCOMM_float1_openmp  $LINKNCAR_float1_openmp $LINKEXT_float1_openmp 
   set +x
fi
# set +v
#  * Return a non-zero exit status and error message if there is 
#  * a problem with the compilation.

# if [ "$?" -ne 252 -a "$?" -ne 0 ] ; then
if [ "$?" -ne 0 -o \( "$HOSTHW" = 'SX-6' -a ! -s "$a" \) ] ; then
  echo " nogo: Problem in compile"
  echo " nogo: Problem in compile" >> haltit
  
  if [ -s code$$.lst ] ; then
   cat code$$.lst
  elif [ -s code$$.L ] ; then
   cat code$$.L
  fi 
  exit 5
fi
hash -r

#  * Generate a listing if requested.

if [ "$list" = 'yes' ] ; then
  if [ -s code$$.lst ] ; then
   cat code$$.lst
  elif [ -s code$$.L ] ; then
   cat code$$.L
  fi 
fi

exit
