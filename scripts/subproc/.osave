#!/bin/sh

#   Feb 25/02 - F.Majaess (Revised for SX6)
#   Jan 05/00 - F.Majaess (Added kz/kaze)
#   Jan 21/99 - F.Majaess (Added yo/yonaka)
#   Jul 14/97 - F.Majaess (Revise for hiru)
#   Mar 18/97 - F.Majaess (Revise for group file ownership)
#   Aug 26/96 - F.Majaess (Revise to use "samefs" instead of relying on "df")
#   Jul 15/96 - F.Majaess (Revise for SX4)
#   Dec 08/95 - F.Majaess (Replace 'hostname' calls by "$HOSTID")
#   Jun 05/95 - F. Majaess (disble changing c3cnet RUNPATH to 3rcnet on sx3r)
#   May 29/95 - F. Majaess (Temporarily change c3cnet RUNPATH to 3rcnet on sx3r)
#   Feb 13/95 - F. Majaess (enforce "pfn" to be in lower case)
#   Nov 08/94 - F. Majaess (Modify for sx3r)
#   Jan 31/94 - F. Majaess (Ensure error detection due to filesystem failure)
#   Dec 29/93 - F. Majaess (Modify for saving files from other than
#                           "$RUNPATH")
#   Mar 08/93 - E. Chan (Save with no file access for "others")
#   Jan 19/92 - E. Chan (Add printing out of info at completion of script) 
#   Nov 24/92 - E. Chan (Modify to save files on current file system
#                        and add corresponding symbolic links to the 
#                        official CCRD/N data directory)
#   Nov 18/92 - E. Chan (Replace copy of file to data directory with a
#                        move followed by a symbolic link)
#   Sep 08/92 - E. Chan (Modify check for executable binaries for MIPS)
#   Sep 10/91 - E. Chan

#id save - saves a file on the user's own run directory and establishes
#id        a symbolic link by the same name on the offical CCRD/N data
#id        directory

#   AUTHOR  - E. Chan

#hd PURPOSE - If the current directory is residing on the same filesystem 
#hd           as the user's '$RUNPATH' directory then: 
#hd             "save" moves the file into the user's own '$RUNPATH' directory  
#hd             and appends an edition number to the filename. 
#hd  
#hd             After the file is moved, a symbolic link to the file is made
#hd             in the directory from which "save" was invoked to replace
#hd             the local version of the file. This is like a copy operation
#hd             without incurring the penalty for I/O. 
#hd           otherwise,
#hd             "save" copies the file into the user's own '$RUNPATH' directory  
#hd             which can reside on the other back-end machine if running on
#hd             the back-end and appends an edition number to the filename. 
#hd
#hd             In addition, a ".fn"_Link soft link to the permanent dataset is
#hd             created. 
#hd  
#hd           The edition number is in the form of a three digit extension 
#hd           following a period. If other editions of the same file already 
#hd           exist, "save" generates the next higher edition by default.
#hd
#hd           if '$RUNPATH' and '$DATAPATH' are different then a symbolic link 
#hd           to the saved file is placed on the official CCRD/N data directory 
#hd           '$DATAPATH' which may reside on a different file system. This 
#hd           allows users running on other file systems to access the file 
#hd           simply by using the link established in '$DATAPATH'. The name of 
#hd           the link is identical to the name of the saved file.

#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     fn     = name of file in directory from which script is invoked  
#pr  
#pr     pfn    = name of file saved on user's own '$RUNPATH' directory
#pr 
#pr   PRIMARY
#pr
#pr     ed     = edition number of offical CCRD/N file 
#pr              (default: the latest edition)
#pr
#pr     dpalist= switch which can be set to enable/disable the use of
#pr              ".datapath.list.access" local cache file of DATAPATH
#pr              subdirectory listing to speedup the script execution.
#pr              This mode is used only if no "ed" input is specified.
#pr              And, for interactive session, it's honoured only if 
#pr              explicitily specified,(ie. dpalist=... ),on the command
#pr              line otherwise it's assumed to be set to "off/no" 
#pr              equivalent (see below).
#pr              =on/yes; create ".datapath.list.access" file if not present,
#pr                       then use it to decide on "pfn" edition number to use
#pr              =new;    create ".datapath.list.access" file then use it
#pr                       to decide on "pfn" edition number to use
#pr              =off/no; delete ".datapath.list.access" file if present,
#pr                       scan DATAPATH subdirectory for "pfn" edition
#pr                       number to use
#pr              unset;  default mode is to scan DATAPATH subdirectory for
#pr                      "pfn" edition number to use
#pr
#pr fil_grpname= User has the option of specifying one of
#pr              'ccrn_shr/ccrn_stf/ccrn_net/ccrn_rcm/ccrn_mam/ccrn_gst'
#pr              group ownership on the saved file.
#pr              (=$DEFFGRP).
#pr
#pr   PRIMARY/SECONDARY
#pr
#pr     na     = switch that, if set, ensures the return of exit status 0 
#pr              (i.e. 'no abort') even if save fails (=''/'na')

#ex EXAMPLE:
#ex   
#ex     save data datfile ed=3
#ex
#ex     The above example moves a file called 'data' into the user's own
#ex     '$RUNPATH' directory with the name 'datfile.003'. If the edition
#ex     number is not specified, the file is saved as the latest edition.
#ex     A symbolic link to 'datfile.003' is made in the directory from which 
#ex     save is invoked. The link is named 'data' as a replacement for the 
#ex     local version of the file. A second link to 'datfile.003' is placed
#ex     in '$DATAPATH' with a name identical to that of the saved file.
#ex
#ex     save data datfile dpalist=on
#ex
#ex     The above will result in creating ".datapath.list.access" cache file 
#ex     of DATAPATH subdirectory listing if not already present, then use it 
#ex     to scan and decide on "pfn" edition number to use
#ex

#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script). 

IFS=' '
export IFS

#  * Capture command line arguments in "arg_list"

arg_list=$@

# Ensure "Shell_Mode" variable is defined.

if [ "$SITE_ID" = 'Dorval' ] ; then
 case "$-" in
  *i*)  Shell_Mode='interactive' ;;
  *) Shell_Mode='not interactive' ;;
 esac
 if [ -n "$SSH_TTY" ] ; then Shell_Mode='interactive' ; fi
fi

# Restrict "dpalist" option use for "interactive" mode
# to "dpalist=..." be specified on the command line.

if [ "$SITE_ID" = 'Dorval' -a "$Shell_Mode" = 'interactive' ] ; then
  dpalist=''
fi

#  * Obtain the file names and any specified option.

for arg in $arg_list
do 
  case $arg in
      -*) set $arg                                         ;;
    ed=*) ed=`expr $arg : 'ed=\([0-9]*\)'`                 ;;
    fil_grpname=*) eval "$arg"                             ;;
      na) na='na'                                          ;;
       *) tmp1=${fn:+$arg} ; pfn=${pfn:=$tmp1}              ; 
          fn=${fn:=$arg}                                             
  esac
done
 
#  * Set variable 'AWK' to the version of "awk" to be used.

AWK=${AWK:='awk'}

#  * Setup for SX-6 hardware.

if [ "$SITE_ID" = 'Dorval' ] ; then 
 HOSTHW=${HOSTHW:=`uname -m`}
fi

#  * Ensure filenames have been specified.

if [ -z "$fn" -o -z "$pfn" ] ; then
  echo "  Abort in SAVE: must specify both input and output files"
  exit 1
fi

pfn=`echo $pfn | tr '[A-Z]' '[a-z]'`
DIRLIST='.datapath.list.access'

# For "interactive" session case, set "dpalist='no'" if it's not 
# explicitly specified in the comand line.

if [ "$SITE_ID" = 'Dorval' ] ; then
 if [ "$Shell_Mode" = 'interactive' -a -z "$dpalist" ] ; then
  dpalist='no'
 fi
 if [ "$dpalist" = 'on' ] ; then
  dpalist='yes'
 elif [ "$dpalist" = 'off' ] ; then
  dpalist='no'
 fi
 if [ "$dpalist" = 'no' ] ; then
  (\rm -f $DIRLIST || : )
 fi
fi

#  * Setup for back-end target machine based on $RUNPATH.

same='yes'
# if [ "$HOSTID" = 'sx3r' -a "$RUNPATH" = '/data/c3cnet' ] ; then
#   RUNPATH='/data/3rcnet'
#   export RUNPATH
# fi
##! if [ "$HOSTID" = 'hiru' -o "$HOSTID" = 'yonaka' -o "$HOSTID" = 'asa' ] ; then
##!  sbemchn=`echo "$RUNPATH" | sed -n -e 's/^\/node\/crb\/10.*$/yonaka/p' `
##!  sbemchn=${sbemchn:=`echo "$RUNPATH" | sed -n -e 's/^\/node\/temp_ibm.*$/yonaka/p' `}
##!  sbemchn=${sbemchn:=`echo "$RUNPATH" | sed -n -e 's/^\/sx\/crb\/src0.*$/yonaka/p' `}
##!  sbemchn=${sbemchn:=`echo "$RUNPATH" | sed -n -e 's/^\/sx\/crb\/fc.*$/kaze/p' `}
##!  sbemchn=${sbemchn:=`echo "$RUNPATH" | sed -n -e 's/^\/node\/crb\/src_kz.*$/kaze/p' `}
##!  sbemchn=${sbemchn:=`echo "$RUNPATH" | sed -n -e 's/^\/data\/asa.*$/asa/p' `}
##!  sbemchn=${sbemchn:=`echo "$RUNPATH" | sed -n -e 's/^\/sx\/crb.*$/hiru/p' `}
##!  sbemchn=${sbemchn:=`echo "$RUNPATH" | sed -n -e 's/^\/node\/crba0.*$/hiru/p' `}
##!  sbemchn=${sbemchn:-'asa'}
##!  case $sbemchn in
##!      asa) sdatapath='/data/asa_ccrd/src/data' ;;
##!     hiru) sdatapath='/sx/crba_src'  ;;
##!     yonaka) sdatapath='/node/crb/10/data' ;;
##!     kaze) sdatapath='/node/crb/src_kz/data' ;;
##!       * ) sdatapath='/data/c3src/data' 
##!  esac
##!  if [ "$HOSTID" != "$sbemchn" ] ; then
##!   same='no'
##!  fi
##! fi
fromtmp='no'

#  * Generate the edition number if it is not provided.

if [ -z "$ed" ] ; then 

  #  * Check on the current machine the official CCRD/N data directory, 
  #  * $CCRNTMP/recover (and possibly the official CCRD/N data directory 
  #  * on the other back-end machine if running on the back-end) and 
  #  * assign the name of the latest existing edition of the file to the 
  #  * parameter 'file'.

  if [ "$same" = 'yes' ] ; then
   # if [ "$SITE_ID" = 'Dorval' -a \( "$dpalist" = 'yes' -o "$dpalist" = 'new' \)  -a "$Shell_Mode" = 'not interactive' ] ; then
   if [ "$SITE_ID" = 'Dorval' -a \( "$dpalist" = 'yes' -o "$dpalist" = 'new' \) ] ; then
     # create a file list of DATAPATH, if it does not exist
     #if [ ! -s $DIRLIST -o ${DATAPATH}/. -nt $DIRLIST ] ; then
     if [ "$dpalist" = 'new' ] ; then
      (\rm -f $DIRLIST || : )
     fi
     if [ ! -s $DIRLIST ] ; then
      echo "Create $DIRLIST"
      dirplist > $DIRLIST
     fi
     file=`cat $DIRLIST | sed -e 's/\@ *$//g' | egrep "^${pfn}"'[.][0-9][0-9][0-9]$' | sort |  tail -1`
     if [ -n "$file" ] ; then
      file=`ls $DATAPATH/${file} 2>/dev/null`
     fi
   elif [ "$SITE_ID" = 'Dorval' ] ; then
    file=`dirplist | sed -e 's/\@ *$//g' | egrep "^${pfn}"'[.][0-9][0-9][0-9]$' | sort |  tail -1`
    if [ -n "$file" ] ; then
     file=`ls $DATAPATH/${file} 2>/dev/null`
    fi
   else
    file=`ls $DATAPATH/$pfn.[0-9][0-9][0-9] 2>/dev/null | tail -1` 
   fi
  else
   file2=`ls $CCRNTMP/recover/$pfn.[0-9][0-9][0-9] 2>/dev/null | tail -1`
   if [ -z "$file2" ] ; then
    file=`rsh $sbemchn ls $sdatapath/$pfn.[0-9][0-9][0-9] | tail -1 | grep $pfn `
   else
    echo "  Abort from SAVE: saved edition is found in $file, please resubmit after"
    echo "                   removing/relocating $file via delet/rcvrfils scripts" 
    echo "  Abort from SAVE: saved edition is found in $file, please resubmit after" >> haltit
    echo "                   removing/relocating $file via delet/rcvrfils scripts" >> haltit
    exit 1
   fi
  fi

  #  * Check if previous editions exist and set the edition number.
  #  * If previous editions do not exist, set the edition number to '1'.
  #  * Otherwise, add one to the highest existing edition number.

  if [ -z "$file" ] ; then
    ed='1'
  else
    ed=`echo $file | sed -e 's/.*[.]\([0-9][0-9][0-9]\)/\1/' | $AWK '{printf "%03d",$1+1}'`
  fi

fi

# If running at Dorval site, check for proper file group ownership  
# (if specified)...

if [ "$SITE_ID" = 'Dorval' ] ; then
 fil_grpname='ccrn_shr'
else
 unset fil_grpname
fi

#  * Set the error level based on the 'na' option.

if [ -n "$na" ] ; then
  error='Warning'
  level=0
else
  error='  Abort'
  level=1
fi

#  * Ensure RUNPATH exists.

if [ ! -d "${RUNPATH}/." ] ; then
  echo "$error in SAVE: Invalid subdirectory for RUNPATH --> $RUNPATH <-- !"
  exit $level
fi

#  * Verify that file to be saved exists.

if [ ! -f "$fn" ] ; then
  echo "$error in SAVE: file $fn does not exist"
  exit $level
fi

#  * Format edition number
 
ed=`echo $ed | $AWK '{printf "%03d",$1}'`

if [ "$ed" -gt 999 ] ; then
  echo "$error in SAVE: trying to save edition $ed (max: 999)"
  exit $level
fi

#  * Verify that file with given edition number does not already exist on
#  * the official CCRN data directory.  

if [ -f "$DATAPATH/$pfn.$ed" -a "$same" = 'yes' ] ; then 
  echo "$error in SAVE: edition number $ed already exists for file $pfn"
  exit $level
fi

#  * If '$RUNPATH' is on the local machine:
#  * Move the file into the user's "run" directory defined by the user's
#  * own '$RUNPATH'. Establish a symbolic link in current directory using
#  * the name of the local version of the file. If, however, '$fn' is 
#  * already a symbolic link, then copy the file associated with the link
#  * to the file to be saved.

file=$RUNPATH/$pfn.$ed
if [ "$same" = 'yes' ] ; then
# pflstm=`df $RUNPATH 2>/dev/null | tail -1 | sed -n -e 's/ .*$//p' `
# cflstm=`df ./       | tail -1 | sed -n -e 's/ .*$//p' `
# if [ "$pflstm"  = "$cflstm" ] ; then
 xx=`samefs ./. $RUNPATH 2>/dev/null`
 if [ "$?" -eq 0 ] ; then

  if [ ! -L "$fn" ] ; then 
    mv $fn $file 
    if [ "$?" -ne 0 ] ; then 
      echo "$error in SAVE: file $fn has not been copied"
      exit $level
    fi
    if [ -n "$fil_grpname" ] ; then
     chgrp $fil_grpname $file
    fi
    ln -s $file $fn  
    if [ "$?" -ne 0 ] ; then
      echo "$error in SAVE: link $fn can not be established" 
      echo "                to $file file"
      exit $level
    fi
  else
    if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'Linux' ] ; then
     xx=`samefs $fn /fs/cetus3/fs3 2>/dev/null` 
     if [ "$?" -eq 0 ] ; then
      srcp -b 4M cetus3:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file
     #scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file
     else
      xx=`samefs $fn /fs/cetus/fs2 2>/dev/null`
      if [ "$?" -eq 0 ] ; then
       srcp -b 4M cetus:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus:`pwd`/$fn $file
       #scp -o NoneSwitch=yes cetus:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus:`pwd`/$fn $file
      else
       cp $fn $file
      fi
     fi
    else
     cp $fn $file
    fi
    if [ "$?" -ne 0 ] ; then 
      echo "$error in SAVE: file $fn has not been copied"
      exit $level
    fi
    if [ -n "$fil_grpname" ] ; then
     chgrp $fil_grpname $file
    fi
  fi
 else
  Lfn=".""$fn""_Link"

  if [ ! -L "$Lfn" ] ; then 
    if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'Linux' ] ; then
     xx=`samefs $fn /fs/cetus3/fs3 2>/dev/null` 
     if [ "$?" -eq 0 ] ; then
      srcp -b 4M cetus3:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file
     #scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file
     else
      xx=`samefs $fn /fs/cetus/fs2 2>/dev/null`
      if [ "$?" -eq 0 ] ; then
       srcp -b 4M cetus:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus:`pwd`/$fn $file
       #scp -o NoneSwitch=yes cetus:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus:`pwd`/$fn $file
      else
       cp $fn $file
      fi
     fi
    else
     cp $fn $file
    fi
    if [ "$?" -ne 0 ] ; then 
      echo "$error in SAVE: file $fn has not been copied"
      exit $level
    fi
    if [ -n "$fil_grpname" ] ; then
     chgrp $fil_grpname $file
    fi
    \rm -f $Lfn 2>/dev/null
    ln -s $file $Lfn 
    if [ "$?" -ne 0 ] ; then
      echo "$error in SAVE: link $Lfn can not be established" 
      echo "                to $file file"
      exit $level
    fi
  else
    if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'Linux' ] ; then
     xx=`samefs $fn /fs/cetus3/fs3 2>/dev/null` 
     if [ "$?" -eq 0 ] ; then
      srcp -b 4M cetus3:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file
     #scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus3:`pwd`/$fn $file
     else
      xx=`samefs $fn /fs/cetus/fs2 2>/dev/null`
      if [ "$?" -eq 0 ] ; then
       srcp -b 4M cetus:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus:`pwd`/$fn $file
       #scp -o NoneSwitch=yes cetus:`pwd`/$fn $file || scp -o NoneSwitch=yes cetus:`pwd`/$fn $file
      else
       cp $fn $file
      fi
     fi
    else
     cp $fn $file
    fi
    if [ "$?" -ne 0 ] ; then 
      echo "$error in SAVE: file $fn has not been copied"
      exit $level
    fi
    if [ -n "$fil_grpname" ] ; then
     chgrp $fil_grpname $file
    fi
    ln -s $file $Lfn 
  fi
 fi
 
 #  * The file is saved without write permission. Execute permission is 
 #  * given only if the file contains executable binary or commands. 
 
 if [ ! -L "$file" ] ; then

  info=`file $file 2>/dev/null `

  file_type=`expr "$info" : '.*\(executable\).*'`
  file_type=${file_type:=`expr "$info" : '.*\(commands\).*'`}
  file_type=${file_type:=`expr "$info" : '.*\(demand paged\).*'`}
 
  if [ -n "$file_type" ] ; then
   chmod 550 $file
  else
   chmod 440 $file
  fi

 fi
 
 #  * Establish symbolic link on the CCRD/N official data directory pointing
 #  * to the file just saved. If the run and data directories are identical,
 #  * then the link is not required.
 
 if [ $RUNPATH != $DATAPATH ] ; then
   ln -s $file $DATAPATH/$pfn.$ed
   if [ "$?" -ne 0 ] ; then 
     echo "$error in SAVE: link on $DATAPATH has not been established for" 
     echo "                file $file"
     exit $level
   fi
 fi

else

 #  * If running on the back-end and '$RUNPATH' is on the other machine:
 #  * Remote copy the file into $RUNPATH, verify the transfer is complete,
 #  * possibly setup the link on the remote machine data directory, make
 #  * sure '$fn' is a file and finally setup a link to keep track of where
 #  * the file is saved.
 #  * In case the saving failed, attempt saving in the local $CCRNTMP/recover.

 sline1=`rsh $sbemchn ls $sdatapath/$pfn.$ed | tail -1 | grep $pfn `
 if [ -n "$sline1" ] ; then
  echo "$error in SAVE: file $sdatapath/$pfn.$ed already exists on $sbemchn"
  exit $level
 fi
 if [ "$sdatapath" != "$RUNPATH" ] ; then
  unset sline1
  sline1=`rsh $sbemchn ls $RUNPATH/$pfn.$ed | tail -1 | grep $pfn `
  if [ -n "$sline1" ] ; then
   echo "$error in SAVE: file $RUNPATH/$pfn.$ed already exists on $sbemchn"
   exit $level
  fi
 fi
 lfn=$fn 
 lpfn=$lfn
 while [ -L "$lfn" ] 
  do
   line=`ls -l $lfn 2>/dev/null`
   lpfn=`expr "$line" : '.*-> \(.*\)'` 
   lfn=$lpfn
  done
  Xpctsz=`ls -al $lpfn | $AWK '{ print \$5 ; } '`
  rcp -p $lpfn $sbemchn:$RUNPATH/$pfn.$ed
  Actlsz=`rsh $sbemchn ls -al $RUNPATH/$pfn.$ed | $AWK '{ print \$5 ; }' `
  if [ "$Xpctsz" != "$Actlsz" ] ; then
   echo "$error in SAVE: Incomplete dataset transfer to $sbemchn:$RUNPATH/$pfn.$ed"
   echo "$error in SAVE: Incomplete dataset transfer to $sbemchn:$RUNPATH/$pfn.$ed" >> haltit
   if [ -f $CCRNTMP/recover/$pfn.999 ] ; then
    echo "  Abort in SAVE: Unable to save $fn in $CCRNTMP/recover/$pfn.999"
    echo "  Abort in SAVE: Unable to save $fn in $CCRNTMP/recover/$pfn.999" >> haltit
    exit 1
   else
    cp -p $lpfn $CCRNTMP/recover/$pfn.999
    file="$CCRNTMP/recover/$pfn.999"
    echo "Warning from SAVE: file $fn is saved in $file, please remove/relocate" 
    echo "                   to your $RUNPATH ASAP using delet/rcvrfils" 
    echo "Warning from SAVE: file $fn is saved in $file, please remove/relocate" >> haltit
    echo "                   to your $RUNPATH ASAP using delet/rcvrfils" >> haltit
    if [ -n "$fil_grpname" ] ; then
     chgrp $fil_grpname $CCRNTMP/recover/$pfn.999
    fi
   fi
   if [ ! -L "$file" ] ; then

    info=`file $file 2>/dev/null `
    file_type=`expr "$info" : '.*\(executable\).*'`
    file_type=${file_type:=`expr "$info" : '.*\(commands\).*'`}
    file_type=${file_type:=`expr "$info" : '.*\(demand paged\).*'`}
   
    if [ -n "$file_type" ] ; then
     chmod 550 $file
    else
     chmod 440 $file
    fi

   fi
   if [ -n "$fil_grpname" ] ; then
    chgrp $fil_grpname $file
   fi
   if [ -L "$fn" ] ; then
    \rm -f $fn 2>/dev/null
    \cp $lpfn $fn
   fi
   Lfn=".""$fn""_Link"
   \rm -f $Lfn 2>/dev/null
   ln -s $file $Lfn
  else
   if [ ! -L "$lpfn" ] ; then
    info=`file $lpfn 2>/dev/null `
    file_type=`expr "$info" : '.*\(executable\).*'`
    file_type=${file_type:=`expr "$info" : '.*\(commands\).*'`}
    file_type=${file_type:=`expr "$info" : '.*\(demand paged\).*'`}
  
    if [ -n "$file_type" ] ; then
     rsh $sbemchn chmod 550 $RUNPATH/$pfn.$ed
    else 
     rsh $sbemchn chmod 440 $RUNPATH/$pfn.$ed
    fi
    if [ -n "$fil_grpname" ] ; then
     rsh $sbemchn chgrp $fil_grpname $RUNPATH/$pfn.$ed
    fi
   fi
  
   if [ "$sdatapath" != "$RUNPATH" ] ; then
    rsh $sbemchn ln -s $RUNPATH/$pfn.$ed $sdatapath/$pfn.$ed
   fi
   if [ -L "$fn" ] ; then
    \rm -f $fn 2>/dev/null
    \cp $lpfn $fn
   fi
   Lfn=".""$fn""_Link"
   \rm -f $Lfn 2>/dev/null
   ln -s $RUNPATH/$pfn.$ed $Lfn
   file="$sbemchn:$RUNPATH/$pfn.$ed"
  fi
fi
# append the name to "DIRLIST" DATAPATH file list if it exists
if [ "$SITE_ID" = 'Dorval' -a -s $DIRLIST ] ; then
 echo $pfn.$ed >> $DIRLIST
fi
#  * Print out onto standard output the successful completion of the
#  * script. 

echo "0** $0 $*"
echo "    File $fn is saved as $file"
echo "0--------  End save  ---------------------------------------------------       0"

exit
