#! /bin/sh

#    Nov 17/2015 - F.Majaess

#id sd_flcpy- Copies individual file(s) into storage disk.

#   AUTHOR  - F.Majaess

#hd PURPOSE - "sd_flcpy" code is used to copy individual file(s) into
#hd           storage disk subdirectory.
#hd           Note: This script is not intended to be called directly by
#hd                 the user, instead it's to be called by higher level
#hd                 scripts.
#hd

#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr      f1 ... fn  = list of n file names to be copied.
#pr
#pr   PRIMARY
#pr
#pr           tpath = Target path subdirectory where the target 
#pr                   file(s) to be copied reside
#pr                   (must be specified)
#pr
#pr           thost = Target sever where "tpath" is to be accessed from.
#pr                   (must be specified)
#pr
#pr      sd_dirpath = Storage disk root subdirectory path
#pr                   (=/fs/cetus/fs2/crb/cccma_storage)
#pr
#pr   SECONDARY
#pr
#pr      sd_rewrite = Switch controls removal and overwritting of existing 
#pr                   dataset(s) (=no/yes)
#pr                   =yes; existing copy removed (if permission allows that)
#pr                         and "ug+rw" permission applied on the transferred 
#pr                         copy 
#pr                   =no; permission retained in the transferred copy as the
#pr                        original. Fate of owverwriting of existing dataset 
#pr                        will depend on its set permission.
#pr                   (default is 'yes')


#pr           offcl = Switch indicating "official" master data subdirectory 
#pr                   mapping.
#pr                   (=no/yes)

#ex EXAMPLE:
#ex   
#ex    ssh cetus 'sd_flcpy FILEA FILEB offcl=yes sd_rewrite=yes thost=hadar 
#ex                        tpath=/fs/dev/crb/had02/data/utmp/tmp_123'
#ex
#ex   The above "ssh" would be invoked from hadar,to excute "sd_flcpy" script  
#ex   on cetus, which would transfer copies of FILEA/FILEB datasets residing
#ex   in /fs/dev/crb/had02/data/utmp/tmp_123 on Hadar, into appropriate 
#ex   location under "sd_dirpath" (=/fs/cetus/fs2/crb/cccma_storage) 
#ex   subdirectory on cetus. Existing dataset removed prior to the transfer.
#ex   "ug+rw" permission applied on transferred dataset.

IFS=' '
export IFS
# set -x

#  * Set variable 'AWK' to the version of "awk" to be used.

AWK=${AWK:='awk'}

#  * Obtain the list of file(s) to be archived.

arg_list=$@
unset file_list
for arg in $arg_list
do
  case $arg in
          -*) set $arg                    ;;
         *=*) eval "$arg"                 ;;
       offcl) offcl='yes'                 ;;
  sd_rewrite) sd_rewrite='yes'                 ;;
           *) if [ -z "$file_list" ] ; then
               file_list="$arg"
              else
               file_list="$file_list $arg"
              fi 
  esac
done

# sd_server=${sd_server:='cetus'}

# Storage subdirectory root path  (on Cetus)

sd_dirpath=${sd_dirpath:='/fs/cetus/fs2/crb/cccma_storage'}

# Switch controls removal/overwriting of existing dataset 
#  yes       - allow
#  otherwise - disallow
sd_rewrite=${sd_rewrite:='yes'}
# sd_rewrite=${sd_rewrite:='no'}
if [ "$sd_rewrite" = 'on' ] ; then
  sd_rewrite='yes'
elif [ "$sd_rewrite" = 'off' ] ; then
  sd_rewrite='no'
fi
sd_rewrite='yes'
if [ -n "$sd_dirpath" -a -n "$tpath" -a -n "$thost" -a -n "$file_list" ] ; then 

  Final_Exit_status=0
  for Fname in $file_list
  do
  
   fname=`echo $Fname | tr '[A-Z]' '[a-z]'`
   rname=`expr //$fname : '.*/\(.*\)'`
    destsubdir='others'
    f1=`printf $rname | cut -f 1 -d _ | cut -c 2`
    f2=`printf $rname | cut -f 2 -d _`
    f3=`printf $rname | cut -f 1 -d _ | cut -c 1`
    clear='no'
    case $f1 in
     a|c|d|f|m|o|r) clear='yes' ;; 
     *) clear='no' ;;
    esac
    if [ "$clear" = 'yes' ] ; then
     case $f3 in
      m|p|d|t|s) clear='yes' ;; 
      *) clear='no' ;;
     esac
    fi
    if [ "$clear" = 'yes' ] ; then
     destsubdir="${f1}/${f2}/${f3}"
    fi 
    if [ "$offcl" = 'yes' ] ; then
     # dirmode='1775'
     maskstng='002'
    else
     LOGNAME=${LOGNAME:=`logname`}
     if [ "$destsubdir" = 'others' ] ; then
      #f1='u'
       f2=`printf $rname | cut -f 1 -d _`
       f3=`printf $rname | cut -f 2 -d _`
      #destsubdir="${f1}/${LOGNAME}/${f2}/${f3}"
       if [ "$f2" = "$rname" ] ; then
        destsubdir="u/${LOGNAME}/others"
       elif [ "$f2" = "$f3" ] ; then
        destsubdir="u/${LOGNAME}/others/${f2}"
       else
        destsubdir="u/${LOGNAME}/others/${f2}/${f3}"
       fi
     else
      destsubdir="u/${LOGNAME}/${destsubdir}"
     fi
     # dirmode='1755'
     maskstng='022'
    fi
    destdir="$sd_dirpath/$destsubdir"
    destfln="$sd_dirpath/$destsubdir/$rname"
    # [ ! -d "${destdir}/." ] && mkdir -p -m $dirmode $destdir || :
    # [ ! -d "${destdir}/." ] && ( umask $maskstng ; mkdir -p $destdir || : ; chmod -R +t $destdir 2>>/dev/null || : )
    [ ! -d "${destdir}/." ] && ( umask $maskstng ; mkdir -p $destdir || : )
    # [ -f "${destfln}" ] && chmod ug+rw ${destfln} || :
      [ -f "${destfln}" -a "$sd_rewrite" = 'yes' ] && rm -f ${destfln} || :

    #if [ -f "${destfln}" ] ; then
    # if [ "$sd_rewrite" = 'yes' ] ; then
    #  rm -f ${destfln} || :
    # else
    #  ( chmod a-w ${destfln} || : )
    # fi
    #fi

    if [ "$sd_rewrite" = 'yes' ] ; then
     [ -d "${destdir}/." ] && scp -p -o NoneSwitch=yes $thost:$tpath/$Fname ${destfln} && chmod ug+rw ${destfln} && Exit_status=$? || Exit_status=$?
    else
     [ -d "${destdir}/." ] && scp -p -o NoneSwitch=yes $thost:$tpath/$Fname ${destfln} && Exit_status=$? || Exit_status=$?
    fi
    [ "$Exit_status" -eq 0 ] && echo " " && echo "  Copied $Fname into ${destfln}" || :
    if [ "$Exit_status" -ne 0 ] ; then
     Final_Exit_status=1
     echo " " ; echo " sd_flcpy: Problem copying $Fname into ${destfln}!"
    fi
  done
  exit $Final_Exit_status
else
 echo " sd_flcpy: Sorry, incomplete needed input information!"
 exit 1
fi
