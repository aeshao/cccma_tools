#! /bin/sh
 
#    MAR 10/92 - F.Majaess
 
#id  prmry_dflt_prmtrs - Used to check/set primary defaults.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "prmry_dflt_prmtrs" code is used to check/set
#hd            parameters to their primary default values...
 
#pr  PARAMETERS:
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      assem  = switch accomodating user request to process assembler
#pr               code instead of Fortran (=no/yes).
#pr      adt    = switch controls producing an audit of back-end disk
#pr               space usage (=no/yes).
#pr      byaccss= switch controls processing of back-end files which have
#pr               not been accessed since a specified date (=no/yes).
#pr      byown  = switch controls processing only user's own files (=no/yes).
#pr      byusers= switch controls processing of back-end files by 
#pr               section/users (=no/yes).
#pr      change = switch controlling listing/changing password occurences.
#pr               (=no/yes)
#pr      cft    = switch accomodating user request to use CFT instead of
#pr               CFT77 to compile Fortran source code (=no/yes).
#pr      rmv    = switch controlling deletion of input file (=no/yes).
#pr      script = switch to generate a copy of the pre-processed job
#pr               script (i.e the next job in the string) without sending
#pr               the job to NQS (diagnostic jobs only) (=/'script').
#pr      extlib = external library to be referenced for the loader
#pr               (=NOLIB/AAAALIB).
#pr      full   = switch to accomodate user request to process whole set/
#pr               options (=no/yes).
#pr      keep   = switch controls keeping of input file (=no/yes).
#pr      lc     = update control character (='*'/'%').
#pr      lpath  = path of subdirectory containig scripts  
#pr               (=$CCRNSTD/$SUBPROC).
#pr      nocheck= switch used to double/bypass checking parameters
#pr               entered (=no/yes).
#pr      nobin  = switch controlling type of data transfer (=no/yes).
#pr      noconv = switch controlling conversion from/to CCRN standard binary
#pr               file to/from character format (=no/yes).
#pr      nolist = switch controlling source listing (=no/yes).
#pr      nobj   = switch controlling creation of an object library on the
#pr               back-end (=no/yes).
#pr      nosvaf = switch controlling saving accessed file on the front-end
#pr               (=no/yes).
#pr      nosave = switch controlling saving generated file on the back-end
#pr               (=no/yes).
#pr      others = switch controls deletion of back-end temporary diagnostic
#pr               run files (=no/yes).
#pr      ppath  = path of subdirectory containing output  (=$OUTPUTQ/$PRNTQ).
#pr      rs     = switch controls deletion of back-end model restart files
#pr               (=no/yes).
#pr      sjtfe  = switch controls job submission to the front-end instead 
#pr               of the back-end (=no/yes).
#pr      subaf  = switch accomodating user request to submit an accessed
#pr               file from another machine (=no/yes).
#pr      sx3tmfx= switch controlling file transfer direction from mfx/sx3 
#pr               to  sx3/mfx (=no/yes).
#pr      secdata= switch controlling default "despath" to primary (=no;default)
#pr               or secondary (=yes) data subdirectory on destination machine.
#pr      sx3    = switch controls changing destination to SX3 (=no/yes).
#pr      updat  = switch controlling creation of front-end update library 
#pr               (=no/yes).
#pr      updates= switch indicating updates to deck are to be used
#pr               (=no/yes).
#pr      updt1  = switch indicating updates to first  deck are to be used
#pr               (=no/yes).
#pr      updt2  = switch indicating updates to second deck are to be used
#pr               (=no/yes).
#pr      vdim   = switch indicating variable dimensioned Fortran source code
#pr               (=no/yes).
#pr      wait   = switch signaling the process to wait (if necessary) for
#pr               transfered input data (=no/yes).
#pr
#pr    PRIMARY
#pr
#pr      bln    = controls the number of longitudes for a subset 
#pr               of the program library which handle large record 
#pr               fields (1x1 grid)(=00360)
#pr      blt    = controls the number of latitudes  for a subset 
#pr               of the program library which handle large record 
#pr		  fields (1x1 grid) (equator included)(=00181).
#pr      box    = box to dispose printout if desired (=$BOX).
#pr      ed     = edition number of back-end file (='')
#pr      fepath = path to user's favourite directory on front-end 
#pr               account (=$FEPATH).
#pr      ilev   = number of model    levels (=00020)
#pr      lat    = number of grid latitudes  (=00048)
#pr      lmt    = m spectral truncation wave numbers (=00032)
#pr      lon    = number of grid longitudes (=00096)
#pr      lrt    = n spectral truncation wave numbers (=00032)
#pr      ncopies= number of copies requested (=1).
#pr      plv    = number of pressure levels (=00020)
#pr      sunpath= path to user's favourite directory on ccrn sun's 
#pr               account (=$SUNPATH).
#pr      sx3path= path to user's favourite directory on SX3 
#pr               account (=$BEOUTPUTQ/.data).
#pr      sv     = ccrn sun server net address (=$RMTSV)
#pr      tsl    = maximum time series length (=0000010241)
#pr      typ    = spectral truncation type 
#pr               (00000=rhomboidal, 00002=triangular)
#pr               (=00002)
#pr      un     = usernumber for printout (=$DESTIN). 
#pr      una    = user's account  (=$USER).
#pr      upath  = path to front-end subdirectory containing update 
#pr               library(ies)  (=$UPDTLIB)
#pr
 
#   * code used to set parameters to primary defaults.
 
eval "assem=${assem='no'}"
eval "adt=${adt='no'}"
eval "bln=${bln='00360'}"
eval "blt=${blt='00181'}"
eval "box=${box=$BOX}"
eval "byaccss=${byaccss='no'}"
eval "byown=${byown='no'}"
eval "byusers=${byusers='no'}"
eval "change=${change='no'}"
eval "cft=${cft='no'}"
eval "rmv=${rmv='no'}"
eval "script=${script=''}"
eval "ed=${ed=''}"
eval "extlib=${extlib='NOLIB'}"
eval "fepath=${fepath=$FEPATH}"
eval "full=${full='no'}"
eval "ilev=${ilev='00020'}"
eval "keep=${keep='no'}"
eval "lat=${lat='00048'}"
eval "lc=${lc='*'}"
eval "lmt=${lmt='00032'}"
eval "lon=${lon='00096'}"
eval "lpath=${lpath=$CCRNSTD}"
eval "lrt=${lrt='00032'}"
eval "ncopies=${ncopies=1}"
eval "nobin=${nobin='no'}"
eval "noconv=${noconv='no'}"
eval "nobj=${nobj='no'}"
eval "nocheck=${nocheck='no'}"
eval "nolist=${nolist='no'}"
eval "nosave=${nosave='no'}"
eval "nosvaf=${nosvaf='no'}"
eval "others=${others='no'}"
eval "plv=${plv='00020'}"
eval "ppath=${ppath=$OUTPUTQ}"
eval "rs=${rs='no'}"
eval "sjtfe=${sjtfe='no'}"
eval "subaf=${subaf='no'}"
eval "sunpath=${sunpath=$SUNPATH}"
eval "sx3path=${sx3path=$BEOUTPUTQ/.data}"
eval "sx3tmfx=${sx3tmfx='no'}"
eval "sv=${sv=$RMTSV}"
eval "tsl=${tsl='0000010241'}"
eval "typ=${typ='00002'}"
eval "un=${un=$DESTIN}"
eval "una=${una=$USER}"
eval "upath=${upath=$UPDTLIB}"
eval "updat=${updat='no'}"
eval "updates=${updates='no'}"
eval "updt1=${updt1='no'}"
eval "updt2=${updt2='no'}"
eval "vdim=${vdim='no'}"
eval "wait=${wait='no'}"
eval "secdata=${secdata='no'}"
eval "sx3=${sx3='no'}"

